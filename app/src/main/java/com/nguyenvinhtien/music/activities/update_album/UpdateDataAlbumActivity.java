package com.nguyenvinhtien.music.activities.update_album;

import android.app.ProgressDialog;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class UpdateDataAlbumActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_ALBUM_REQUEST = 5;
    private static final int SELECT_SONG_REQUEST = 3;



    TextView tvAlbumSelect;
    Button btnSelectAlbum, btnUpdateAlbum, btnSelectSong;
    EditText edtAlbumTitle, edtAlbumArtist, edtAlbumInfo;
    RelativeLayout rlAlbumDetail;

    ArrayList<String> key_list;
    ArrayList<String> title_list;
    ArrayList<String> artist_list;
    ArrayList<String> image_path_list;
    ArrayList<String> song_path_list;
    ArrayList<String> song_lyric_list;

    TextView tvSongSelect, tvListSongSelect;
    ImageButton ibSelectAlbumArt;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_album;
    String title_album;
    String artist_album;
    String info_album;
    String art_path_abum;

    List<String> key_song = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_data_album);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_data_album);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvAlbumSelect = (TextView) findViewById(R.id.tv_album_select);
        btnSelectAlbum = (Button) findViewById(R.id.btn_select_album);
        btnUpdateAlbum = (Button) findViewById(R.id.btn_update);
        edtAlbumTitle = (EditText) findViewById(R.id.edt_album_title);
        edtAlbumArtist = (EditText) findViewById(R.id.edt_album_artist);
        edtAlbumInfo = (EditText) findViewById(R.id.edt_album_info);
        rlAlbumDetail = (RelativeLayout) findViewById(R.id.rl_album_detail);
        tvListSongSelect = (TextView) findViewById(R.id.tv_list_select_song);
        tvListSongSelect.setMovementMethod(new ScrollingMovementMethod());
        tvSongSelect = (TextView) findViewById(R.id.tv_select_song);
        ibSelectAlbumArt = (ImageButton) findViewById(R.id.image_select);
        btnSelectSong = (Button) findViewById(R.id.btn_select_song);
    }

    public void initEvents() {
        btnSelectAlbum.setOnClickListener(this);
        btnUpdateAlbum.setOnClickListener(this);
        btnSelectSong.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_select_album:
                Intent selectAlbumIntent = new Intent(UpdateDataAlbumActivity.this, SelectAlbumToUpdateActivity.class);
                startActivityForResult(selectAlbumIntent, SELECT_ALBUM_REQUEST);
                break;

            case R.id.btn_select_song:
                tvSongSelect.setText("Không thay đổi");
                tvListSongSelect.setText("");
                Intent selectSongIntent = new Intent(UpdateDataAlbumActivity.this, SelectSongForAlbumActivity.class);
                selectSongIntent.putExtra(SelectSongForAlbumActivity.KIEM_TRA_ACTIVY_GOI, "UpdateDataAlbum");
                startActivityForResult(selectSongIntent, SELECT_SONG_REQUEST);
                break;


            case R.id.btn_update:

                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_ALBUM_REQUEST && resultCode == RESULT_OK) {
            key_album = data.getStringExtra("KEY_ALBUM");
            title_album = data.getStringExtra("TITLE_ALBUM");
            artist_album = data.getStringExtra("ARTIST_ALBUM");
            art_path_abum = data.getStringExtra("ART_PATH_ALBUM");
            info_album = data.getStringExtra("INFO_ALBUM");


            //thêm key_song cũ của album vào list key_song để xóa albums của Songs không nằm trong album này nữa
            mDatabase.child("Albums-Songs").child(key_album).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String keysong = dataSnapshot.getKey().toString();
                    key_song.add(keysong);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            updateUI();

        }


        if (requestCode == SELECT_SONG_REQUEST && resultCode == RESULT_OK) {
            key_list = data.getStringArrayListExtra("LIST_KEY_SONG");
            title_list = data.getStringArrayListExtra("LIST_TITLE_SONG");
            artist_list = data.getStringArrayListExtra("LIST_ARTIST_SONG");
            image_path_list = data.getStringArrayListExtra("LIST_IMAGE_PATH_SONG");
            song_path_list = data.getStringArrayListExtra("LIST_PATH_SONG");
            song_lyric_list = data.getStringArrayListExtra("LIST_LYRIC_SONG");

            updateUI();

            if (image_path_list != null) {
                StorageReference reference = FirebaseStorage.getInstance().getReference();
                if (image_path_list.get(0).equals(Constants.DEFAULT_SONG_ART_URL)) {
                    reference = reference.child("SongArts").child(Constants.DEFAULT_NAME_SONG_ART);
                } else {
                    reference = reference.child("SongArts").child(key_list.get(0));
                }

                Glide.with(this)
                        .using(new FirebaseImageLoader())
                        .load(reference)
                        .into(ibSelectAlbumArt);

            }
        }

    }

    private void updateUI() {
        if (title_album != null && artist_album != null && art_path_abum != null) {
            Glide.with(this).load(art_path_abum).into(ibSelectAlbumArt);
            tvAlbumSelect.setVisibility(View.VISIBLE);
            tvAlbumSelect.setText(title_album);
            rlAlbumDetail.setVisibility(View.VISIBLE);
            edtAlbumTitle.setText(title_album);
            edtAlbumArtist.setText(artist_album);
            edtAlbumInfo.setText(info_album);
            if (title_list != null) {
                for (String title : title_list) {
                    tvSongSelect.setText(title_list.size() + " bài hát");
                    tvListSongSelect.append(title + '\n');
                }
            }


        }


    }


    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật album...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String album_title_val = edtAlbumTitle.getText().toString().trim();
        final String album_artist_val = edtAlbumArtist.getText().toString().trim();
        final String album_info_val = edtAlbumInfo.getText().toString().trim();

        if (!TextUtils.isEmpty(album_title_val)
                && !TextUtils.isEmpty(album_artist_val)
                && !TextUtils.isEmpty(album_info_val)
                && !tvAlbumSelect.getText().equals("")) {

            mProgress.show();


            final DatabaseReference newAlbum = mDatabase.child("Albums").child(key_album);

            if (tvSongSelect.getText().equals("Không thay đổi")) {
                newAlbum.child("title").setValue(album_title_val);
                newAlbum.child("artist").setValue(album_artist_val);
                newAlbum.child("info").setValue(album_info_val);
                Toast.makeText(getApplicationContext(), "Cập nhật album thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();
            } else {

                newAlbum.child("info").setValue(album_info_val);
                newAlbum.child("title").setValue(album_title_val);
                newAlbum.child("artist").setValue(album_artist_val);
                newAlbum.child("albumArtPath").setValue(image_path_list.get(0));

                //xóa giá trị album_key trong thuộc tính albums của bài hát trong Songs
                for (int i=0; i <key_song.size(); i++){
                    mDatabase.child("Songs").child(key_song.get(i)).child("albums").child(key_album).removeValue();
                }

                DatabaseReference newAlbumSong = mDatabase.child("Albums-Songs").child(key_album);
                newAlbumSong.removeValue();

                DatabaseReference song = mDatabase.child("Songs");


                for (int i = 0; i < key_list.size(); i++) {
                    //thêm bài hát vào Albums-Songs
                    newAlbumSong.child(key_list.get(i)).child("title").setValue(title_list.get(i));
                    newAlbumSong.child(key_list.get(i)).child("artist").setValue(artist_list.get(i));
                    newAlbumSong.child(key_list.get(i)).child("albumImagePath").setValue(image_path_list.get(i));
                    newAlbumSong.child(key_list.get(i)).child("path").setValue(song_path_list.get(i));
                    newAlbumSong.child(key_list.get(i)).child("lyric").setValue(song_lyric_list.get(i));

                    //thêm giá trị album_key vào albums của danh sách các bài hát thuộc album trong Songs
                    song.child(key_list.get(i)).child("albums").child(key_album).setValue(key_album);
                }

                newAlbum.child("title").setValue(album_title_val);
                newAlbum.child("artist").setValue(album_artist_val);
                newAlbum.child("info").setValue(album_info_val);

                Toast.makeText(getApplicationContext(), "Cập nhật Album thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();
            }

        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn album và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
