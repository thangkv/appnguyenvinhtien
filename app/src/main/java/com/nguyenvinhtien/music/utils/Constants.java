package com.nguyenvinhtien.music.utils;


public class Constants {

    public static final String DEFAULT_SONG_ART_URL = "https://firebasestorage.googleapis.com/v0/b/solemusic-25ac1.appspot.com/o/SongArts%2Fdefault_cover_big.png?alt=media&token=844c2284-f4f7-4f0b-a6cf-ad7b46414323";
    public static final String DEFAULT_NAME_SONG_ART = "default_cover_big.png";

    public static final String SONG_PATH = "song_path";
    public static final String LIST_SONG = "list_song";
    public static final String LIST_PHOTO = "list_photo";

    public static final String SONG_ID = "song_id";//KVT
    public static final String SONG_TITLE = "song_title";//KVT

    public static final String SONG_POS = "position";
    public static final String NEWS_LINK = "news_link";
    public static final String NEWS_ID = "news_id";
    public static final String NEWS_TITLE = "news_title";
    public static final String POST_ID = "post_id";
    public static final String POST_TITLE = "post_title";
    public static final String VIDEO_ID = "video_id";
    public static final String VIDEO_TITLE = "video_title";
    public static final String PHOTO_ID = "photo_id";
    public static final String PHOTO_TITLE = "photo_title";
    public static final String PRIVACY_DESC = "privacy_desc";

    public static final int POSITION_PHOTO = 0;

    public static final String ACTION_COMPLETE_SONG = "com.kimvanthang.solemusic.ACTION_COMPLETE_SONG";
    public static final String ACTION_SWITCH_SONG = "com.kimvanthang.solemusic.ACTION_SWITCH_SONG";
    public static final String ACTION_PREV = "com.kimvanthang.solemusic.ACTION_PREV";
    public static final String ACTION_PLAY_PAUSE = "com.kimvanthang.solemusic.ACTION_PLAY_PAUSE";
    public static final String ACTION_NEXT = "com.kimvanthang.solemusic.ACTION_NEXT";
    public static final String ACTION_CHANGE_ALBUM_ART = "com.kimvanthang.solemusic.ACTION_CHANGE_ALBUM_ART";
    public static final String ACTION_CHANGE_SONG_LYRIC = "com.kimvanthang.solemusic.ACTION_CHANGE_SONG_LYRIC";

    public static final String ACTION_UPDATE_PLAY_STATUS = "com.kimvanthang.solemusic.ACTION_UPDATE_PlAY_STATUS";
    public static final String ACTION_UPDATE_PLAY_PAUSE_BUTTON = "com.kimvanthang.solemusic.ACTION_UPDATE_PlAY_PAUSE_BUTTON";

    public static final String LEGACY_SERVER_KEY ="AIzaSyB80dpZXmKxQjDQ6Ap78u58-YkzwGnVJMw";
    public static final String FCM_PUSH_URL ="https://fcm.googleapis.com/fcm/send";

    public static final String NOTI = "noti";
}
