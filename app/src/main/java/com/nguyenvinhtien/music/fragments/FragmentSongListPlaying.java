package com.nguyenvinhtien.music.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.adapter.song.SongListPlayingAdapter;
import com.nguyenvinhtien.music.models.Song;


import java.util.ArrayList;


public class FragmentSongListPlaying extends Fragment {

    public static final String KEY_SONG_LIST = "key_song_list";

    ArrayList<Song> mLstSongPlaying;
    RecyclerView mRvListSongPlaying;
    SongListPlayingAdapter mAdapter;
    View mView;


    public FragmentSongListPlaying() {
        // Required empty public constructor
    }

    public static FragmentSongListPlaying newInstance(ArrayList<Song> mData) {
        FragmentSongListPlaying fragment = new FragmentSongListPlaying();
        Bundle args = new Bundle();
        args.putSerializable(KEY_SONG_LIST, mData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLstSongPlaying = (ArrayList<Song>) getArguments().getSerializable(KEY_SONG_LIST);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.frament_song_list_playing, container, false);
        initControls();
        showListSongPlaying();



        return mView;
    }

    private void showListSongPlaying() {
        mAdapter = new SongListPlayingAdapter(getActivity(), mLstSongPlaying);
        mRvListSongPlaying.setAdapter(mAdapter);
    }

    private void initControls() {
        mRvListSongPlaying = (RecyclerView) mView.findViewById(R.id.rv_song_list_playing);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity()) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };

        mRvListSongPlaying.setLayoutManager(layoutManager);

    }

}
