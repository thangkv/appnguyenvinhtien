package com.nguyenvinhtien.music.activities.update_album;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

import java.util.ArrayList;
import java.util.List;

public class SelectSongForAlbumActivity extends AppCompatActivity {

    public static final String KIEM_TRA_ACTIVY_GOI = "kiemtra";

    DatabaseReference mDatabase;
    RecyclerView listSong;
    FirebaseRecyclerAdapter<Song, SelectSongRecyclerViewHolder> adapter;
    List<String> listSongKey;
    List<String> listSongTitle;
    List<String> listSongArtist;
    List<String> listSongImagePath;
    List<String> listSongPath;
    List<String> listSongLyric;

    String kiemtra;

    Button btnOk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_song_for_album);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_song);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        listSongKey = new ArrayList<>();
        listSongTitle = new ArrayList<>();
        listSongArtist = new ArrayList<>();
        listSongImagePath = new ArrayList<>();
        listSongPath = new ArrayList<>();
        listSongLyric = new ArrayList<>();

        kiemtra = getIntent().getStringExtra(KIEM_TRA_ACTIVY_GOI);

        initControls();
        initEvents();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Songs");

        adapter = new FirebaseRecyclerAdapter<Song, SelectSongRecyclerViewHolder>(Song.class, R.layout.item_select_song_for_album, SelectSongRecyclerViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(final SelectSongRecyclerViewHolder viewHolder, final Song model, final int position) {
                viewHolder.tvTitle.setText(model.getTitle());
                viewHolder.tvArtist.setText(model.getArtist());
                viewHolder.tvTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewHolder.checkBox.setChecked(!viewHolder.checkBox.isChecked());
                    }
                });


                viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked == true) {
                            listSongKey.add(getRef(position).getKey());
                            listSongTitle.add(model.getTitle());
                            listSongArtist.add(model.getArtist());
                            listSongImagePath.add(model.getAlbumImagePath());
                            listSongPath.add(model.getPath());
                            listSongLyric.add(model.getLyric());
                        } else {
                            listSongKey.remove(getRef(position).getKey());
                            listSongTitle.remove(model.getTitle());
                            listSongArtist.remove(model.getArtist());
                            listSongImagePath.remove(model.getAlbumImagePath());
                            listSongPath.remove(model.getPath());
                            listSongLyric.remove(model.getLyric());
                        }
                    }
                });

            }

        };

        adapter.notifyDataSetChanged();
        listSong.setAdapter(adapter);


        if (AppController.getInstance().checkInternetState() == false) {
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }

    }

    public static class SelectSongRecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvArtist;
        public CheckBox checkBox;


        public SelectSongRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_song);
            tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_song);
            checkBox = (CheckBox) itemView.findViewById(R.id.cb_select_song);

        }

    }

    private void initControls() {
        listSong = (RecyclerView) findViewById(R.id.rv_song_list_select);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listSong.setLayoutManager(layoutManager);
        btnOk = (Button) findViewById(R.id.btn_ok);

    }

    private void initEvents() {
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listSongKey.size() != 0) {
                    if (kiemtra.equals("UpdateDataAlbum")) {
                        Intent i = new Intent(SelectSongForAlbumActivity.this, UpdateDataAlbumActivity.class);
                        i.putStringArrayListExtra("LIST_KEY_SONG", (ArrayList<String>) listSongKey);
                        i.putStringArrayListExtra("LIST_TITLE_SONG", (ArrayList<String>) listSongTitle);
                        i.putStringArrayListExtra("LIST_ARTIST_SONG", (ArrayList<String>) listSongArtist);
                        i.putStringArrayListExtra("LIST_IMAGE_PATH_SONG", (ArrayList<String>) listSongImagePath);
                        i.putStringArrayListExtra("LIST_PATH_SONG", (ArrayList<String>) listSongPath);
                        i.putStringArrayListExtra("LIST_LYRIC_SONG", (ArrayList<String>) listSongLyric);
                        setResult(RESULT_OK, i);
                        finish();
                    } else {
                        Intent i = new Intent(SelectSongForAlbumActivity.this, AddAlbumActivity.class);
                        i.putStringArrayListExtra("LIST_KEY_SONG", (ArrayList<String>) listSongKey);
                        i.putStringArrayListExtra("LIST_TITLE_SONG", (ArrayList<String>) listSongTitle);
                        i.putStringArrayListExtra("LIST_ARTIST_SONG", (ArrayList<String>) listSongArtist);
                        i.putStringArrayListExtra("LIST_IMAGE_PATH_SONG", (ArrayList<String>) listSongImagePath);
                        i.putStringArrayListExtra("LIST_PATH_SONG", (ArrayList<String>) listSongPath);
                        i.putStringArrayListExtra("LIST_LYRIC_SONG", (ArrayList<String>) listSongLyric);
                        setResult(RESULT_OK, i);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Cần chọn danh sách bài hát", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
