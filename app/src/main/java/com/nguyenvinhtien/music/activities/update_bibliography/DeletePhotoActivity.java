package com.nguyenvinhtien.music.activities.update_bibliography;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class DeletePhotoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_PHOTO_REQUEST = 6;
    TextView tvPhotoSelect;
    Button btnSelectPhoto, btnUpdatePhoto;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_photo;
    String title_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_delete_photo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_delete_photo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvPhotoSelect = (TextView) findViewById(R.id.tv_photo_select);
        btnSelectPhoto = (Button) findViewById(R.id.btn_select_photo);
        btnUpdatePhoto = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnSelectPhoto.setOnClickListener(this);
        btnUpdatePhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_photo:
                Intent selectVideoIntent = new Intent(DeletePhotoActivity.this, SelectPhotoToUpdateActivity.class);
                startActivityForResult(selectVideoIntent, SELECT_PHOTO_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO_REQUEST && resultCode == RESULT_OK) {
            key_photo = data.getStringExtra("KEY_PHOTO");
            title_photo = data.getStringExtra("TITLE_PHOTO");
            updateUI();

        }

    }

    private void updateUI() {
        if (title_photo != null) {
            tvPhotoSelect.setVisibility(View.VISIBLE);
            tvPhotoSelect.setText(title_photo);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Xóa hình ảnh...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
        if (!tvPhotoSelect.getText().equals("")){


            mProgress.show();

            DatabaseReference photo = mDatabase.child("Photos").child(key_photo);
            photo.removeValue();

            DatabaseReference photo_comment = mDatabase.child("Photo-Comments").child(key_photo);
            photo_comment.removeValue();

            StorageReference imagePath = mStorage.child("Photos").child(key_photo);
            imagePath.delete();

            Toast.makeText(getApplicationContext(), "Xóa hình ảnh thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();
        } else {
            Toast.makeText(this, "Cần chọn hình ảnh muốn xóa", Toast.LENGTH_SHORT).show();
        }
    }
}
