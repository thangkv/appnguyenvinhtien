package com.nguyenvinhtien.music.adapter.song;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.fragments.FragmentPlay;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by KimVanThang on 3/16/2017.
 */

public class SongOnlineListSuggestAdapter extends RecyclerView.Adapter<SongOnlineListSuggestAdapter.ViewHolderSong> {


    Activity mContext;
    ArrayList<Song> mData;

    LayoutInflater mLayoutInflater;

    DownloadManager downloadManager;
    long enqueue;


    public SongOnlineListSuggestAdapter(Activity mContext, ArrayList<Song> mData) {
        this.mContext = mContext;
        this.mData = mData;

        mLayoutInflater = LayoutInflater.from(mContext);

        mContext.registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }

    //Broadcast lắng nghe download thành công
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                Toast.makeText(mContext, "Tải về thành công", Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public SongOnlineListSuggestAdapter.ViewHolderSong onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_online_song, null);
        SongOnlineListSuggestAdapter.ViewHolderSong holder = new SongOnlineListSuggestAdapter.ViewHolderSong(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final ViewHolderSong holder, final int position) {

        Song item = mData.get(position);
        holder.setId(position);
        String path = item.getAlbumImagePath();
        if (!path.equals(Constants.DEFAULT_SONG_ART_URL)){
            Glide.with(mContext).load(path).placeholder(R.drawable.song).into(holder.imgAlbum);
        } else {
            holder.imgAlbum.setImageResource(R.drawable.default_cover_big);
        }
        holder.tvTitle.setText(item.getTitle());
        holder.tvArtist.setText(item.getArtist());
        holder.tvOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, holder.tvOption);
                popupMenu.inflate(R.menu.menu_song);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item_download:
                                Toast.makeText(mContext, "Đang tải bài hát " + mData.get(position).getTitle(), Toast.LENGTH_SHORT).show();

                                File direct = new File(Environment.getExternalStorageDirectory()
                                        + "/Nguyễn Vĩnh Tiến - Nhạc");

                                if (!direct.exists()) {
                                    direct.mkdirs();
                                }

                                downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
                                //Yeu cau download
                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(mData.get(position).getPath()));

                                //cho phep down bang wifi hoac 3G
                                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                                        //dat ten cho file down ve thanh "ten_bai_hat.mp3"
                                        .setTitle(mData.get(position).getTitle() + ".mp3")
                                        //dat ten cho phan mieu ta thanh "ten_bai_hat-ten_ca_si"
                                        .setDescription(mData.get(position).getTitle() + "-" + mData.get(position).getArtist())
                                        //thu muc luu bai hat Public de cho cac app khac co the truy cap
                                        .setDestinationInExternalPublicDir("/Nguyễn Vĩnh Tiến - Nhạc", mData.get(position).getTitle() + ".mp3");

                                //Dua request vao hang doi
                                enqueue = downloadManager.enqueue(request);
                                break;

                            case R.id.item_share:
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mData.get(position).getPath());
                                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolderSong extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView tvTitle;
            TextView tvArtist;
            ImageView imgAlbum;
            TextView tvOption;


            int id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public ViewHolderSong(View itemView) {
                super(itemView);

                tvTitle = (TextView) itemView.findViewById(R.id.tv_song_name_play);
                tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_song_play);
                imgAlbum = (ImageView) itemView.findViewById(R.id.img_album_song_play);
                tvOption = (TextView) itemView.findViewById(R.id.tv_option);


                itemView.setOnClickListener(this);
            }

        @Override
        public void onClick(View v) {
            if (AppController.getInstance().checkInternetState() == false){
                Toast.makeText(mContext, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(mContext, PlayMusicActivity.class);
                intent.putExtra(Constants.SONG_PATH, mData.get(id).getPath());
                intent.putExtra(Constants.SONG_POS, id);
                intent.putExtra(Constants.LIST_SONG, mData);
                intent.putExtra(PlayMusicActivity.IS_PLAYING, false);
                intent.putExtra(FragmentPlay.TYPE, mData.get(id).getType());
                mContext.startActivity(intent);
                mContext.overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
            }
        }



    }

    @Override
    public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        super.unregisterAdapterDataObserver(observer);
        mContext.unregisterReceiver(receiver);

    }

}
