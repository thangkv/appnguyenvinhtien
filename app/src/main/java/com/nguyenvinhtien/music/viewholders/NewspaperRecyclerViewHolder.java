package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;

/**
 * Created by KimVanThang on 4/22/2017.
 */

public class NewspaperRecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView tvTitle;
    public TextView tvSource;
    public TextView tvOption;
    public ImageView imgImage;



    public NewspaperRecyclerViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_title_news);
        tvSource = (TextView) itemView.findViewById(R.id.tv_source_news);
        imgImage = (ImageView) itemView.findViewById(R.id.img_news);
        tvOption = (TextView) itemView.findViewById(R.id.tv_option);

    }


}
