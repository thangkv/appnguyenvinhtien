package com.nguyenvinhtien.music.activities.update_album;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

import java.util.ArrayList;
import java.util.List;

public class DeleteAlbumActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_ALBUM_REQUEST = 5;
    TextView tvAlbumSelect;
    Button btnSelectAlbum, btnUpdateAlbum;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_album;
    String title_album;

    List<String> key_song = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_delete_album);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_delete_album);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvAlbumSelect = (TextView) findViewById(R.id.tv_album_select);
        btnSelectAlbum = (Button) findViewById(R.id.btn_select_album);
        btnUpdateAlbum = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnSelectAlbum.setOnClickListener(this);
        btnUpdateAlbum.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_album:
                Intent selectAlbumIntent = new Intent(DeleteAlbumActivity.this, SelectAlbumToUpdateActivity.class);
                startActivityForResult(selectAlbumIntent, SELECT_ALBUM_REQUEST);
                break;

            case R.id.btn_update:

                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_ALBUM_REQUEST && resultCode == RESULT_OK) {
            key_album = data.getStringExtra("KEY_ALBUM");
            title_album = data.getStringExtra("TITLE_ALBUM");

            //thêm key_song cũ của album vào list key_song để xóa albums của Songs không nằm trong album này nữa
            mDatabase.child("Albums-Songs").child(key_album).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String keysong = dataSnapshot.getKey().toString();
                    key_song.add(keysong);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            updateUI();

        }

    }

    private void updateUI() {
        if (title_album != null) {
            tvAlbumSelect.setVisibility(View.VISIBLE);
            tvAlbumSelect.setText(title_album);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Xóa album...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        if (!tvAlbumSelect.getText().equals("")){

            mProgress.show();

            //xóa giá trị album_key trong thuộc tính albums của bài hát trong Songs
            for (int i=0; i <key_song.size(); i++){
                mDatabase.child("Songs").child(key_song.get(i)).child("albums").child(key_album).removeValue();
            }

            DatabaseReference album = mDatabase.child("Albums").child(key_album);
            DatabaseReference album_song = mDatabase.child("Albums-Songs").child(key_album);
            album.removeValue();
            album_song.removeValue();


            Toast.makeText(getApplicationContext(), "Xóa album thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();
        } else {
            Toast.makeText(this, "Cần chọn album muốn xóa", Toast.LENGTH_SHORT).show();
        }
    }
}
