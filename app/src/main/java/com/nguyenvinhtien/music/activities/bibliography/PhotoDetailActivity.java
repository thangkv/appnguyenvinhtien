package com.nguyenvinhtien.music.activities.bibliography;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.adapter.bibliography.FullScreenImageAdapter;
import com.nguyenvinhtien.music.models.Photo;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import java.io.File;
import java.util.ArrayList;

public class PhotoDetailActivity extends AppCompatActivity {


    ArrayList<Photo> mLstPhoto = new ArrayList<>();
    TextView tvTitleToolbar;
    ViewPager viewPager;
    FullScreenImageAdapter adapter;
    Button btnComment;
    String photoID;

    String photoPath;
    String photoTitle;

    int position;

    DatabaseReference mDatabase;

    DownloadManager downloadManager;
    long enqueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_photo_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail_photo);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        position = intent.getIntExtra(String.valueOf(Constants.POSITION_PHOTO), 0);
        photoID = intent.getStringExtra(Constants.PHOTO_ID);

        mLstPhoto = (ArrayList<Photo>) intent.getExtras().getSerializable(Constants.LIST_PHOTO);

        tvTitleToolbar = (TextView) findViewById(R.id.tv_news);
        viewPager = (ViewPager) findViewById(R.id.photo_pager);

        adapter = new FullScreenImageAdapter(this, mLstPhoto);
        adapter.notifyDataSetChanged();
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                String tenanh = mLstPhoto.get(position).getTitle();
                tvTitleToolbar.setText(tenanh);
                photoID = mLstPhoto.get(position).getId();
                photoPath = mLstPhoto.get(position).getPath();
                photoTitle = mLstPhoto.get(position).getTitle();


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mDatabase.child("Photos").child(photoID).child("title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                photoTitle = (String) dataSnapshot.getValue();
                tvTitleToolbar.setText(photoTitle);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("Photos").child(photoID).child("path").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                photoPath = (String) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        btnComment = (Button) findViewById(R.id.btn_comment);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.getInstance().checkInternetState()==true){
                    Intent intent = new Intent(PhotoDetailActivity.this, CommentPhotoActivity.class);
                    intent.putExtra(Constants.PHOTO_ID, photoID);
                    intent.putExtra(Constants.PHOTO_TITLE, photoTitle);
                    startActivity(intent);
                } else {
                    Toast.makeText(PhotoDetailActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                }

            }
        });


        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        if (AppController.getInstance().checkInternetState() == false) {
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();

        }

    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                Toast.makeText(context, "Tải về thành công!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.item_download:
                Toast.makeText(this, "Đang tải hình ảnh...", Toast.LENGTH_SHORT).show();

                File direct = new File(Environment.getExternalStorageDirectory()
                        + "/Nguyễn Vĩnh Tiến - Hình ảnh");

                if (!direct.exists()) {
                    direct.mkdirs();
                }

                downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                //Yeu cau download
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(photoPath));

                //cho phep down bang wifi hoac 3G
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                        //dat ten cho file down ve thanh "ten_anh"
                        .setTitle(photoTitle)
                        //dat ten cho phan mieu ta thanh "ten_anh"
                        .setDescription(photoTitle)
                        //thu muc luu anh Public de cho cac app khac co the truy cap
                        .setDestinationInExternalPublicDir("/Nguyễn Vĩnh Tiến - Hình ảnh", photoTitle + ".jpeg");

                //Dua request vao hang doi
                enqueue = downloadManager.enqueue(request);
                break;

            case R.id.item_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, photoPath);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
