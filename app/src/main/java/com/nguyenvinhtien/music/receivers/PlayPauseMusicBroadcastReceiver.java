package com.nguyenvinhtien.music.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;


public class PlayPauseMusicBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PlayMusicActivity musicActivity = (PlayMusicActivity) AppController.getInstance().getPlayMusicActivity();
        PlayMusicService musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        if (musicActivity != null) {
            musicActivity.playPauseMusic();
            Log.d("playpause", "test");
        } else {
            musicService.playPauseMusic();
        }
        musicService.showNotification(true);

        Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
        context.sendBroadcast(intent1);

        Intent intent6 = new Intent(Constants.ACTION_UPDATE_PLAY_PAUSE_BUTTON);
        context.sendBroadcast(intent6);

    }
}
