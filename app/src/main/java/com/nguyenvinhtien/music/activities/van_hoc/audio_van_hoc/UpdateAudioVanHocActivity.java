package com.nguyenvinhtien.music.activities.van_hoc.audio_van_hoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.update_audio_vanhoc.AddAudioVanHocActivity;
import com.nguyenvinhtien.music.activities.update_audio_vanhoc.DeleteAudioVanHocActivity;
import com.nguyenvinhtien.music.activities.update_audio_vanhoc.UpdateDataAudioVanHocActivity;
import com.nguyenvinhtien.music.activities.update_song.AddSongActivity;
import com.nguyenvinhtien.music.activities.update_song.DeleteSongActivity;
import com.nguyenvinhtien.music.activities.update_song.UpdateDataSongActivity;
import com.nguyenvinhtien.music.activities.update_song.UpdateGiaiThuongActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateAudioVanHocActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddSong;
    Button btnUpdateSong;
    Button btnDeleteSong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_audio_van_hoc);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_song);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnUpdateSong = (Button) findViewById(R.id.btn_UpdateSong);
        btnAddSong = (Button) findViewById(R.id.btn_AddSong);
        btnDeleteSong = (Button)findViewById(R.id.btn_DeleteSong);

    }

    public void initEvents(){
        btnUpdateSong.setOnClickListener(this);
        btnAddSong.setOnClickListener(this);
        btnDeleteSong.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddSong:
                Intent intent_add_song = new Intent(getApplicationContext(), AddAudioVanHocActivity.class);
                startActivity(intent_add_song);
                break;
            case R.id.btn_UpdateSong:
                Intent intent_update_song = new Intent(getApplicationContext(), UpdateDataAudioVanHocActivity.class);
                startActivity(intent_update_song);
                break;
            case R.id.btn_DeleteSong:
                Intent intent_delete_song = new Intent(getApplicationContext(), DeleteAudioVanHocActivity.class);
                startActivity(intent_delete_song);
                break;

        }
    }
}
