package com.nguyenvinhtien.music.activities.hot_video;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.activities.update_video.AddVideoActivity;
import com.nguyenvinhtien.music.activities.update_video.DeleteVideoActivity;
import com.nguyenvinhtien.music.activities.update_video.UpdateDataVideoActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateVideoActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddVideo;
    Button btnUpdateVideo;
    Button btnDeleteVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_video);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_video);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnUpdateVideo = (Button) findViewById(R.id.btn_UpdateVideo);
        btnAddVideo = (Button) findViewById(R.id.btn_AddVideo);
        btnDeleteVideo = (Button)findViewById(R.id.btn_DeleteVideo);
    }

    public void initEvents(){
        btnUpdateVideo.setOnClickListener(this);
        btnAddVideo.setOnClickListener(this);
        btnDeleteVideo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddVideo:
                Intent intent_add_video = new Intent(getApplicationContext(), AddVideoActivity.class);
                startActivity(intent_add_video);
                break;
            case R.id.btn_UpdateVideo:
                Intent intent_update_video = new Intent(getApplicationContext(), UpdateDataVideoActivity.class);
                startActivity(intent_update_video);
                break;
            case R.id.btn_DeleteVideo:
                Intent intent_delete_video = new Intent(getApplicationContext(), DeleteVideoActivity.class);
                startActivity(intent_delete_video);
                break;

        }
    }
}
