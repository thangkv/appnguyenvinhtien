package com.nguyenvinhtien.music.activities.kien_truc.bai_viet;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

public class PostDetailActivity extends AppCompatActivity {

    TextView tvTitle, tvContent;
    ImageView imageView;
    Button btnComment;
    String newsID;
    String postTitle;

    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_post_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail_news);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        newsID = intent.getStringExtra(Constants.POST_ID);
        postTitle = intent.getStringExtra(Constants.POST_TITLE);


        tvTitle = (TextView)findViewById(R.id.tv_title_post);
        imageView = (ImageView) findViewById(R.id.image_post);
        tvContent = (TextView) findViewById(R.id.tv_content_post);
        btnComment = (Button) findViewById(R.id.btn_comment);

        mDatabase.child("Posts").child(newsID).child("title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String tieude = (String) dataSnapshot.getValue();
                tvTitle.setText(tieude);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("Posts").child(newsID).child("content").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String noidung = (String) dataSnapshot.getValue();
                tvContent.setText(noidung);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("Posts").child(newsID).child("imagePath").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String linkAnh = (String) dataSnapshot.getValue();
                try{
                    Glide.with(PostDetailActivity.this).load(linkAnh).into(imageView);
                } catch (Exception e){

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnComment = (Button)findViewById(R.id.btn_comment);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.getInstance().checkInternetState() == true){
                    Intent intent = new Intent(PostDetailActivity.this, CommentPostActivity.class);
                    intent.putExtra(Constants.POST_ID, newsID);
                    intent.putExtra(Constants.POST_TITLE, postTitle);
                    startActivity(intent);
                } else {
                    Toast.makeText(PostDetailActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
