package com.nguyenvinhtien.music.activities.update_video;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateDataVideoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_VIDEO_REQUEST = 6;
    private static final int IMAGE_GALLERY_REQUEST = 2;

    TextView tvVideoSelect;
    Button btnSelectVideo, btnUpdateVideo;
    EditText edtVideoTitle, edtVideoArtist, edtVideoPath, edtVideoDesc;
    RelativeLayout rlVideoDetail;

    ImageButton ibSelectAlbumArt;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_video;
    String title_video;
    String artist_video;
    String path_video;
    String desc_video;
    String image_path_video;

    Uri imageUri = null;
    Uri imageURL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_data_video);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_data_video);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvVideoSelect = (TextView) findViewById(R.id.tv_video_select);
        btnSelectVideo = (Button) findViewById(R.id.btn_select_video);
        btnUpdateVideo = (Button) findViewById(R.id.btn_update);
        edtVideoTitle = (EditText) findViewById(R.id.edt_video_title);
        edtVideoArtist = (EditText) findViewById(R.id.edt_video_artist);
        edtVideoPath = (EditText) findViewById(R.id.edt_video_path);
        edtVideoDesc = (EditText)findViewById(R.id.edt_video_desc);
        rlVideoDetail = (RelativeLayout) findViewById(R.id.rl_video_detail);
        ibSelectAlbumArt = (ImageButton) findViewById(R.id.image_select);
    }

    public void initEvents() {
        btnSelectVideo.setOnClickListener(this);
        btnUpdateVideo.setOnClickListener(this);
        ibSelectAlbumArt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_video:
                Intent selectVideoIntent = new Intent(UpdateDataVideoActivity.this, SelectVideoToUpdateActivity.class);
                startActivityForResult(selectVideoIntent, SELECT_VIDEO_REQUEST);
                break;

            case R.id.image_select:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_VIDEO_REQUEST && resultCode == RESULT_OK) {
            key_video = data.getStringExtra("KEY_VIDEO");
            title_video = data.getStringExtra("TITLE_VIDEO");
            artist_video = data.getStringExtra("ARTIST_VIDEO");
            path_video = data.getStringExtra("PATH_VIDEO");
            desc_video = data.getStringExtra("DESC_VIDEO");
            image_path_video = data.getStringExtra("IMAGE_PATH_VIDEO");
            updateUI();

        }

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK) {
            imageUri = data.getData();
            ibSelectAlbumArt.setImageURI(imageUri);
        }

    }

    private void updateUI() {
        if (title_video != null && artist_video != null
                && path_video != null && image_path_video != null) {
            tvVideoSelect.setVisibility(View.VISIBLE);
            tvVideoSelect.setText(title_video);
            rlVideoDetail.setVisibility(View.VISIBLE);
            Glide.with(this).load(image_path_video).into(ibSelectAlbumArt);
            edtVideoTitle.setText(title_video);
            edtVideoArtist.setText(artist_video);
            edtVideoPath.setText(path_video);
            edtVideoDesc.setText(desc_video);
        }
    }


    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật video...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String video_title_val = edtVideoTitle.getText().toString().trim();
        final String video_artist_val = edtVideoArtist.getText().toString().trim();
        final String video_path_val = edtVideoPath.getText().toString().trim();
        final String video_description_val = edtVideoDesc.getText().toString().trim();

        if (!TextUtils.isEmpty(video_title_val)
                && !TextUtils.isEmpty(video_artist_val)
                && !TextUtils.isEmpty(video_path_val)
                && !TextUtils.isEmpty(video_description_val)
                && !tvVideoSelect.getText().equals("")) {

            mProgress.show();

            final DatabaseReference newVideo = mDatabase.child("Videos").child(key_video);

            if (imageUri == null){
                newVideo.child("title").setValue(video_title_val);
                newVideo.child("artist").setValue(video_artist_val);
                newVideo.child("path").setValue(video_path_val);
                newVideo.child("description").setValue(video_description_val);
                Toast.makeText(getApplicationContext(), "Cập nhật video thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();
            } else {
                StorageReference imagePath = mStorage.child("VideoArts").child(key_video);

                imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageURL = taskSnapshot.getDownloadUrl();
                        newVideo.child("imagePath").setValue(imageURL.toString());
                        newVideo.child("title").setValue(video_title_val);
                        newVideo.child("artist").setValue(video_artist_val);
                        newVideo.child("path").setValue(video_path_val);
                        newVideo.child("description").setValue(video_description_val);
                        Toast.makeText(getApplicationContext(), "Cập nhật video thành công!", Toast.LENGTH_SHORT).show();
                        mProgress.dismiss();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn video và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
