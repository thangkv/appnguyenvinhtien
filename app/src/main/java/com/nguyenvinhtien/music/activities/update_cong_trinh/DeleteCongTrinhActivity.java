package com.nguyenvinhtien.music.activities.update_cong_trinh;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.update_post.DeletePostActivity;
import com.nguyenvinhtien.music.activities.update_post.SelectPostToUpdateActivity;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

import java.util.ArrayList;

public class DeleteCongTrinhActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_POST_REQUEST = 6;
    TextView tvPostSelect;
    Button btnSelectPost, btnUpdatePost;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_post;
    String title_post;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_delete_cong_trinh);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_delete_post);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvPostSelect = (TextView) findViewById(R.id.tv_post_select);
        btnSelectPost = (Button) findViewById(R.id.btn_select_post);
        btnUpdatePost = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnSelectPost.setOnClickListener(this);
        btnUpdatePost.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_post:
                Intent selectVideoIntent = new Intent(DeleteCongTrinhActivity.this, SelectCongTrinhToUpdateActivity.class);
                startActivityForResult(selectVideoIntent, SELECT_POST_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false) {
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_POST_REQUEST && resultCode == RESULT_OK) {
            key_post = data.getStringExtra("KEY_POST");
            title_post = data.getStringExtra("TITLE_POST");
            updateUI();

        }

    }

    private void updateUI() {
        if (title_post != null) {
            tvPostSelect.setVisibility(View.VISIBLE);
            tvPostSelect.setText(title_post);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Xóa công trình...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
        if (!tvPostSelect.getText().equals("")) {


            mProgress.show();

            DatabaseReference post = mDatabase.child("Congtrinhs").child(key_post);
            post.removeValue();

            DatabaseReference post_comment = mDatabase.child("CongTrinh-Comments").child(key_post);
            post_comment.removeValue();


            //lấy key ảnh công trình để xóa trên storage
            final DatabaseReference congtrinh_anh = mDatabase.child("Congtrinhs-Anhs").child(key_post);
            final StorageReference imagePath = mStorage.child("CongTrinhAnhs").child(key_post);
            congtrinh_anh.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String key_photo = (String) dataSnapshot.getKey();
                    imagePath.child(key_photo).delete();

                    congtrinh_anh.removeValue();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            Toast.makeText(getApplicationContext(), "Xóa công trình thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();
        } else {
            Toast.makeText(this, "Cần chọn công trình muốn xóa", Toast.LENGTH_SHORT).show();
        }
    }
}
