package com.nguyenvinhtien.music.models;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/19/2017.
 */

public class Video implements Serializable {

    private String id;
    private String title;
    private String artist;
    String imagePath;
    String description;

    private String path;

    public static DatabaseReference mData = FirebaseDatabase.getInstance().getReference();

    private static ArrayList<Video> listVideo;


    public Video() {

    }

    public Video(String id, String title, String artist, String desc, String imagePath, String path) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.imagePath = imagePath;
        this.description = desc;
        this.path = path;
    }

    public Video(String id, String title, String artist, String imagePath, String path) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.imagePath = imagePath;
        this.path = path;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public static ArrayList<Video> getListVideo() {
        if (listVideo == null) {
            listVideo = new ArrayList<>();
            mData.child("Videos").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String artist = (String) dataSnapshot.child("artist").getValue();
                    String imagePath = (String) dataSnapshot.child("imagePath").getValue();
                    String path = (String) dataSnapshot.child("path").getValue();
                    String description = (String) dataSnapshot.child("description").getValue();

                    Video video = new Video(id, title, artist, description, imagePath, path);
                    listVideo.add(video);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        return listVideo;
    }


}


