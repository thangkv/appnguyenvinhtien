package com.nguyenvinhtien.music.activities.update_post;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.models.Post;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class SelectPostToUpdateActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    RecyclerView listPost;
    FirebaseRecyclerAdapter<Post, SelectPostToUpdateActivity.SelectPostRecyclerViewHolder> adapter;
    String postKey = null;
    String postTitle = null;
    String postImagePath = null;
    String postContent = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_post_to_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_post_to_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Posts");
        adapter = new FirebaseRecyclerAdapter<Post, SelectPostToUpdateActivity.SelectPostRecyclerViewHolder>(Post.class, R.layout.item_select_post, SelectPostToUpdateActivity.SelectPostRecyclerViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(final SelectPostToUpdateActivity.SelectPostRecyclerViewHolder viewHolder, final Post model, final int position) {
                viewHolder.tvTitle.setText(model.getTitle());
                viewHolder.tvContent.setText(model.getContent());

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postKey = getRef(position).getKey();
                        postTitle = model.getTitle();
                        postImagePath = model.getImagePath();
                        postContent = model.getContent();
                        viewHolder.itemView.setBackgroundColor(Color.rgb(66,110,180));
                        Intent i = new Intent(SelectPostToUpdateActivity.this, UpdateDataPostActivity.class);
                        i.putExtra("KEY_POST", postKey);
                        i.putExtra("TITLE_POST", postTitle);
                        i.putExtra("IMAGE_PATH_POST", postImagePath);
                        i.putExtra("CONTENT_POST", postContent);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });


            }


        };

        initControls();
        initEvents();
        adapter.notifyDataSetChanged();
        listPost.setAdapter(adapter);

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

    public static class SelectPostRecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvContent;


        public SelectPostRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_post);
            tvContent = (TextView)itemView.findViewById(R.id.tv_content_post) ;

        }

    }

    private void initControls() {
        listPost = (RecyclerView) findViewById(R.id.rv_post_list_select);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        listPost.setLayoutManager(layoutManager);

    }

    private void initEvents() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
