package com.nguyenvinhtien.music.activities.kien_truc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.cong_trinh.UpdateCongTrinhActivity;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.UpdateAudioKienTrucActivity;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.UpdatePostActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateKienTrucActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnTho;
    Button btnTruyen;
    Button btnAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_kien_truc);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_privacy);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnTho = (Button) findViewById(R.id.btn_Tho);
        btnTruyen = (Button) findViewById(R.id.btn_Truyen);
        btnAudio = (Button)findViewById(R.id.btn_Audio_Vanhoc);

    }

    public void initEvents(){
        btnTho.setOnClickListener(this);
        btnTruyen.setOnClickListener(this);
        btnAudio.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Tho:
                Intent intent_hot_video_privacy = new Intent(getApplicationContext(), UpdatePostActivity.class);
                startActivity(intent_hot_video_privacy);
                break;
            case R.id.btn_Truyen:
                Intent intent_hot_photo_privacy = new Intent(getApplicationContext(), UpdateCongTrinhActivity.class);
                startActivity(intent_hot_photo_privacy);
                break;
            case R.id.btn_Audio_Vanhoc:
                Intent intent_hot_news_privacy = new Intent(getApplicationContext(), UpdateAudioKienTrucActivity.class);
                startActivity(intent_hot_news_privacy);
                break;
        }
    }
}
