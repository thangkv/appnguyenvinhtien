package com.nguyenvinhtien.music.activities.hot_album;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.activities.update_album.AddAlbumActivity;
import com.nguyenvinhtien.music.activities.update_album.DeleteAlbumActivity;
import com.nguyenvinhtien.music.activities.update_album.UpdateDataAlbumActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateAlbumActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddAlbum;
    Button btnUpdateAlbum;
    Button btnDeleteAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_album);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_album);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnUpdateAlbum = (Button) findViewById(R.id.btn_UpdateAlbum);
        btnAddAlbum = (Button) findViewById(R.id.btn_AddAlbum);
        btnDeleteAlbum = (Button)findViewById(R.id.btn_DeleteAlbum);
    }

    public void initEvents(){
        btnUpdateAlbum.setOnClickListener(this);
        btnAddAlbum.setOnClickListener(this);
        btnDeleteAlbum.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddAlbum:
                Intent intent_add_album = new Intent(getApplicationContext(), AddAlbumActivity.class);
                startActivity(intent_add_album);
                break;
            case R.id.btn_UpdateAlbum:
                Intent intent_update_album = new Intent(getApplicationContext(), UpdateDataAlbumActivity.class);
                startActivity(intent_update_album);
                break;
            case R.id.btn_DeleteAlbum:
                Intent intent_delete_album = new Intent(getApplicationContext(), DeleteAlbumActivity.class);
                startActivity(intent_delete_album);
                break;

        }
    }
}
