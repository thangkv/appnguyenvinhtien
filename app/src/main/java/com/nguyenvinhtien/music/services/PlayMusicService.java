package com.nguyenvinhtien.music.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.NotificationTarget;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.bibliography.BibliographyActivity;
import com.nguyenvinhtien.music.activities.bibliography.BibliographyDetailActivity;
import com.nguyenvinhtien.music.activities.hit_song.HitSongActivity;
import com.nguyenvinhtien.music.activities.hit_song.SuggestSongListActivity;
import com.nguyenvinhtien.music.activities.hot_album.AlbumOnlineListActivity;
import com.nguyenvinhtien.music.activities.hot_album.HotAlbumActivity;
import com.nguyenvinhtien.music.activities.hot_album.SuggestAlbumActivity;
import com.nguyenvinhtien.music.activities.hot_video.HotVideoActivity;
import com.nguyenvinhtien.music.activities.hot_video.SuggestVideoActivity;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.SuggestAudioKienTrucListActivity;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.PostSuggestListActivity;
import com.nguyenvinhtien.music.activities.kien_truc.cong_trinh.CongTrinhActivity;
import com.nguyenvinhtien.music.activities.kien_truc.cong_trinh.CongTrinhSuggestListActivity;
import com.nguyenvinhtien.music.activities.newspaper.NewsSuggestListActivity;
import com.nguyenvinhtien.music.activities.newspaper.NewspaperListActivity;
import com.nguyenvinhtien.music.activities.other.MainActivity;
import com.nguyenvinhtien.music.activities.other.OfflineMusicActivity;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;

import com.nguyenvinhtien.music.activities.kien_truc.KienTrucActivity;
import com.nguyenvinhtien.music.activities.bibliography.BiblioPhotoActivity;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.HotPostPrivacyActivity;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.AudioKienTrucActivity;
import com.nguyenvinhtien.music.activities.van_hoc.VanHocAcitivity;
import com.nguyenvinhtien.music.activities.van_hoc.audio_van_hoc.AudioVanHocActivity;
import com.nguyenvinhtien.music.activities.van_hoc.audio_van_hoc.SuggestAudioVanHocListActivity;
import com.nguyenvinhtien.music.activities.van_hoc.tho.ThoActivity;
import com.nguyenvinhtien.music.activities.van_hoc.tho.ThoSuggestListActivity;
import com.nguyenvinhtien.music.activities.van_hoc.truyen.TruyenActivity;
import com.nguyenvinhtien.music.activities.van_hoc.truyen.TruyenSuggestListActivity;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.receivers.RemoteReceiver;
import com.nguyenvinhtien.music.utils.AppController;

import com.nguyenvinhtien.music.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static android.content.ContentValues.TAG;


public class PlayMusicService extends Service {

    public static final String ACTION_STOP_SERVICE = "com.kimvanthang.solemusic.ACTION_STOP_SERVICE";
    private static final int NOTIFICATION_ID = 1609;

    private static MediaPlayer mediaPlayer;
    private LocalBinder localBinder = new LocalBinder();
    private boolean isRepeat = false;
    boolean isShuffle = false;
    private boolean isShowNotification = false;

    ArrayList<Song> lstSongPlaying;
    ArrayList<Integer> histories;
    Random rand;

    int currentSongPos;
    String albumArtPath;
    Song currentSong;
    RemoteViews bigViews;
    RemoteViews views;

    NotificationManager notificationManager;
    Notification n;
    AudioManager audioManager;
    int result;
    MediaSessionCompat mediaSession;


    public class LocalBinder extends Binder {
        public PlayMusicService getInstantBoundService() {
            return PlayMusicService.this;
        }

    }

    //lấy dữ liệu cho Noti
    public void setDataForNotification(ArrayList<Song> lstSong, int currentPos,
                                       Song itemCurrent, String albumArtPath) {
        this.lstSongPlaying = lstSong;
        this.currentSongPos = currentPos;
        this.albumArtPath = albumArtPath;
        this.currentSong = itemCurrent;

        showLockScreen();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Log.d(TAG, "test called to cancel service");
        if (ACTION_STOP_SERVICE.equals(intent.getAction())) {
            PlayMusicActivity musicActivity = (PlayMusicActivity) AppController.getInstance().getPlayMusicActivity();
            MainActivity mainActivity = (MainActivity) AppController.getInstance().getMainActivity();

            HitSongActivity hitSongActivity = (HitSongActivity) AppController.getInstance().getHitSongActivity();
            SuggestSongListActivity suggestSongListActivity = (SuggestSongListActivity) AppController.getInstance().getHitSongSuggestActivity();

            HotVideoActivity hotVideoActivity = (HotVideoActivity) AppController.getInstance().getHotVideoActivity();
            SuggestVideoActivity suggestVideoActivity = (SuggestVideoActivity) AppController.getInstance().getHotVideoSuggestActivity();

            HotAlbumActivity hotAlbumActivity = (HotAlbumActivity) AppController.getInstance().getHotAlbumActivity();
            SuggestAlbumActivity suggestAlbumActivity = (SuggestAlbumActivity) AppController.getInstance().getHotAlbumSuggestActivity();
            AlbumOnlineListActivity albumOnlineListActivity = (AlbumOnlineListActivity) AppController.getInstance().getAlbumDetailActivity();

            NewspaperListActivity newspaperListActivity = (NewspaperListActivity) AppController.getInstance().getHotNewsActivity();
            NewsSuggestListActivity newsSuggestListActivity = (NewsSuggestListActivity) AppController.getInstance().getHotNewsSuggestActivity();

            KienTrucActivity privacyActivity = (KienTrucActivity) AppController.getInstance().getPrivacyActivity();
            BiblioPhotoActivity hotPhotosPrivacyActivity = (BiblioPhotoActivity) AppController.getInstance().getPhotoPrivacyActivity();
            HotPostPrivacyActivity hotPostPrivacyActivity = (HotPostPrivacyActivity) AppController.getInstance().getPostPrivacyActivity();

            BibliographyDetailActivity bibliographyActivity = (BibliographyDetailActivity) AppController.getInstance().getBiblioActivity();

            OfflineMusicActivity offlineMusicActivity = (OfflineMusicActivity) AppController.getInstance().getOfflineSongActivity();

            ThoActivity thoActivity = (ThoActivity) AppController.getInstance().getThoActivity();
            TruyenActivity truyenActivity = (TruyenActivity) AppController.getInstance().getTruyenActivity();
            VanHocAcitivity vanHocAcitivity = (VanHocAcitivity) AppController.getInstance().getVanhocActivity();
            AudioVanHocActivity audioVanHocActivity = (AudioVanHocActivity) AppController.getInstance().getAudiovanhocActivity();

            KienTrucActivity kienTrucActivity = (KienTrucActivity) AppController.getInstance().getKientrucActivity();
            AudioKienTrucActivity audioKienTrucActivity = (AudioKienTrucActivity) AppController.getInstance().getAudiokientrucActivity();

            BibliographyActivity bibliographyActivity1 = (BibliographyActivity) AppController.getInstance().getBibliographyActivity();

            CongTrinhActivity congTrinhActivity = (CongTrinhActivity) AppController.getInstance().getCongtrinhActivity();

            PostSuggestListActivity postSuggestListActivity = (PostSuggestListActivity) AppController.getInstance().getPostSuggestActivity();

            ThoSuggestListActivity thoSuggestListActivity = (ThoSuggestListActivity) AppController.getInstance().getThoSuggestActivity();

            TruyenSuggestListActivity truyenSuggestListActivity = (TruyenSuggestListActivity) AppController.getInstance().getTruyenSuggestActivity();

            CongTrinhSuggestListActivity congTrinhSuggestListActivity = (CongTrinhSuggestListActivity) AppController.getInstance().getCongTrinhSuggestActivity();

            SuggestAudioKienTrucListActivity suggestAudioKienTrucListActivity = (SuggestAudioKienTrucListActivity) AppController.getInstance().getAudioKienTrucSuggestActivity();

            SuggestAudioVanHocListActivity suggestAudioVanHocListActivity = (SuggestAudioVanHocListActivity) AppController.getInstance().getAudioVanHocSuggestActivity();

            Log.d(TAG, "called to cancel service");

            if (suggestAudioKienTrucListActivity != null) {
                suggestAudioKienTrucListActivity.changePlayButtonState();
            }

            if (suggestAudioVanHocListActivity != null) {
                suggestAudioVanHocListActivity.changePlayButtonState();
            }

            if (postSuggestListActivity != null) {
                postSuggestListActivity.changePlayButtonState();
            }

            if (thoSuggestListActivity != null) {
                thoSuggestListActivity.changePlayButtonState();
            }

            if (truyenSuggestListActivity != null) {
                truyenSuggestListActivity.changePlayButtonState();
            }

            if (congTrinhSuggestListActivity != null) {
                congTrinhSuggestListActivity.changePlayButtonState();
            }



            if (musicActivity != null) {
                musicActivity.changePlayButtonState();
            }
            if (mainActivity != null) {
                mainActivity.changePlayButtonState();
            }

            if (hitSongActivity != null) {
                hitSongActivity.changePlayButtonState();
            }
            if (suggestSongListActivity != null) {
                suggestSongListActivity.changePlayButtonState();
            }

            if (hotVideoActivity != null) {
                hotVideoActivity.changePlayButtonState();
            }
            if (suggestVideoActivity != null) {
                suggestVideoActivity.changePlayButtonState();
            }

            if (hotAlbumActivity != null) {
                hotAlbumActivity.changePlayButtonState();
            }
            if (suggestAlbumActivity != null) {
                suggestAlbumActivity.changePlayButtonState();
            }
            if (albumOnlineListActivity != null) {
                albumOnlineListActivity.changePlayButtonState();
            }

            if (newspaperListActivity != null) {
                newspaperListActivity.changePlayButtonState();
            }
            if (newsSuggestListActivity != null) {
                newsSuggestListActivity.changePlayButtonState();
            }

            if (privacyActivity != null) {
                privacyActivity.changePlayButtonState();
            }

            if (hotPhotosPrivacyActivity != null) {
                hotPhotosPrivacyActivity.changePlayButtonState();
            }
            if (hotPostPrivacyActivity != null) {
                hotPostPrivacyActivity.changePlayButtonState();
            }

            if (bibliographyActivity != null) {
                bibliographyActivity.changePlayButtonState();
            }

            if (offlineMusicActivity != null) {
                offlineMusicActivity.changePlayButtonState();
            }


            if (thoActivity != null) {
                thoActivity.changePlayButtonState();
            }


            if (truyenActivity != null) {
                truyenActivity.changePlayButtonState();
            }


            if (vanHocAcitivity != null) {
                vanHocAcitivity.changePlayButtonState();
            }

            if (audioVanHocActivity != null) {
                audioVanHocActivity.changePlayButtonState();
            }

            if (kienTrucActivity != null) {
                kienTrucActivity.changePlayButtonState();
            }

            if (audioKienTrucActivity != null) {
                audioKienTrucActivity.changePlayButtonState();
            }

            if (bibliographyActivity1 != null) {
                bibliographyActivity1.changePlayButtonState();
            }

            if (congTrinhActivity != null) {
                congTrinhActivity.changePlayButtonState();
            }




            if (musicActivity == null && mainActivity == null) {
                stopSelf();
            }

            pauseMusic();
            stopForeground(true);
            isShowNotification = false;

        } else {
            showNotification(isShowNotification());
            isShowNotification = true;
        }

        setStatePlayPause();

        return START_NOT_STICKY;
    }

    public void setStatePlayPause() {

        if (mediaSession != null) {
            if (mediaSession.getController().getPlaybackState().getState() == PlaybackStateCompat.STATE_PLAYING) {
                mediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                        .setState(PlaybackStateCompat.STATE_PAUSED, 0, 1.0f)
                        .setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                        .build());
            } else {
                mediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                        .setState(PlaybackStateCompat.STATE_PLAYING, 0, 1.0f)
                        .setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                        .build());
            }
        }

    }


    @Override
    public void onCreate() {
        super.onCreate();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        result = audioManager.requestAudioFocus(afChangeListener,
                // Use the music stream
                AudioManager.STREAM_MUSIC,
                // Request permanent focus
                AudioManager.AUDIOFOCUS_GAIN);
        histories = new ArrayList<>();
        rand = new Random();
    }

    //show ở màn hình khóa
    public void showLockScreen() {
        ComponentName receiver = new ComponentName(getPackageName(), RemoteReceiver.class.getName());
        mediaSession = new MediaSessionCompat(this, "PlayService", receiver, null);
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PAUSED, 0, 1.0f)
                .setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                .build());

        mediaSession.setMetadata(new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, currentSong.getArtist())
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, currentSong.getTitle())
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, mediaPlayer.getDuration())
                .build());
        mediaSession.setActive(true);
    }

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            PlayMusicActivity activity = (PlayMusicActivity) AppController.getInstance().getPlayMusicActivity();
            Log.d("check", "focusChange->" + focusChange);
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:

                    if (activity == null) {
                        pauseMusic();
                        changePlayPauseState();

                    } else {
                        activity.pauseMusic();
                        changePlayPauseState();
                        activity.updatePlayPauseButton();
                        Log.d("check", "AUDIOFOCUS_LOSS_TRANSIENT");
                    }
                    break;
//                case AudioManager.AUDIOFOCUS_GAIN:
//                    if (activity == null) {
//                        resumeMusic();
//                    } else {
//                        activity.resumeMusic();
//                        Log.d("check", "AUDIOFOCUS_GAIN");
//                    }
//                    break;
                case AudioManager.AUDIOFOCUS_LOSS:
                    if (activity == null) {
                        pauseMusic();
                        changePlayPauseState();
                    } else {
                        activity.pauseMusic();
                        changePlayPauseState();
                        Log.d("check", "AUDIOFOCUS_LOSS");
                    }

                    break;

            }
        }
    };

    public boolean isShowNotification() {
        return isShowNotification;
    }

    //Notification
    public Notification showNotification(boolean isUpdate) {

        bigViews = new RemoteViews(getPackageName(), R.layout.notification_view_expanded);
        views = new RemoteViews(getPackageName(), R.layout.notification_view);

        Intent intent = new Intent(getApplicationContext(), PlayMusicActivity.class);
        intent.putExtra(PlayMusicActivity.IS_PLAYING, true);


        if (isPlaying()) {
            views.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_pause);
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_pause);
        } else {
            views.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_play);
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_play);
        }


        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentPrev = new Intent(Constants.ACTION_PREV);
        intentPrev.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntentPrev = PendingIntent.getBroadcast(getApplicationContext(), 0, intentPrev, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentPlayPause = new Intent(Constants.ACTION_PLAY_PAUSE);
        intentPlayPause.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntentPlayPause = PendingIntent.getBroadcast(getApplicationContext(), 0, intentPlayPause, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentNext = new Intent(Constants.ACTION_NEXT);
        intentNext.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntentNext = PendingIntent.getBroadcast(getApplicationContext(), 0, intentNext, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent intentStopSelf = new Intent(this, PlayMusicService.class);
        intentStopSelf.setAction(PlayMusicService.ACTION_STOP_SERVICE);
        PendingIntent pendingIntentStopSelf = PendingIntent.getService(this, 0, intentStopSelf, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setSmallIcon(R.drawable.ic_music);
        builder.setContentIntent(pendingIntent);
        builder.setContent(views);
        builder.setCustomBigContentView(bigViews);

        try {
            bigViews.setTextViewText(R.id.tv_song_title_noti, currentSong.getTitle());
            bigViews.setTextViewText(R.id.tv_artist_noti, currentSong.getArtist());

            views.setTextViewText(R.id.tv_song_title_noti, currentSong.getTitle());
            views.setTextViewText(R.id.tv_artist_noti, currentSong.getArtist());
        } catch (Exception e) {

        }

        n = builder.build();

        NotificationTarget notiBigViewsTarget = new NotificationTarget(
                this,
                bigViews,
                R.id.img_album_art_noti,
                n,
                NOTIFICATION_ID);

        NotificationTarget notiViewsTarget = new NotificationTarget(
                this,
                views,
                R.id.img_album_art_noti,
                n,
                NOTIFICATION_ID);


        if (albumArtPath != null) {
            if (albumArtPath.contains("firebasestorage")) {
                if (albumArtPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                    bigViews.setImageViewResource(R.id.img_album_art_noti, R.drawable.default_cover_big);
                    views.setImageViewResource(R.id.img_album_art_noti, R.drawable.default_cover_big);
                } else {
                    Glide.with(getApplicationContext())
                            .load(albumArtPath)
                            .asBitmap()
                            .into(notiBigViewsTarget);
                    Glide.with(getApplicationContext())
                            .load(albumArtPath)
                            .asBitmap()
                            .into(notiViewsTarget);
                }
            } else {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(currentSong.getPath());

                byte[] image = mmr.getEmbeddedPicture();
                if (image != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                    bigViews.setImageViewBitmap(R.id.img_album_art_noti, bitmap);
                    views.setImageViewBitmap(R.id.img_album_art_noti, bitmap);
                } else {
                    bigViews.setImageViewResource(R.id.img_album_art_noti, R.drawable.default_cover_big);
                    views.setImageViewResource(R.id.img_album_art_noti, R.drawable.default_cover_big);
                }
            }

        } else {
            bigViews.setImageViewResource(R.id.img_album_art_noti, R.drawable.default_cover_big);
            views.setImageViewResource(R.id.img_album_art_noti, R.drawable.default_cover_big);
        }


        bigViews.setOnClickPendingIntent(R.id.btn_close_noti, pendingIntentStopSelf);
        bigViews.setOnClickPendingIntent(R.id.btn_prev_noti, pendingIntentPrev);
        bigViews.setOnClickPendingIntent(R.id.btn_next_noti, pendingIntentNext);
        bigViews.setOnClickPendingIntent(R.id.btn_play_pause_noti, pendingIntentPlayPause);

        views.setOnClickPendingIntent(R.id.btn_close_noti, pendingIntentStopSelf);
        views.setOnClickPendingIntent(R.id.btn_next_noti, pendingIntentNext);
        views.setOnClickPendingIntent(R.id.btn_play_pause_noti, pendingIntentPlayPause);

        if (isUpdate) {
            startForeground(NOTIFICATION_ID, n);
        }
        return n;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }


    private void releaseMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            stopMusic();
            mediaPlayer.release();
        }
    }

    public void pauseMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();

        }
    }

    public void resumeMusic() {
        if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        }
    }


    public void nextMusic() {
        currentSongPos = getNextPosition();
        currentSong = lstSongPlaying.get(currentSongPos);
        String path = currentSong.getPath();
        albumArtPath = currentSong.getAlbumImagePath();
        playMusic(path);
    }

    public void backMusic() {
        currentSongPos = getPrePosition();
        currentSong = lstSongPlaying.get(currentSongPos);
        String path = currentSong.getPath();
        albumArtPath = currentSong.getAlbumImagePath();
        playMusic(path);
    }

    public void playPauseMusic() {
        if (mediaPlayer.isPlaying()) {
            pauseMusic();
        } else {
            resumeMusic();
        }

    }

    //Doi hinh button Play/Pause tren Noti
    public void changePlayPauseState() {
        if (isPlaying()) {
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_pause);
            views.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_pause);
        } else {
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_play);
            views.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_play);
        }
        startForeground(NOTIFICATION_ID, n);
    }

//    //thay đổi bìa album ở Fragment Play
//    private void setAlbumArt() {
//        Intent intent1 = new Intent(Constants.ACTION_CHANGE_ALBUM_ART);
//        intent1.putExtra(FragmentPlay.KEY_ALBUM_PLAY, lstSongPlaying.get(currentSongPos).getAlbumImagePath());
//        sendBroadcast(intent1);
//    }


    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }


    public void playMusic(final String path) {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (AppController.getInstance().getPlayMusicActivity() != null) {
                    Intent intent = new Intent(Constants.ACTION_COMPLETE_SONG);
                    sendBroadcast(intent);
                    showNotification(true);
                } else {
                    if (isRepeat()) {
                        playMusic(currentSong.getPath());
                    } else {
                        nextMusic();

                    }
                    showNotification(true);
                }
                //update cac man hinh
                Intent intent = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                sendBroadcast(intent);
            }
        });

        //Trường hợp bài hát bị xóa khỏi thiết bị nhưng vẫn hiển thị trong danh sách phát nhạc của người dùng
        if (!path.contains("firebasestorage") && checkFileExist(path) == false) {
            Toast.makeText(getApplicationContext(), "Bài hát không tồn tại!", Toast.LENGTH_SHORT).show();
        } else {

            try {
                mediaPlayer.setDataSource(path);
                mediaPlayer.prepare();


            } catch (IOException e) {
                e.printStackTrace();
            }


            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                mediaPlayer.start();
                histories.add(currentSongPos);// them bai hat hien tai vao histories
            }


        }

    }


    public boolean checkFileExist(String path) {
        File f = new File(path);
        if (f.exists()) return true;
        return false;
    }

    public void stopMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }

    public int getTotalTime() {
        return mediaPlayer.getDuration() / 1000;
    }

    public int getCurrentLength() {
        return mediaPlayer.getCurrentPosition() / 1000;
    }

    public void seekTo(int seconds) {
        mediaPlayer.seekTo(seconds * 1000);
    }

    //Lay vi tri bai hat tiep theo
    public int getNextPosition() {
        if (isShuffle) {
            int newSongPosition = currentSongPos;
            //newSongPosition phai khac bai hat hien tai khi bat shuffle
            while (newSongPosition == currentSongPos)
                newSongPosition = rand.nextInt(lstSongPlaying.size());
            return newSongPosition;
        }
        int newSongPosition;
        //Dang o bai hat cuoi list thi chay den bai hat dau list
        if (currentSongPos == lstSongPlaying.size() - 1) {
            currentSongPos = 0;
        } else {
            currentSongPos++;
        }
//        if (histories.size() > lstSongPlaying.size() - 1)
//            histories.remove(0);
//        if (currentSongPos < 0) return 0;
//        if (isRepeat()) {
//            return currentSongPos;
//        }
        newSongPosition = currentSongPos;
        return newSongPosition;
    }

    public int getPrePosition() {
        if (isShuffle()) {
            int newSongPosition = currentSongPos;
            while (newSongPosition == currentSongPos)
                newSongPosition = rand.nextInt(lstSongPlaying.size());
            return newSongPosition;
        }
//        int newSongPosition = histories.get(histories.size() - 1);
//        histories.remove(histories.size() - 1);
        int newSongPosition;
        if (currentSongPos == 0) {
            currentSongPos = lstSongPlaying.size() - 1;
        } else {
            currentSongPos--;
        }
        newSongPosition = currentSongPos;
        return newSongPosition;
    }


    public void setRepeat(boolean repeat) {
        this.isRepeat = repeat;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public ArrayList<Song> getLstSongPlaying() {
        return lstSongPlaying;
    }

    public void setShuffle(boolean shuffle) {
        this.isShuffle = shuffle;
    }

    public boolean isShuffle() {
        return isShuffle;
    }

    public int getCurrentSongPos() {
        return currentSongPos;
    }

    public String getAlbumArtPath() {
        return albumArtPath;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        audioManager.abandonAudioFocus(afChangeListener);
    }
}
