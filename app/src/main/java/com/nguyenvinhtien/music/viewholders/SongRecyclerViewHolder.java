package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;

/**
 * Created by KimVanThang on 4/17/2017.
 */

public class SongRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView tvTitle;
    public TextView tvArtist;
    public TextView tvOption;
    public ImageView imgImage;

    public SongRecyclerViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_song_name_play);
        tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_song_play);
        imgImage = (ImageView) itemView.findViewById(R.id.img_album_song_play);
        tvOption = (TextView) itemView.findViewById(R.id.tv_option);
    }
}
