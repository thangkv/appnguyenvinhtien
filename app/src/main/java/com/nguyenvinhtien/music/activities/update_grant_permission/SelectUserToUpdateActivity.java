package com.nguyenvinhtien.music.activities.update_grant_permission;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.models.User;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class SelectUserToUpdateActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    DatabaseReference adminRef;
    DatabaseReference specialUserRef;

    RecyclerView listAdmin ;
    RecyclerView listSpecialUser;

    FirebaseRecyclerAdapter<User, SelectUserToUpdateActivity.SelectUserRecyclerViewHolder> adapterAdmin;
    FirebaseRecyclerAdapter<User, SelectUserToUpdateActivity.SelectUserRecyclerViewHolder> adapterSpecialUser;

    String userKey = null;
    String userEmail = null;
    String userPermission = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_user_to_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_user_to_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);


        mDatabase = FirebaseDatabase.getInstance().getReference().child("Permission-Users");
        adminRef = mDatabase.child("Admins");
        adapterAdmin = new FirebaseRecyclerAdapter<User, SelectUserToUpdateActivity.SelectUserRecyclerViewHolder>(User.class, R.layout.item_select_user, SelectUserToUpdateActivity.SelectUserRecyclerViewHolder.class, adminRef) {
            @Override
            protected void populateViewHolder(final SelectUserToUpdateActivity.SelectUserRecyclerViewHolder viewHolder, final User model, final int position) {
                viewHolder.tvEmail.setText(model.getEmail());
                viewHolder.tvPermission.setText("Admin");

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        userKey = getRef(position).getKey();
                        userEmail = model.getEmail();
                        userPermission = "Admins";
                        viewHolder.itemView.setBackgroundColor(Color.rgb(66,110,180));
                        Intent i = new Intent(SelectUserToUpdateActivity.this, UpdateUserPermissionActivity.class);
                        i.putExtra("KEY_USER", userKey);
                        i.putExtra("EMAIL_USER", userEmail);
                        i.putExtra("PERMISSION_USER", userPermission);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });
            }
        };



        specialUserRef = mDatabase.child("Special-Users");
        adapterSpecialUser = new FirebaseRecyclerAdapter<User, SelectUserToUpdateActivity.SelectUserRecyclerViewHolder>(User.class, R.layout.item_select_user, SelectUserToUpdateActivity.SelectUserRecyclerViewHolder.class, specialUserRef) {
            @Override
            protected void populateViewHolder(final SelectUserToUpdateActivity.SelectUserRecyclerViewHolder viewHolder, final User model, final int position) {
                viewHolder.tvEmail.setText(model.getEmail());
                viewHolder.tvPermission.setText("Special User");

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        userKey = getRef(position).getKey();
                        userEmail = model.getEmail();
                        userPermission = "Special-Users";
                        viewHolder.itemView.setBackgroundColor(Color.rgb(66,110,180));
                        Intent i = new Intent(SelectUserToUpdateActivity.this, UpdateUserPermissionActivity.class);
                        i.putExtra("KEY_USER", userKey);
                        i.putExtra("EMAIL_USER", userEmail);
                        i.putExtra("PERMISSION_USER", userPermission);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });


            }


        };



        initControls();
        initEvents();

        adapterAdmin.notifyDataSetChanged();
        listAdmin.setAdapter(adapterAdmin);

        adapterSpecialUser.notifyDataSetChanged();
        listSpecialUser.setAdapter(adapterSpecialUser);

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }

    }

    public static class SelectUserRecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvEmail;
        public TextView tvPermission;

        public SelectUserRecyclerViewHolder(View itemView) {
            super(itemView);
            tvEmail = (TextView) itemView.findViewById(R.id.tv_user_email);
            tvPermission = (TextView) itemView.findViewById(R.id.tv_user_permission);
        }

    }

    private void initControls() {
        listAdmin = (RecyclerView) findViewById(R.id.rv_admin_list_select);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listAdmin.setLayoutManager(layoutManager);

        listSpecialUser = (RecyclerView) findViewById(R.id.rv_special_user_list_select);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(this);
        listSpecialUser.setLayoutManager(layoutManager1);

    }

    private void initEvents() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapterAdmin.cleanup();
        adapterSpecialUser.cleanup();
    }
}
