package com.nguyenvinhtien.music.activities.update_album;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.models.Album;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class SelectAlbumToUpdateActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    RecyclerView listAlbum;
    FirebaseRecyclerAdapter<Album, SelectAlbumToUpdateActivity.SelectAlbumRecyclerViewHolder> adapter;
    String albumKey = null;
    String albumTitle = null;
    String albumArtist = null;
    String albumInfo = null;
    String albumArtPath = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_album_to_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_album_to_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Albums");
        adapter = new FirebaseRecyclerAdapter<Album, SelectAlbumRecyclerViewHolder>(Album.class, R.layout.item_select_album, SelectAlbumToUpdateActivity.SelectAlbumRecyclerViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(final SelectAlbumToUpdateActivity.SelectAlbumRecyclerViewHolder viewHolder, final Album model, final int position) {
                viewHolder.tvTitle.setText(model.getTitle());
                viewHolder.tvArtist.setText(model.getArtist());

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        albumKey = getRef(position).getKey();
                        albumTitle = model.getTitle();
                        albumArtist = model.getArtist();
                        albumArtPath = model.getAlbumArtPath();
                        albumInfo = model.getInfo();
                        viewHolder.itemView.setBackgroundColor(Color.rgb(66,110,180));
                        Intent i = new Intent(SelectAlbumToUpdateActivity.this, UpdateDataAlbumActivity.class);
                        i.putExtra("KEY_ALBUM", albumKey);
                        i.putExtra("TITLE_ALBUM", albumTitle);
                        i.putExtra("ARTIST_ALBUM", albumArtist);
                        i.putExtra("ART_PATH_ALBUM", albumArtPath);
                        i.putExtra("INFO_ALBUM", albumInfo);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });


            }


        };

        initControls();
        initEvents();
        adapter.notifyDataSetChanged();
        listAlbum.setAdapter(adapter);


        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }


    public static class SelectAlbumRecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvArtist;

        public SelectAlbumRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_album);
            tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_album);
        }

    }

    private void initControls() {
        listAlbum = (RecyclerView) findViewById(R.id.rv_album_list_select);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listAlbum.setLayoutManager(layoutManager);

    }

    private void initEvents() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
