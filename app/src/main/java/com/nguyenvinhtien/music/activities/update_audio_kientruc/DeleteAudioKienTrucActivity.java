package com.nguyenvinhtien.music.activities.update_audio_kientruc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.update_audio_vanhoc.DeleteAudioVanHocActivity;
import com.nguyenvinhtien.music.activities.update_audio_vanhoc.SelectAudioVanHocToUpdateActivity;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class DeleteAudioKienTrucActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_SONG_REQUEST = 4;


    TextView tvSongSelect;
    Button btnSelectSong, btnUpdateSong;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_song;
    String title_song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_delete_audio_kien_truc);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_delete_song);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvSongSelect = (TextView) findViewById(R.id.tv_song_select);
        btnSelectSong = (Button) findViewById(R.id.btn_select_song);
        btnUpdateSong = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnSelectSong.setOnClickListener(this);
        btnUpdateSong.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_song:
                Intent selectSongIntent = new Intent(DeleteAudioKienTrucActivity.this, SelectAudioKienTrucToUpdateActivity.class);
                startActivityForResult(selectSongIntent, SELECT_SONG_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_SONG_REQUEST && resultCode == RESULT_OK) {
            key_song = data.getStringExtra("KEY_SONG");
            title_song = data.getStringExtra("TITLE_SONG");

            updateUI();


        }

    }

    private void updateUI() {
        if (title_song != null) {
            tvSongSelect.setVisibility(View.VISIBLE);
            tvSongSelect.setText(title_song);


        }

    }


    private void startUpdate() {
        mProgress.setMessage("Xóa audio kiến trúc...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        if (!tvSongSelect.getText().equals("")) {


            mProgress.show();


            DatabaseReference song = mDatabase.child("AudioKienTrucs").child(key_song);

            song.removeValue();


            DatabaseReference song_comment = mDatabase.child("AudioKienTruc-Comments").child(key_song);
            song_comment.removeValue();


            //Không xóa trên storage để người dùng nào đang play 1 list chứa bài vừa xóa sẽ không bị dừng đột ngột
//            StorageReference imagePath = mStorage.child("SongArts").child(key_song);
//            imagePath.delete();

//            StorageReference songPath = mStorage.child("Songs").child(key_song);
//            songPath.delete();

            Toast.makeText(getApplicationContext(), "Xóa audio kiến trúc thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();
        } else {
            Toast.makeText(this, "Cần chọn audio muốn xóa", Toast.LENGTH_SHORT).show();
        }
    }
}
