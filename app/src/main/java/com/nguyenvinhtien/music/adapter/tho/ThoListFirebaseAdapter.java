package com.nguyenvinhtien.music.adapter.tho;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.PostDetailActivity;
import com.nguyenvinhtien.music.activities.van_hoc.tho.ThoDetailActiivty;
import com.nguyenvinhtien.music.models.Post;
import com.nguyenvinhtien.music.models.Tho;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.PostRecyclerViewHolder;

import java.util.ArrayList;

/**
 * Created by Kim Van Thang on 5/27/2017.
 */

public class ThoListFirebaseAdapter extends FirebaseRecyclerAdapter<Tho, PostRecyclerViewHolder> {


    private Activity mContext;


    ArrayList<Tho> mLstPost = new ArrayList<>();


    private DatabaseReference mDatabase;


    public ThoListFirebaseAdapter(Activity mContext, Class<Tho> modelClass, int modelLayout, Class<PostRecyclerViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;

        getListTho();

    }


    @Override
    protected void populateViewHolder(final PostRecyclerViewHolder viewHolder, final Tho model, final int position) {
        viewHolder.tvTitle.setText(model.getTitle());
        viewHolder.tvContent.setText(model.getContent());
        try {
            Glide.with(mContext).load(model.getImagePath()).placeholder(R.drawable.picture).into(viewHolder.imgImage);
        } catch (Exception e) {
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ThoDetailActiivty.class);
                intent.putExtra(Constants.POST_ID, getRef(position).getKey());
                intent.putExtra(Constants.POST_TITLE, model.getTitle());
                mContext.startActivity(intent);
                mContext.overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
            }
        });


    }

    private ArrayList<Tho> getListTho() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Thos").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                String title = (String) dataSnapshot.child("title").getValue();
                String content = (String) dataSnapshot.child("content").getValue();
                String imagePath = (String) dataSnapshot.child("imagePath").getValue();

                Tho tho = new Tho(id, title, content, imagePath);
                mLstPost.add(tho);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return mLstPost;
    }
}
