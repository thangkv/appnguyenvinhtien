package com.nguyenvinhtien.music.activities.update_bibliography;

import android.app.ProgressDialog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateBiblioContentActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnUpdateContent;
    EditText edtTieuSu, edtAmNhac, edtKienTruc, edtVanHoc;

    DatabaseReference mDatabase;
    DatabaseReference tieusu;
    DatabaseReference sunghiep;
    ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_biblio_content);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_content_biblio);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("BibliographyDetail");
        tieusu = mDatabase.child("tieusu");
        sunghiep = mDatabase.child("sunghiep");

        tieusu.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                edtTieuSu.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        sunghiep.child("amnhac").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                edtAmNhac.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        sunghiep.child("kientruc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                edtKienTruc.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        sunghiep.child("vanhoc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                edtVanHoc.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        edtAmNhac = (EditText) findViewById(R.id.edt_amnhac);
        edtKienTruc = (EditText) findViewById(R.id.edt_kientruc);
        edtVanHoc = (EditText) findViewById(R.id.edt_vanhoc);
        edtTieuSu = (EditText) findViewById(R.id.edt_tieusu);
        btnUpdateContent = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnUpdateContent.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật tiểu sử...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String tieu_su_val = edtTieuSu.getText().toString().trim();
        final String am_nhac_val = edtAmNhac.getText().toString().trim();
        final String van_hoc_val = edtVanHoc.getText().toString().trim();
        final String kien_truc_val = edtKienTruc.getText().toString().trim();

        if (!TextUtils.isEmpty(tieu_su_val)
                && !TextUtils.isEmpty(am_nhac_val)
                && !TextUtils.isEmpty(van_hoc_val)
                && !TextUtils.isEmpty(kien_truc_val)) {
            mProgress.show();

            tieusu.setValue(tieu_su_val);
            sunghiep.child("amnhac").setValue(am_nhac_val);
            sunghiep.child("vanhoc").setValue(van_hoc_val);
            sunghiep.child("kientruc").setValue(kien_truc_val);

                Toast.makeText(getApplicationContext(), "Cập nhật tiểu sử thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();

        } else {
            Toast.makeText(getApplicationContext(), "Cần điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
