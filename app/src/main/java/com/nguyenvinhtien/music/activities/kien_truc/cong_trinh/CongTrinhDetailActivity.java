package com.nguyenvinhtien.music.activities.kien_truc.cong_trinh;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.CommentPostActivity;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.PostDetailActivity;
import com.nguyenvinhtien.music.adapter.cong_trinh.CongTrinhPhotoListFirebaseAdapter;
import com.nguyenvinhtien.music.models.Photo;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.PhotoRecyclerViewHolder;

public class CongTrinhDetailActivity extends AppCompatActivity {

    TextView tvTitle, tvContent;
    RecyclerView mRvCongTrinhPhotoList;

    CongTrinhPhotoListFirebaseAdapter adapter;
    LoadListPhoto loadListPhoto;

    Button btnComment;
    String newsID;
    String postTitle;

    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cong_trinh_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail_news);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        newsID = intent.getStringExtra(Constants.POST_ID);
        postTitle = intent.getStringExtra(Constants.POST_TITLE);


        tvTitle = (TextView)findViewById(R.id.tv_title_post);

        mRvCongTrinhPhotoList = (RecyclerView) findViewById(R.id.rv_congtrinh_photo);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvCongTrinhPhotoList.setLayoutManager(layoutManager);

        tvContent = (TextView) findViewById(R.id.tv_content_post);
        btnComment = (Button) findViewById(R.id.btn_comment);

        showListPhoto();

        mDatabase.child("Congtrinhs").child(newsID).child("title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String tieude = (String) dataSnapshot.getValue();
                tvTitle.setText(tieude);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("Congtrinhs").child(newsID).child("content").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String noidung = (String) dataSnapshot.getValue();
                tvContent.setText(noidung);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnComment = (Button)findViewById(R.id.btn_comment);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.getInstance().checkInternetState() == true){
                    Intent intent = new Intent(CongTrinhDetailActivity.this, CommentCongTrinhActivity.class);
                    intent.putExtra(Constants.POST_ID, newsID);
                    intent.putExtra(Constants.POST_TITLE, postTitle);
                    startActivity(intent);
                } else {
                    Toast.makeText(CongTrinhDetailActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void showListPhoto() {
        loadListPhoto = new LoadListPhoto();
        loadListPhoto.execute();
    }

    private class LoadListPhoto extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Query queryComment = mDatabase.child("Congtrinhs-Anhs").child(newsID);

            adapter = new CongTrinhPhotoListFirebaseAdapter(CongTrinhDetailActivity.this, Photo.class,
                    R.layout.item_photo, PhotoRecyclerViewHolder.class, queryComment, newsID);
            adapter.notifyDataSetChanged();
            return adapter;

        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            mRvCongTrinhPhotoList.setAdapter(adapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
