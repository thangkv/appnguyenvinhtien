package com.nguyenvinhtien.music.activities.van_hoc.audio_van_hoc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.SuggestAudioKienTrucListActivity;
import com.nguyenvinhtien.music.activities.other.MainActivity;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.adapter.song.SongOnlineListSuggestAdapter;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

public class SuggestAudioVanHocListActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvSongName;
    RecyclerView mRvListSong;
    SongOnlineListSuggestAdapter mSongAdapter;
    ArrayList<Song> mLstSong = new ArrayList<>();


    String query;
    SuggestAudioVanHocListActivity.LoadListSong loadListSong;


    public LinearLayout currentPlayingBar;
    PlayMusicService musicService;
    ImageView btnPlayPauseCurrent;
    ImageView btnNextCurrent;
    ImageView btnPrevCurrent;
    ImageView imgAlbumArtCurrent;
    TextView tvTitle;
    TextView tvArtist;


    String currentPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_suggest_audio_van_hoc_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_list_online);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Common.setStatusBarTranslucent(true, this);

        AppController.getInstance().setAudioVanHocSuggestActivity(this);

        initControls();
        initEvents();
        showListSong();

        if (musicService != null) {
            updatePlayingState();
            showCurrentSong();
        }

        registerBroadcastUpdatePlayingSuggestSong();

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

    BroadcastReceiver broadcastReceiverUpdatePlayingSuggestSong = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
            if (musicService != null) {
                currentPlayingBar.setVisibility(View.VISIBLE);

            } else {
                currentPlayingBar.setVisibility(View.GONE);
            }
            showCurrentSong();
            updatePlayingState();
        }
    };

    private void registerBroadcastUpdatePlayingSuggestSong() {
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_UPDATE_PLAY_STATUS);
        registerReceiver(broadcastReceiverUpdatePlayingSuggestSong, intentFilter);
    }

    private void unRegisterBroadcastUpdatePlayingSuggestSong() {
        unregisterReceiver(broadcastReceiverUpdatePlayingSuggestSong);
    }

    public void showCurrentSong() {
        if (musicService != null) {
            tvTitle.setText(musicService.getCurrentSong().getTitle());
            tvArtist.setText(musicService.getCurrentSong().getArtist());
            String albumPath = musicService.getCurrentSong().getAlbumImagePath();

            currentPath = musicService.getCurrentSong().getPath();

            if (albumPath != null) {
                if (albumPath.contains("firebasestorage")) {
                    if (albumPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    } else {
                        Glide.with(this).load(albumPath).into(imgAlbumArtCurrent);
                    }
                } else {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(currentPath);

                    byte[] image = mmr.getEmbeddedPicture();
                    if (image != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                        imgAlbumArtCurrent.setImageBitmap(bitmap);
                    } else {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    }
                }
            } else {
                imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
            }
        }
    }

    public void updatePlayingState() {
        if (musicService != null) {
            if (musicService.isPlaying()) {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
            } else {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
            }
        }
    }


    private void initEvents() {

        currentPlayingBar.setOnClickListener(this);
        btnPlayPauseCurrent.setOnClickListener(this);
        btnNextCurrent.setOnClickListener(this);
        btnPrevCurrent.setOnClickListener(this);
    }

    private void initControls() {
        tvSongName = (TextView) findViewById(R.id.tv_song_online_title);
        mRvListSong = (RecyclerView) findViewById(R.id.rv_song_list_online);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRvListSong.setLayoutManager(layoutManager);


        currentPlayingBar = (LinearLayout) findViewById(R.id.current_playing_bar);
        btnPlayPauseCurrent = (ImageView) findViewById(R.id.btn_play_pause_current);
        btnNextCurrent = (ImageView) findViewById(R.id.btn_next_current);
        btnPrevCurrent = (ImageView) findViewById(R.id.btn_prev_current);
        imgAlbumArtCurrent = (ImageView) findViewById(R.id.img_album_current_bar);
        tvTitle = (TextView) findViewById(R.id.tv_song_title_current);
        tvTitle.setSelected(true);
        tvArtist = (TextView) findViewById(R.id.tv_artist_current);

        musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        if (musicService != null) {
            currentPlayingBar.setVisibility(View.VISIBLE);
        } else {
            currentPlayingBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.current_playing_bar:
                if (musicService != null) {
                    Intent intent = new Intent(SuggestAudioVanHocListActivity.this, PlayMusicActivity.class);
                    intent.putExtra(PlayMusicActivity.IS_PLAYING, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
                }
                break;
            case R.id.btn_play_pause_current:
                if (musicService != null) {
                    Intent intent = new Intent(Constants.ACTION_PLAY_PAUSE);
                    if (musicService.isPlaying()) {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
                    } else {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
                    }
                    sendBroadcast(intent);

                }
                break;
            case R.id.btn_next_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_NEXT);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_NEXT);
                        sendBroadcast(intent);
                    }

                }
                break;
            case R.id.btn_prev_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_PREV);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_PREV);
                        sendBroadcast(intent);
                    }

                }
                break;
        }
    }


    //thay đổi button Play,Pause khi stop service trên thanh thông báo
    public void changePlayButtonState() {
        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showListSong() {
        loadListSong = new SuggestAudioVanHocListActivity.LoadListSong();
        loadListSong.execute();
    }


    private class LoadListSong extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Intent intent = getIntent();
            query = intent.getStringExtra(MainActivity.AUDIOVANHOC_NAME);

            for (int i = 0; i <Song.getListAudioVanHoc().size(); i++) {
                if (Song.getListAudioVanHoc().get(i).getTitle().toLowerCase().contains(query.toLowerCase())) {
                    mLstSong.add(Song.getListAudioVanHoc().get(i));
                }
            }
            return mLstSong;
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            mSongAdapter = new SongOnlineListSuggestAdapter(SuggestAudioVanHocListActivity.this, mLstSong);
            mSongAdapter.notifyDataSetChanged();
            mRvListSong.setAdapter(mSongAdapter);
            tvSongName.setText(query);

        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterBroadcastUpdatePlayingSuggestSong();
    }
}
