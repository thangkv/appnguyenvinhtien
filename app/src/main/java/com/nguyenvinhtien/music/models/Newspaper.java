package com.nguyenvinhtien.music.models;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/22/2017.
 */

public class Newspaper {
    String id;
    String title;
    String source;
    String link;
    String imagePath;

    public static DatabaseReference mData = FirebaseDatabase.getInstance().getReference();

    private static ArrayList<Newspaper> listNewspaper;

    public Newspaper() {
    }


    public Newspaper(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public Newspaper(String title, String source, String link) {
        this.title = title;
        this.source = source;
        this.link = link;
    }

    public Newspaper(String id, String title, String source, String link, String imagePath) {
        this.id = id;
        this.title = title;
        this.source = source;
        this.link = link;
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public static ArrayList<Newspaper> getListNewspaper() {
        if (listNewspaper == null) {
            listNewspaper = new ArrayList<>();
            mData.child("News").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String source = (String) dataSnapshot.child("source").getValue();
                    String link = (String) dataSnapshot.child("link").getValue();
                    String imagePath = (String) dataSnapshot.child("imagePath").getValue();

                    Newspaper newspaper = new Newspaper(id, title, source, link, imagePath);
                    listNewspaper.add(newspaper);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        return listNewspaper;
    }
}
