package com.nguyenvinhtien.music.models;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/25/2017.
 */

public class Post {
    String id;
    String title;
    String content;
    String imagePath;

    public static DatabaseReference mData = FirebaseDatabase.getInstance().getReference();

    private static ArrayList<Post> listPost;

    public Post() {
    }

    public Post(String id, String title, String content, String imagePath) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.imagePath = imagePath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public static ArrayList<Post> getListPost() {
        if (listPost == null) {
            listPost = new ArrayList<>();
            mData.child("Posts").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String content = (String) dataSnapshot.child("content").getValue();
                    String imagePath = (String) dataSnapshot.child("imagePath").getValue();

                    Post post = new Post(id, title, content, imagePath);
                    listPost.add(post);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        return listPost;
    }
}
