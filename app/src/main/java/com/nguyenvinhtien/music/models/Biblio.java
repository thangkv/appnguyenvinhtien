package com.nguyenvinhtien.music.models;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KimVanThang on 5/6/2017.
 */

public class Biblio {
    public static DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    public static String showTieuSu() {
        final String[] tieusu = new String[1];
        mDatabase.child("BibliographyDetail").child("tieusu").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tieusu[0] = (String) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return tieusu[0];
    }

    public static ArrayList<String> showSuNghiep() {
        final List<String> listString = new ArrayList<>();
        mDatabase.child("BibliographyDetail").child("sunghiep").child("amnhac").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String amnhac = (String) dataSnapshot.getValue();
                listString.add(amnhac);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("BibliographyDetail").child("sunghiep").child("kientruc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String kientruc = (String) dataSnapshot.getValue();
                listString.add(kientruc);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("BibliographyDetail").child("sunghiep").child("vanhoc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String vanhoc = (String) dataSnapshot.getValue();
                listString.add(vanhoc);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return (ArrayList<String>) listString;
    }

}
