package com.nguyenvinhtien.music.activities.other;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.UpdateKienTrucActivity;
import com.nguyenvinhtien.music.activities.hot_album.UpdateAlbumActivity;
import com.nguyenvinhtien.music.activities.bibliography.UpdateBibliographyActivity;
import com.nguyenvinhtien.music.activities.newspaper.UpdateNewsActivity;
import com.nguyenvinhtien.music.activities.hit_song.UpdateSongActivity;
import com.nguyenvinhtien.music.activities.van_hoc.UpdateVanHocActivity;
import com.nguyenvinhtien.music.activities.hot_video.UpdateVideoActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateDataActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnUpdateSong;
    Button btnUpdateAlbum;
    Button btnUpdateVideo;
    Button btnUpdateTieuSu;
    Button btnUpdateKienTruc;
    Button btnUpdateBaiBao;
    Button btnUpdateVanHoc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_data);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_data);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnUpdateSong = (Button) findViewById(R.id.btn_UpdateSong);
        btnUpdateAlbum = (Button) findViewById(R.id.btn_UpdateAlbum);
        btnUpdateVideo = (Button)findViewById(R.id.btn_UpdateVideo);
        btnUpdateKienTruc = (Button) findViewById(R.id.btn_UpdateKienTruc);
        btnUpdateTieuSu = (Button) findViewById(R.id.btn_UpdateTieuSu);
        btnUpdateBaiBao = (Button) findViewById(R.id.btn_UpdateBaiBao);
        btnUpdateVanHoc = (Button) findViewById(R.id.btn_UpdateVanHoc);
    }

    public void initEvents(){
        btnUpdateSong.setOnClickListener(this);
        btnUpdateAlbum.setOnClickListener(this);
        btnUpdateVideo.setOnClickListener(this);
        btnUpdateBaiBao.setOnClickListener(this);
        btnUpdateKienTruc.setOnClickListener(this);
        btnUpdateVanHoc.setOnClickListener(this);
        btnUpdateTieuSu.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_UpdateSong:
                Intent intent_update_song = new Intent(getApplicationContext(), UpdateSongActivity.class);
                startActivity(intent_update_song);
                break;
            case R.id.btn_UpdateAlbum:
                Intent intent_update_album = new Intent(getApplicationContext(), UpdateAlbumActivity.class);
                startActivity(intent_update_album);
                break;
            case R.id.btn_UpdateVideo:
                Intent intent_update_video = new Intent(getApplicationContext(), UpdateVideoActivity.class);
                startActivity(intent_update_video);
                break;
            case R.id.btn_UpdateBaiBao:
                Intent intent_update_news = new Intent(getApplicationContext(), UpdateNewsActivity.class);
                startActivity(intent_update_news);
                break;
            case R.id.btn_UpdateKienTruc:
                Intent intent_update_privacy = new Intent(getApplicationContext(), UpdateKienTrucActivity.class);
                startActivity(intent_update_privacy);
                break;
            case R.id.btn_UpdateVanHoc:
                Intent intent_update_vanhoc = new Intent(getApplicationContext(), UpdateVanHocActivity.class);
                startActivity(intent_update_vanhoc);
                break;
            case R.id.btn_UpdateTieuSu:
                Intent intent_update_bibliography = new Intent(getApplicationContext(), UpdateBibliographyActivity.class);
                startActivity(intent_update_bibliography);
                break;
        }
    }
}
