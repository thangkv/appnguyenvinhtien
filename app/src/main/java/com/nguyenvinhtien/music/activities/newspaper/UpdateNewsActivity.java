package com.nguyenvinhtien.music.activities.newspaper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.activities.update_news.AddNewsActivity;
import com.nguyenvinhtien.music.activities.update_news.DeleteNewsActivity;
import com.nguyenvinhtien.music.activities.update_news.UpdateDataNewsActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateNewsActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddNews;
    Button btnUpdateNews;
    Button btnDeleteNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_news);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnUpdateNews = (Button) findViewById(R.id.btn_UpdateNews);
        btnAddNews = (Button) findViewById(R.id.btn_AddNews);
        btnDeleteNews = (Button)findViewById(R.id.btn_DeleteNews);
    }

    public void initEvents(){
        btnUpdateNews.setOnClickListener(this);
        btnAddNews.setOnClickListener(this);
        btnDeleteNews.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddNews:
                Intent intent_add_news = new Intent(getApplicationContext(), AddNewsActivity.class);
                startActivity(intent_add_news);
                break;
            case R.id.btn_UpdateNews:
                Intent intent_update_news = new Intent(getApplicationContext(), UpdateDataNewsActivity.class);
                startActivity(intent_update_news);
                break;
            case R.id.btn_DeleteNews:
                Intent intent_delete_news = new Intent(getApplicationContext(), DeleteNewsActivity.class);
                startActivity(intent_delete_news);
                break;

        }
    }
}
