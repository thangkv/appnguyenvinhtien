package com.nguyenvinhtien.music.activities.update_bibliography;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateDataPhotoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_PHOTO_REQUEST = 6;
    private static final int IMAGE_GALLERY_REQUEST = 2;
    TextView tvPhotoSelect;
    Button btnSelectPhoto, btnUpdatePhoto;
    EditText edtPhotoTitle;
    RelativeLayout rlPhotoDetail;

    ImageButton ibSelectPhoto;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_photo;
    String title_photo;
    String path_photo;


    Uri imageUri = null;
    Uri imageURL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_data_photo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_data_photo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvPhotoSelect = (TextView) findViewById(R.id.tv_photo_select);
        btnSelectPhoto = (Button) findViewById(R.id.btn_select_photo);
        btnUpdatePhoto = (Button) findViewById(R.id.btn_update);
        edtPhotoTitle = (EditText) findViewById(R.id.edt_photo_title);
        rlPhotoDetail = (RelativeLayout) findViewById(R.id.rl_photo_detail);
        ibSelectPhoto = (ImageButton) findViewById(R.id.image_select);
    }

    public void initEvents() {
        btnSelectPhoto.setOnClickListener(this);
        btnUpdatePhoto.setOnClickListener(this);
        ibSelectPhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_photo:
                Intent selectVideoIntent = new Intent(UpdateDataPhotoActivity.this, SelectPhotoToUpdateActivity.class);
                startActivityForResult(selectVideoIntent, SELECT_PHOTO_REQUEST);
                break;

            case R.id.image_select:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO_REQUEST && resultCode == RESULT_OK) {
            key_photo = data.getStringExtra("KEY_PHOTO");
            title_photo = data.getStringExtra("TITLE_PHOTO");
            path_photo = data.getStringExtra("PATH_PHOTO");
            updateUI();

        }

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK) {
            imageUri = data.getData();
            ibSelectPhoto.setImageURI(imageUri);
        }

    }

    private void updateUI() {
        if (title_photo != null
                && path_photo != null) {
            tvPhotoSelect.setVisibility(View.VISIBLE);
            tvPhotoSelect.setText(title_photo);
            rlPhotoDetail.setVisibility(View.VISIBLE);
            Glide.with(this).load(path_photo).into(ibSelectPhoto);
            edtPhotoTitle.setText(title_photo);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật hình ảnh...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String photo_title_val = edtPhotoTitle.getText().toString().trim();

        if (!TextUtils.isEmpty(photo_title_val)
                && !tvPhotoSelect.getText().equals("")) {

            mProgress.show();

            final DatabaseReference newPhoto = mDatabase.child("Photos").child(key_photo);

            if (imageUri == null){
                newPhoto.child("title").setValue(photo_title_val);
                Toast.makeText(getApplicationContext(), "Cập nhật hình ảnh thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();
            } else {
                StorageReference imagePath = mStorage.child("Photos").child(key_photo);

                imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageURL = taskSnapshot.getDownloadUrl();
                        newPhoto.child("path").setValue(imageURL.toString());
                        newPhoto.child("title").setValue(photo_title_val);
                        Toast.makeText(getApplicationContext(), "Cập nhật hình ảnh thành công!", Toast.LENGTH_SHORT).show();
                        mProgress.dismiss();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                    }
                });
            }


        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn ảnh và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
