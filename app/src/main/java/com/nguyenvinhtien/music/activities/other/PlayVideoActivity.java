package com.nguyenvinhtien.music.activities.other;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.hot_video.CommentVideoActivity;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.CommentAudioKienTrucActivity;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;

import java.net.MalformedURLException;
import java.net.URL;


public class PlayVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final String YOUTUBE_API_KEY = "AIzaSyDQweIpvTvMHxziNxPnueV1IY3LONvp8gs";

    public static final String VIDEO_LINK = "video_link";


    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;

    private YouTubePlayer youTubePlayer;
    Button btnComment;
    TextView tvDesc;
    DatabaseReference mDatabase;

    String videoID;
    String videoTitle;
    String description;


    private MyPlayerStateChangeListener playerStateChangeListener;
    private MyPlaybackEventListener playbackEventListener;

    String path;
    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_play_video);
        PlayMusicActivity activity = (PlayMusicActivity) AppController.getInstance().getPlayMusicActivity();
        PlayMusicService playMusicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
        if (playMusicService != null) {
            if (activity == null) {
                playMusicService.pauseMusic();
                playMusicService.changePlayPauseState();
                Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                sendBroadcast(intent1);
            } else {
                activity.pauseMusic();
                playMusicService.changePlayPauseState();

                Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                sendBroadcast(intent1);
                Log.d("check", "AUDIOFOCUS_LOSS_TRANSIENT");
            }
        }



        mDatabase = FirebaseDatabase.getInstance().getReference();

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(YOUTUBE_API_KEY, this);

        playerStateChangeListener = new MyPlayerStateChangeListener();
        playbackEventListener = new MyPlaybackEventListener();

        path = getIntent().getStringExtra(VIDEO_LINK);

        videoID = getIntent().getStringExtra(Constants.VIDEO_ID);
        videoTitle = getIntent().getStringExtra(Constants.VIDEO_TITLE);
        description = getIntent().getStringExtra(Constants.PRIVACY_DESC);


        tvDesc = (TextView) findViewById(R.id.tv_description);
        tvDesc.setText(description);

        btnComment = (Button) findViewById(R.id.btn_comment_video);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.getInstance().checkInternetState() == true){
                        Intent intent = new Intent(PlayVideoActivity.this, CommentVideoActivity.class);
                        intent.putExtra(Constants.VIDEO_ID, videoID);
                        intent.putExtra(Constants.VIDEO_TITLE, videoTitle);
                        startActivity(intent);
                } else {
                    Toast.makeText(PlayVideoActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        this.youTubePlayer = youTubePlayer;
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);

        try {
            link = extractYoutubeId(path);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (!b) {
            youTubePlayer.loadVideo(link);

        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), youTubeInitializationResult.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }


    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onPlaying() {
            // Called when playback starts, either due to user action or call to play().

        }

        @Override
        public void onPaused() {
            // Called when playback is paused, either due to user action or call to pause().

        }

        @Override
        public void onStopped() {
            // Called when playback stops for a reason other than being paused.

        }

        @Override
        public void onBuffering(boolean b) {
            // Called when buffering starts or ends.
        }

        @Override
        public void onSeekTo(int i) {
            // Called when a jump in playback position occurs, either
            // due to user scrubbing or call to seekRelativeMillis() or seekToMillis()
        }
    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        @Override
        public void onLoading() {
            // Called when the player is loading a video
            // At this point, it's not ready to accept commands affecting playback such as play() or pause()
        }

        @Override
        public void onLoaded(String s) {
            // Called when a video is done loading.
            // Playback methods such as play(), pause() or seekToMillis(int) may be called after this callback.
        }

        @Override
        public void onAdStarted() {
            // Called when playback of an advertisement starts.
        }

        @Override
        public void onVideoStarted() {
            // Called when playback of the video starts.
        }

        @Override
        public void onVideoEnded() {
            // Called when the video reaches its end.
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            // Called when an error occurs.
        }
    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String id = null;
        try {
            String query = new URL(url).getQuery();
            if (query != null) {
                String[] param = query.split("&");
                for (String row : param) {
                    String[] param1 = row.split("=");
                    if (param1[0].equals("v")) {
                        id = param1[1];
                    }
                }
            } else {
                if (url.contains("youtu.be") || url.contains("embed")) {
                    id = url.substring(url.lastIndexOf("/") + 1);
                }
            }
        } catch (Exception ex) {
            Log.e("Exception", ex.toString());
        }
        return id;
    }
}

