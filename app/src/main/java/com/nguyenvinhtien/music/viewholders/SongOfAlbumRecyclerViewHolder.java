package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;

/**
 * Created by KimVanThang on 4/30/2017.
 */

public class SongOfAlbumRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView tvTitle;
    public TextView tvArtist;
    public TextView tvOption;


    public SongOfAlbumRecyclerViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_song_name_play);
        tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_song_play);
        tvOption = (TextView) itemView.findViewById(R.id.tv_option);
    }
}
