package com.nguyenvinhtien.music.adapter.other;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nguyenvinhtien.music.fragments.FragmentLyric;
import com.nguyenvinhtien.music.fragments.FragmentPlay;
import com.nguyenvinhtien.music.fragments.FragmentSongListPlaying;
import com.nguyenvinhtien.music.models.Song;

import java.util.ArrayList;


public class ViewPagerPlayAdapter extends FragmentStatePagerAdapter {

    ArrayList<Song> mData;
    String coverPath;
    String songPath;
    String type;



    String lyric;
    String noti;


    public ViewPagerPlayAdapter(FragmentManager fm, ArrayList<Song> mData, String coverPath,
                                String songPath, String lyric, String type) {
        super(fm);
        this.mData = mData;
        this.coverPath = coverPath;
        this.lyric = lyric;
        this.songPath = songPath;
        this.type = type;


}

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position){
            case 0:
                fragment = FragmentSongListPlaying.newInstance(mData);

                break;
            case 1:
                fragment = FragmentPlay.newInstance(coverPath, songPath, type);
                break;
            case 2:
                fragment = FragmentLyric.newInstance(lyric);
        }
        return fragment;
    }



    @Override
    public int getCount() {
        return 3;
    }
}
