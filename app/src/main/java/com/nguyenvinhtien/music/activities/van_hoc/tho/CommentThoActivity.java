package com.nguyenvinhtien.music.activities.van_hoc.tho;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.CommentPostActivity;
import com.nguyenvinhtien.music.activities.other.SignInActivity;
import com.nguyenvinhtien.music.models.Comment;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.CommentRecyclerViewHolder;

import java.util.HashMap;
import java.util.Map;

public class CommentThoActivity extends AppCompatActivity {

    private RecyclerView rvListComment;
    private Button btnComment, btnLogin;
    private EditText edtCommentContent;
    private TextView tvHuongdan;
    private LinearLayoutManager linearLayoutManager;
    private FirebaseRecyclerAdapter<Comment, CommentRecyclerViewHolder> adapter;

    private DatabaseReference mDatabase;
    private FirebaseUser user;

    private String key_news;
    private String title_post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_tho);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_comment_news);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        key_news = getIntent().getStringExtra(Constants.POST_ID);
        title_post = getIntent().getStringExtra(Constants.POST_TITLE);

        rvListComment = (RecyclerView) findViewById(R.id.rv_comment_news);
        btnComment = (Button) findViewById(R.id.btn_comment);
        btnLogin = (Button) findViewById(R.id.btn_Login);
        edtCommentContent = (EditText) findViewById(R.id.edt_comment);
        tvHuongdan = (TextView) findViewById(R.id.tv_huongdan);

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            tvHuongdan.setText("Bạn cần đăng nhập để thực hiện chức năng bình luận");
            edtCommentContent.setVisibility(View.INVISIBLE);
            btnComment.setVisibility(View.INVISIBLE);
            btnLogin.setVisibility(View.VISIBLE);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });


        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.getInstance().checkInternetState() == true) {
                    submitCommentContent();
                    edtCommentContent.setText("");
                } else {
                    Toast.makeText(CommentThoActivity.this, "Không có kết nối mạng", Toast.LENGTH_SHORT).show();
                }
            }
        });


        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        rvListComment.setLayoutManager(linearLayoutManager);


        Query queryComment = mDatabase.child("Tho-Comments").child(key_news);

        adapter = new FirebaseRecyclerAdapter<Comment, CommentRecyclerViewHolder>(Comment.class, R.layout.item_comment, CommentRecyclerViewHolder.class, queryComment) {
            @Override
            protected void populateViewHolder(CommentRecyclerViewHolder viewHolder, Comment model, int position) {
                viewHolder.tvAuthorComment.setText(model.author);
                viewHolder.tvContentComment.setText(model.content);
                viewHolder.imageViewAva.setImageResource(R.drawable.user_ava);
            }
        };

        rvListComment.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void submitCommentContent() {
        String commentContent = edtCommentContent.getText().toString().trim();

        if (TextUtils.isEmpty(commentContent)) {
            edtCommentContent.setError("Yêu cầu nhập text");
            return;
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userID = user.getUid();
        String userEmail = user.getEmail();

        Comment comment = new Comment(userID, userEmail, commentContent);
        Map<String, Object> commentValues = comment.toMap();

        String key = mDatabase.child("Tho-Comments").child(key_news).push().getKey();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("Tho-Comments/" + key_news + "/" + key, commentValues);

        mDatabase.updateChildren(childUpdates);

        //AppController.getInstance().sendNotificationAddThoComment(key_news, commentContent, title_post);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}


