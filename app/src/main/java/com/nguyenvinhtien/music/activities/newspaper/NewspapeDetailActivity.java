package com.nguyenvinhtien.music.activities.newspaper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;


import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

public class NewspapeDetailActivity extends AppCompatActivity {

    WebView webview;
    Button btnComment;
    TextView tvTitleNews;

    private ProgressDialog progressDialog;
    String newsLink;
    String newsID;
    String newsTitle;

    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail_newspaper);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail_news);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        newsLink = intent.getStringExtra(Constants.NEWS_LINK);
        newsID = intent.getStringExtra(Constants.NEWS_ID);
        newsTitle = intent.getStringExtra(Constants.NEWS_TITLE);

        tvTitleNews = (TextView)findViewById(R.id.tv_news);

        mDatabase.child("News").child(newsID).child("title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String tieude = (String) dataSnapshot.getValue();
                tvTitleNews.setText(tieude);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        webview = (WebView) findViewById(R.id.webview);

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        progressDialog = ProgressDialog.show(this, "", "Đang tải dữ liệu bài báo...");


        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;

            }

            public void onPageFinished(WebView view, String url) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                alertDialog.setTitle("Lỗi");
                alertDialog.setMessage("Cần bật mạng để đọc báo");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        return;
                    }
                });
                alertDialog.show();
            }
        });
        webview.loadUrl(newsLink);

        btnComment = (Button)findViewById(R.id.btn_comment);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.getInstance().checkInternetState() == true){
                    Intent intent = new Intent(NewspapeDetailActivity.this, CommentNewsActivity.class);
                    intent.putExtra(Constants.NEWS_ID, newsID);
                    intent.putExtra(Constants.NEWS_TITLE, newsTitle);

                    startActivity(intent);
                } else {
                    Toast.makeText(NewspapeDetailActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}



