package com.nguyenvinhtien.music.adapter.cong_trinh;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;


import java.util.ArrayList;

/**
 * Created by Kim Van Thang on 5/29/2017.
 */

public class CongTrinhPhotoUpdateAdapter extends RecyclerView.Adapter<CongTrinhPhotoUpdateAdapter.ViewHolderNews> {


    Activity mContext;
    ArrayList<Uri> mData;

    LayoutInflater mLayoutInflater;

    public CongTrinhPhotoUpdateAdapter(Activity mContext, ArrayList<Uri> mData) {
        this.mContext = mContext;
        this.mData = mData;

        mLayoutInflater = LayoutInflater.from(mContext);


    }

    @Override
    public CongTrinhPhotoUpdateAdapter.ViewHolderNews onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_photo, null);
        CongTrinhPhotoUpdateAdapter.ViewHolderNews holder = new CongTrinhPhotoUpdateAdapter.ViewHolderNews(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final CongTrinhPhotoUpdateAdapter.ViewHolderNews holder, final int position) {

        Uri item = mData.get(position);
        holder.setId(position);
        Glide.with(mContext).load(item).placeholder(R.drawable.picture).centerCrop().into(holder.imgNews);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolderNews extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgNews;

        int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public ViewHolderNews(View itemView) {
            super(itemView);


            imgNews = (ImageView) itemView.findViewById(R.id.photo_img_item);



            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }


    }


}
