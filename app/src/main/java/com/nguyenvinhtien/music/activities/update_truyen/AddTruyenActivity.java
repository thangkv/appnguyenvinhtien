package com.nguyenvinhtien.music.activities.update_truyen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class AddTruyenActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int IMAGE_GALLERY_REQUEST = 2;

    Button btnUpdatePost;
    ImageView ibSelectPostArt;
    EditText edtPostTitle, edtPostContent;
    Uri imageUri = null;
    ProgressDialog mProgress;

    StorageReference mStorage;
    DatabaseReference mDatabase;

    Uri imageURL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_truyen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_post);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        mProgress = new ProgressDialog(this);

        btnUpdatePost = (Button)findViewById(R.id.btn_update);
        ibSelectPostArt = (ImageView) findViewById(R.id.image_post);
        edtPostTitle = (EditText)findViewById(R.id.edt_title_post);
        edtPostContent = (EditText) findViewById(R.id.edt_content_post);
    }

    public void initEvents(){

        btnUpdatePost.setOnClickListener(this);
        ibSelectPostArt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_post:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            Glide.with(this).load(imageUri).into(ibSelectPostArt);
        }
    }

    private void startUpdate() {
        mProgress.setMessage("Đang thêm Truyện đọc...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);


        final String post_title_val = edtPostTitle.getText().toString().trim();
        final String post_content_val = edtPostContent.getText().toString().trim();

        if (!TextUtils.isEmpty(post_title_val)
                && !TextUtils.isEmpty(post_content_val)
                && imageUri != null){

            mProgress.show();

            final DatabaseReference newPost = mDatabase.child("Truyens").push();
            String key_post = newPost.getKey();

            StorageReference imagePath = mStorage.child("TruyenArts").child(key_post);

            imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imageURL = taskSnapshot.getDownloadUrl();

                    newPost.child("title").setValue(post_title_val);
                    newPost.child("imagePath").setValue(imageURL.toString());
                    newPost.child("content").setValue(post_content_val);

                    //AppController.getInstance().sendNotificationAddTruyen(post_title_val);

                    Toast.makeText(getApplicationContext(), "Thêm truyện thành công!", Toast.LENGTH_SHORT).show();
                    mProgress.dismiss();
                    finish();


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Cần điền đủ các trường và chọn ảnh", Toast.LENGTH_SHORT).show();
        }
    }
}
