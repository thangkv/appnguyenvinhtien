package com.nguyenvinhtien.music.activities.other;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.update_grant_permission.GrantPermissionActivity;
import com.nguyenvinhtien.music.models.User;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 111;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public static GoogleApiClient mGoogleApiClient;
    private SignInButton btnSignIn;
    private TextView tvDisplayName, tvEmail;
    private Button btnSignOut;
    private Button btnUpdateDatabase;
    private Button btnGrantPermission;
    CircleImageView ivAccount;

    private static DatabaseReference mDatabase;

    private ProgressDialog mProgress;
    private static ArrayList<String> listAdminUser;
    private static ArrayList<String> listSpecialUser;

    Activity mainActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_in);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_sign_in);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mainActivity = AppController.getInstance().getMainActivity();

        mProgress = new ProgressDialog(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    btnSignOut.setVisibility(View.VISIBLE);
                    btnSignIn.setVisibility(View.GONE);
                    tvDisplayName.setVisibility(View.VISIBLE);
                    tvDisplayName.setText(user.getDisplayName());
                    tvEmail.setVisibility(View.VISIBLE);
                    tvEmail.setText(user.getEmail());
                    ivAccount.setVisibility(View.VISIBLE);
                    Glide.with(getApplicationContext()).load(user.getPhotoUrl()).into(ivAccount);
                    if (listSpecialUser != null && listAdminUser != null){
                        if (listSpecialUser.contains(user.getEmail())) {
                            btnGrantPermission.setVisibility(View.INVISIBLE);
                            btnUpdateDatabase.setVisibility(View.VISIBLE);
                        } else if (listAdminUser.contains(user.getEmail())) {
                            btnUpdateDatabase.setVisibility(View.VISIBLE);
                            btnGrantPermission.setVisibility(View.VISIBLE);
                        } else {
                            btnGrantPermission.setVisibility(View.INVISIBLE);
                            btnUpdateDatabase.setVisibility(View.INVISIBLE);
                        }
                    }

                }
            }
        };


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(SignInActivity.this, "Xảy ra lỗi!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signOut() {
        mainActivity.recreate();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {

            }
        });
    }

    public void initControls() {
        tvDisplayName = (TextView) findViewById(R.id.tv_displayName);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        btnSignIn = (SignInButton) findViewById(R.id.btnSignIn);
        TextView textView = (TextView) btnSignIn.getChildAt(0);
        textView.setText("Đăng nhập");
        btnUpdateDatabase = (Button) findViewById(R.id.btnUpdate);
        btnGrantPermission = (Button) findViewById(R.id.btnGrantPermission);
        btnSignOut = (Button) findViewById(R.id.btnSignOut);
        ivAccount = (CircleImageView) findViewById(R.id.iv_account);
    }

    public void initEvents() {
        btnSignIn.setOnClickListener(this);
        btnSignOut.setOnClickListener(this);
        btnUpdateDatabase.setOnClickListener(this);
        btnGrantPermission.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdate:

                Intent intent = new Intent(SignInActivity.this, UpdateDataActivity.class);
                startActivity(intent);

                break;
            case R.id.btnSignIn:
                if (AppController.getInstance().checkInternetState() == false) {
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    signIn();
                }
                break;
            case R.id.btnSignOut:

                mAuth.signOut();
                signOut();
                btnSignIn.setVisibility(View.VISIBLE);
                btnSignOut.setVisibility(View.GONE);
                btnUpdateDatabase.setVisibility(View.INVISIBLE);
                tvDisplayName.setVisibility(View.INVISIBLE);
                tvEmail.setVisibility(View.INVISIBLE);
                ivAccount.setVisibility(View.INVISIBLE);

                break;
            case R.id.btnGrantPermission:
                Intent intent_grant_permission = new Intent(SignInActivity.this, GrantPermissionActivity.class);
                intent_grant_permission.putExtra("LIST_ADMIN_USER", listAdminUser);
                intent_grant_permission.putExtra("LIST_SPECIAL_USER", listSpecialUser);
                startActivity(intent_grant_permission);

                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            mProgress.setMessage("Đang đăng nhập...");
            mProgress.show();

            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                mProgress.dismiss();

            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        //thay đổi ảnh đại diện khi đăng nhập
                        mainActivity.recreate();
                        writeUser();

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(SignInActivity.this, "Đăng nhập thất bại!",
                                    Toast.LENGTH_SHORT).show();
                        }

                        mProgress.dismiss();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
//                mainActivity.recreate();
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void writeUser() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        try{
            User user = new User(currentUser.getEmail());
            mDatabase.child("users").child(currentUser.getUid()).setValue(user);
        } catch (Exception e){
        }


    }


    public static ArrayList<String> getListEmailAdmin() {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (listAdminUser == null) {
            listAdminUser = new ArrayList<>();
            mDatabase.child("Permission-Users").child("Admins").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String email = (String) dataSnapshot.child("email").getValue();
                    listAdminUser.add(email);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    String email = (String) dataSnapshot.child("email").getValue();
                    listAdminUser.remove(email);
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return listAdminUser;
    }

    public static ArrayList<String> getListEmailSpecialUser() {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (listSpecialUser == null) {
            listSpecialUser = new ArrayList<>();
            mDatabase.child("Permission-Users").child("Special-Users").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String email = (String) dataSnapshot.child("email").getValue();
                    listSpecialUser.add(email);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    String email = (String) dataSnapshot.child("email").getValue();
                    listSpecialUser.remove(email);
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return listSpecialUser;
    }

}
