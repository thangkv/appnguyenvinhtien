package com.nguyenvinhtien.music.activities.hot_album;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.adapter.song.SongOnlineListFirebaseAdapter;
import com.nguyenvinhtien.music.adapter.song.SongOnlineListOfAlbumFirebaseAdapter;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.SongOfAlbumRecyclerViewHolder;


public class AlbumOnlineListActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String ALBUM_KEY = "album_key";

    RecyclerView mRvSongList;
    ImageView mIvAlbumCover;
    ImageView mIvAlbumCoverMini;
    TextView tvAlbumTitleMini;
    TextView tvAlbumArtistMini;


    SongOnlineListOfAlbumFirebaseAdapter mAdapter;
    private DatabaseReference mDatabase;

    TextView tvAlbumInfo;
    public String mAlbumId;
    String artist, title, path;


    public LinearLayout currentPlayingBar;
    PlayMusicService musicService;
    ImageView btnPlayPauseCurrent;
    ImageView btnNextCurrent;
    ImageView btnPrevCurrent;
    ImageView imgAlbumArtCurrent;
    TextView tvTitle;
    TextView tvArtist;


    String currentPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_album_online_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_album_online_detail);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Common.setStatusBarTranslucent(true, this);

        AppController.getInstance().setAlbumDetailActivity(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        mAlbumId = intent.getExtras().getString(SongOnlineListFirebaseAdapter.ALBUM_KEY_ONLINE);

        initControls();
        initEvents();
        getAndShowSongList();
        showCover();


        if (musicService != null) {
            updatePlayingState();
            showCurrentSong();
        }

        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerBroadcastUpdatePlayingAlbumOnlineList();

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }

    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                Toast.makeText(context, "Tải về thành công!", Toast.LENGTH_SHORT).show();
            }
        }
    };


    BroadcastReceiver broadcastReceiverUpdatePlayingAlbumOnlineList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
            if (musicService != null) {
                currentPlayingBar.setVisibility(View.VISIBLE);

            } else {
                currentPlayingBar.setVisibility(View.GONE);
            }
            showCurrentSong();
            updatePlayingState();
        }
    };

    private void registerBroadcastUpdatePlayingAlbumOnlineList() {
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_UPDATE_PLAY_STATUS);
        registerReceiver(broadcastReceiverUpdatePlayingAlbumOnlineList, intentFilter);
    }

    private void unRegisterBroadcastUpdatePlayingAlbumOnlineList() {
        unregisterReceiver(broadcastReceiverUpdatePlayingAlbumOnlineList);
    }

    public void showCurrentSong() {
        if (musicService != null) {
            tvTitle.setText(musicService.getCurrentSong().getTitle());
            tvArtist.setText(musicService.getCurrentSong().getArtist());
            String albumPath = musicService.getCurrentSong().getAlbumImagePath();

            currentPath = musicService.getCurrentSong().getPath();

            if (albumPath != null) {
                if (albumPath.contains("firebasestorage")) {
                    if (albumPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    } else {
                        Glide.with(this).load(albumPath).into(imgAlbumArtCurrent);
                    }
                } else {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(currentPath);

                    byte[] image = mmr.getEmbeddedPicture();
                    if (image != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                        imgAlbumArtCurrent.setImageBitmap(bitmap);
                    } else {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    }
                }
            } else {
                imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
            }
        }
    }

    public void updatePlayingState() {
        if (musicService != null) {
            if (musicService.isPlaying()) {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
            } else {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
            }
        }
    }

    private void initControls() {
        mRvSongList = (RecyclerView) findViewById(R.id.rv_album_list_play);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRvSongList.setLayoutManager(layoutManager);
        tvAlbumInfo = (TextView) findViewById(R.id.tv_album_info);
        tvAlbumInfo.setMovementMethod(new ScrollingMovementMethod());
        mIvAlbumCover = (ImageView) findViewById(R.id.img_album_list_play);
        mIvAlbumCover.setAlpha(90);
        mIvAlbumCoverMini = (ImageView) findViewById(R.id.img_album_list_play_mini);
        tvAlbumTitleMini = (TextView) findViewById(R.id.tv_title_album_mini);
        tvAlbumArtistMini = (TextView) findViewById(R.id.tv_artist_album_mini);


        currentPlayingBar = (LinearLayout) findViewById(R.id.current_playing_bar);
        btnPlayPauseCurrent = (ImageView) findViewById(R.id.btn_play_pause_current);
        btnNextCurrent = (ImageView) findViewById(R.id.btn_next_current);
        btnPrevCurrent = (ImageView) findViewById(R.id.btn_prev_current);
        imgAlbumArtCurrent = (ImageView) findViewById(R.id.img_album_current_bar);
        tvTitle = (TextView) findViewById(R.id.tv_song_title_current);
        tvTitle.setSelected(true);
        tvArtist = (TextView) findViewById(R.id.tv_artist_current);

        musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        if (musicService != null) {
            currentPlayingBar.setVisibility(View.VISIBLE);
        } else {
            currentPlayingBar.setVisibility(View.GONE);
        }
    }

    private void initEvents() {

        currentPlayingBar.setOnClickListener(this);
        btnPlayPauseCurrent.setOnClickListener(this);
        btnNextCurrent.setOnClickListener(this);
        btnPrevCurrent.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.current_playing_bar:
                if (musicService != null) {
                    Intent intent = new Intent(AlbumOnlineListActivity.this, PlayMusicActivity.class);
                    intent.putExtra(PlayMusicActivity.IS_PLAYING, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
                }
                break;
            case R.id.btn_play_pause_current:
                if (musicService != null) {
                    Intent intent = new Intent(Constants.ACTION_PLAY_PAUSE);
                    if (musicService.isPlaying()) {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
                    } else {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
                    }
                    sendBroadcast(intent);

                }
                break;
            case R.id.btn_next_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_NEXT);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_NEXT);
                        sendBroadcast(intent);
                    }


                }
                break;
            case R.id.btn_prev_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_PREV);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_PREV);
                        sendBroadcast(intent);
                    }

                }
                break;
        }
    }


    private void showCover() {

        mDatabase.child("Albums").child(mAlbumId).child("title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                title = String.valueOf(dataSnapshot.getValue());
                tvAlbumTitleMini.setText(title);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        mDatabase.child("Albums").child(mAlbumId).child("artist").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                artist = String.valueOf(dataSnapshot.getValue());

                tvAlbumArtistMini.setText(artist);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("Albums").child(mAlbumId).child("info").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    tvAlbumInfo.setText(dataSnapshot.getValue().toString());
                } catch (Exception e){

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mDatabase.child("Albums").child(mAlbumId).child("albumArtPath").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                path = String.valueOf(dataSnapshot.getValue());
                if (!path.equals(Constants.DEFAULT_SONG_ART_URL)){
                    Glide.with(getApplicationContext()).load(path).centerCrop().into(mIvAlbumCover);
                    Glide.with(getApplicationContext()).load(path).into(mIvAlbumCoverMini);
                } else {
                    mIvAlbumCoverMini.setImageResource(R.drawable.default_cover_big);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void getAndShowSongList() {
        Query queryComment = mDatabase.child("Albums-Songs").child(mAlbumId);
        mAdapter = new SongOnlineListOfAlbumFirebaseAdapter(this, Song.class, R.layout.item_song_of_album, SongOfAlbumRecyclerViewHolder.class, queryComment);
        mAdapter.notifyDataSetChanged();
        mRvSongList.setAdapter(mAdapter);

    }

    //thay đổi button Play,Pause khi stop service trên thanh thông báo
    public void changePlayButtonState() {
        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
        unregisterReceiver(receiver);
        unRegisterBroadcastUpdatePlayingAlbumOnlineList();
    }
}
