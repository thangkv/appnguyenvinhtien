package com.nguyenvinhtien.music.activities.hit_song;


import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nguyenvinhtien.music.R;


import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.adapter.song.SongOnlineListFirebaseAdapter;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import com.nguyenvinhtien.music.viewholders.SongRecyclerViewHolder;


import static com.nguyenvinhtien.music.activities.other.MainActivity.SONG_NAME;

public class HitSongActivity extends AppCompatActivity implements View.OnClickListener {

    android.support.v4.widget.SimpleCursorAdapter suggestionAdapter;

    RecyclerView mRvListSong;

    SongOnlineListFirebaseAdapter adapter;

    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference mDatabase;

    TextView tvGiaiThuong;
    LoadListSong loadListSong;

    public LinearLayout currentPlayingBar;
    PlayMusicService musicService;
    ImageView btnPlayPauseCurrent;
    ImageView btnNextCurrent;
    ImageView btnPrevCurrent;
    ImageView imgAlbumArtCurrent;
    TextView tvTitle;
    TextView tvArtist;

    RelativeLayout rlHitSong;

    String currentPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_hit_song);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        final String[] from = new String[]{"songName"};
        final int[] to = new int[]{android.R.id.text1};
        suggestionAdapter = new android.support.v4.widget.SimpleCursorAdapter(this,
                android.R.layout.simple_expandable_list_item_1,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_hit_song);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Common.setStatusBarTranslucent(true, this);

        AppController.getInstance().setHitSongActivity(this);

        initControls();
        initEvents();

        showCurrentSong();

        showGiaiThuong();

        showListSong();


        if (musicService != null) {
            updatePlayingState();
            showCurrentSong();
        }
        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerBroadcastUpdatePlayingHitSong();


        if (AppController.getInstance().checkInternetState() == false) {
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();

        }
    }

    private void showGiaiThuong() {
        mDatabase.child("GiaiThuong").child("content").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String giaithuong = (String) dataSnapshot.getValue();
                tvGiaiThuong.setText(giaithuong);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showListSong() {
        loadListSong = new LoadListSong();
        loadListSong.execute();
    }

    private class LoadListSong extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Query queryComment = mDatabase.child("Songs");
            adapter = new SongOnlineListFirebaseAdapter(HitSongActivity.this, Song.class, R.layout.item_online_song, SongRecyclerViewHolder.class, queryComment);
            adapter.notifyDataSetChanged();
            return adapter;
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            mRvListSong.setAdapter(adapter);

        }
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                Toast.makeText(context, "Tải về thành công!", Toast.LENGTH_SHORT).show();

            }
        }
    };

    BroadcastReceiver broadcastReceiverUpdatePlayingHitSong = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
            if (musicService != null) {
                currentPlayingBar.setVisibility(View.VISIBLE);

            } else {
                currentPlayingBar.setVisibility(View.GONE);
            }
            showCurrentSong();
            updatePlayingState();
        }
    };

    private void registerBroadcastUpdatePlayingHitSong() {
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_UPDATE_PLAY_STATUS);
        registerReceiver(broadcastReceiverUpdatePlayingHitSong, intentFilter);
    }

    private void unRegisterBroadcastUpdatePlayingHitSong() {
        unregisterReceiver(broadcastReceiverUpdatePlayingHitSong);
    }

    public void showCurrentSong() {
        if (musicService != null) {
            tvTitle.setText(musicService.getCurrentSong().getTitle());
            tvArtist.setText(musicService.getCurrentSong().getArtist());
            String albumPath = musicService.getCurrentSong().getAlbumImagePath();

            currentPath = musicService.getCurrentSong().getPath();

            if (albumPath != null) {
                if (albumPath.contains("firebasestorage")) {
                    if (albumPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    } else {
                        Glide.with(this).load(albumPath).into(imgAlbumArtCurrent);
                    }
                } else {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(currentPath);

                    byte[] image = mmr.getEmbeddedPicture();
                    if (image != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                        imgAlbumArtCurrent.setImageBitmap(bitmap);
                    } else {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    }
                }
            } else {
                imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
            }
        }
    }

    public void updatePlayingState() {
        if (musicService != null) {
            if (musicService.isPlaying()) {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
            } else {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
            }
        }
    }

    private void initEvents() {

        currentPlayingBar.setOnClickListener(this);
        btnPlayPauseCurrent.setOnClickListener(this);
        btnNextCurrent.setOnClickListener(this);
        btnPrevCurrent.setOnClickListener(this);
    }

    private void initControls() {


        rlHitSong = (RelativeLayout) findViewById(R.id.rl_hit_song);

        mRvListSong = (RecyclerView) findViewById(R.id.rv_hit_song);
        linearLayoutManager = new LinearLayoutManager(this);
        mRvListSong.setLayoutManager(linearLayoutManager);

        tvGiaiThuong = (TextView) findViewById(R.id.tv_giai_thuong);
        tvGiaiThuong.setMovementMethod(new ScrollingMovementMethod());


        currentPlayingBar = (LinearLayout) findViewById(R.id.current_playing_bar);
        btnPlayPauseCurrent = (ImageView) findViewById(R.id.btn_play_pause_current);
        btnNextCurrent = (ImageView) findViewById(R.id.btn_next_current);
        btnPrevCurrent = (ImageView) findViewById(R.id.btn_prev_current);
        imgAlbumArtCurrent = (ImageView) findViewById(R.id.img_album_current_bar);
        tvTitle = (TextView) findViewById(R.id.tv_song_title_current);
        tvTitle.setSelected(true);
        tvArtist = (TextView) findViewById(R.id.tv_artist_current);

        musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();


        if (musicService != null) {
            currentPlayingBar.setVisibility(View.VISIBLE);
        } else {
            currentPlayingBar.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.current_playing_bar:
                if (musicService != null) {
                    Intent intent = new Intent(HitSongActivity.this, PlayMusicActivity.class);
                    intent.putExtra(PlayMusicActivity.IS_PLAYING, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
                }
                break;
            case R.id.btn_play_pause_current:
                if (musicService != null) {
                    Intent intent = new Intent(Constants.ACTION_PLAY_PAUSE);
                    if (musicService.isPlaying()) {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
                    } else {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
                    }
                    sendBroadcast(intent);
                    // showCurrentSong();
                }
                break;
            case R.id.btn_next_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")) {
                        if (AppController.getInstance().checkInternetState() == false) {
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_NEXT);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_NEXT);
                        sendBroadcast(intent);
                    }


                }
                break;
            case R.id.btn_prev_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")) {
                        if (AppController.getInstance().checkInternetState() == false) {
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_PREV);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_PREV);
                        sendBroadcast(intent);
                    }

                }
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void changePlayButtonState() {
        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_song_list, menu);
        MenuItem item = menu.findItem(R.id.action_search_detail);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        searchAutoComplete.setTextColor(Color.WHITE);
        searchAutoComplete.setHint("Nhập tên bài hát");
        searchAutoComplete.setHintTextColor(Color.GRAY);

        ImageView closeButtonImage = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButtonImage.setColorFilter(Color.WHITE);

        searchView.setSuggestionsAdapter(suggestionAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getSuggestion(newText);
                return true;
            }
        });

        //Khi click vao goi y se goi lop List Activity hien thi list cac bai hat goi y
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                suggestionAdapter.getCursor().moveToPosition(position); //move to row
                String query = suggestionAdapter.getCursor().getString(1); //move to column
                //create intent
                Intent intent = new Intent(HitSongActivity.this, SuggestSongListActivity.class);
                intent.putExtra(SONG_NAME, query);
                startActivity(intent);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    //suggest search, Lay ra cac goi y
    private void getSuggestion(String text) {
        //Suggestion is a matrix which has 2 columns and n rows. Column 0 is ID, column 1 is songName.
        MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "songName"});
        for (int i = 0; i < Song.getListSong().size(); i++) {
            //if songName contains input text, add them on suggestion
            if (Song.getListSong().get(i).getTitle().toLowerCase().contains(text.toLowerCase())) {
                c.addRow(new Object[]{i, Song.getListSong().get(i).getTitle()});
            }
        }

        suggestionAdapter.changeCursor(c);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
        unRegisterBroadcastUpdatePlayingHitSong();
        unregisterReceiver(receiver);
    }


}
