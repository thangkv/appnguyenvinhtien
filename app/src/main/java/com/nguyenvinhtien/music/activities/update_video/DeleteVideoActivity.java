package com.nguyenvinhtien.music.activities.update_video;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class DeleteVideoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_VIDEO_REQUEST = 6;
    TextView tvVideoSelect;
    Button btnSelectVideo, btnUpdateVideo;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_video;
    String title_video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_delete_video);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_delete_video);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvVideoSelect = (TextView) findViewById(R.id.tv_video_select);
        btnSelectVideo = (Button) findViewById(R.id.btn_select_video);
        btnUpdateVideo = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnSelectVideo.setOnClickListener(this);
        btnUpdateVideo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_video:
                Intent selectVideoIntent = new Intent(DeleteVideoActivity.this, SelectVideoToUpdateActivity.class);
                startActivityForResult(selectVideoIntent, SELECT_VIDEO_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_VIDEO_REQUEST && resultCode == RESULT_OK) {
            key_video = data.getStringExtra("KEY_VIDEO");
            title_video = data.getStringExtra("TITLE_VIDEO");
            updateUI();

        }

    }

    private void updateUI() {
        if (title_video != null) {
            tvVideoSelect.setVisibility(View.VISIBLE);
            tvVideoSelect.setText(title_video);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Xóa video...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
        if (!tvVideoSelect.getText().equals("")){


            mProgress.show();

            DatabaseReference video = mDatabase.child("Videos").child(key_video);
            video.removeValue();

            DatabaseReference video_comment = mDatabase.child("Video-Comments").child(key_video);
            video_comment.removeValue();

            StorageReference imagePath = mStorage.child("VideoArts").child(key_video);
            imagePath.delete();

            Toast.makeText(getApplicationContext(), "Xóa video thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();
        } else {
            Toast.makeText(this, "Cần chọn video muốn xóa", Toast.LENGTH_SHORT).show();
        }
    }
}
