package com.nguyenvinhtien.music.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.hit_song.CommentSongActivity;
import com.nguyenvinhtien.music.activities.hit_song.HitSongActivity;
import com.nguyenvinhtien.music.activities.hot_album.HotAlbumActivity;
import com.nguyenvinhtien.music.activities.hot_video.CommentVideoActivity;
import com.nguyenvinhtien.music.activities.hot_video.HotVideoActivity;
import com.nguyenvinhtien.music.activities.kien_truc.cong_trinh.CommentCongTrinhActivity;
import com.nguyenvinhtien.music.activities.newspaper.CommentNewsActivity;

import com.nguyenvinhtien.music.activities.newspaper.NewspaperListActivity;
import com.nguyenvinhtien.music.activities.bibliography.CommentPhotoActivity;
import com.nguyenvinhtien.music.activities.bibliography.BiblioPhotoActivity;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.CommentPostActivity;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.HotPostPrivacyActivity;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.CommentAudioKienTrucActivity;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.AudioKienTrucActivity;
import com.nguyenvinhtien.music.activities.van_hoc.audio_van_hoc.AudioVanHocActivity;
import com.nguyenvinhtien.music.activities.van_hoc.audio_van_hoc.CommentAudioVanHocActivity;
import com.nguyenvinhtien.music.activities.van_hoc.tho.CommentThoActivity;
import com.nguyenvinhtien.music.activities.van_hoc.tho.ThoActivity;
import com.nguyenvinhtien.music.activities.van_hoc.truyen.CommentTruyenActivity;
import com.nguyenvinhtien.music.activities.van_hoc.truyen.TruyenActivity;
import com.nguyenvinhtien.music.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kim Van Thang on 5/22/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String key;
    String message;
    String title;
    String song_title;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        if (remoteMessage.getNotification() != null) {
            Log.d("Kiem_tra", "Message Notification Body: " + remoteMessage.getNotification().getBody());

        }

        if (remoteMessage.getData().size() > 0) {

            try {
                title = new JSONObject(remoteMessage.getData()).getString("title");
                message = new JSONObject(remoteMessage.getData()).getString("message");
                key = new JSONObject(remoteMessage.getData()).getString("key");
                song_title = new JSONObject(remoteMessage.getData()).getString("song_title");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("Kiem_tra", "Message Notification Body: " + message);


        }
        sendNotification(message);

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }


    private void sendNotification(String messageBody) {
        PendingIntent pendingIntent = null;

        Intent intent = null;

        if (title.contains("Bài báo")) {
            intent = new Intent(this, NewspaperListActivity.class);
        } else if (title.contains("Album")) {
            intent = new Intent(this, HotAlbumActivity.class);
        } else if (title.contains("Bài hát")) {
            intent = new Intent(this, HitSongActivity.class);
        } else if (title.contains("Bài viết")) {
            intent = new Intent(this, HotPostPrivacyActivity.class);
        } else if (title.contains("Bài thơ")) {
            intent = new Intent(this, ThoActivity.class);
        } else if (title.contains("Truyện đọc")) {
            intent = new Intent(this, TruyenActivity.class);
        } else if (title.contains("Video")) {
            intent = new Intent(this, HotVideoActivity.class);
        } else if (title.contains("Hình ảnh")) {
            intent = new Intent(this, BiblioPhotoActivity.class);
        } else if (title.contains("File")) {
            intent = new Intent(this, AudioVanHocActivity.class);
        } else if (title.contains("Audio")) {
            intent = new Intent(this, AudioKienTrucActivity.class);
        } else if (title.contains("bình luận bài báo")) {
            intent = new Intent(this, CommentNewsActivity.class);
            intent.putExtra(Constants.NEWS_ID, key);
        } else if (title.contains("bình luận bài viết")) {
            intent = new Intent(this, CommentPostActivity.class);
            intent.putExtra(Constants.POST_ID, key);
        } else if (title.contains("bình luận video")) {
            intent = new Intent(this, CommentVideoActivity.class);
            intent.putExtra(Constants.VIDEO_ID, key);
        } else if (title.contains("bình luận hình ảnh")) {
            intent = new Intent(this, CommentPhotoActivity.class);
            intent.putExtra(Constants.PHOTO_ID, key);
        } else if (title.contains("bình luận bài hát")) {
            intent = new Intent(this, CommentSongActivity.class);
            intent.putExtra(Constants.SONG_ID, key);
            intent.putExtra(Constants.SONG_TITLE, song_title);
            intent.putExtra(Constants.NOTI, "yes");
        } else if (title.contains("bình luận audio kiến trúc")) {
            intent = new Intent(this, CommentAudioKienTrucActivity.class);
            intent.putExtra(Constants.SONG_ID, key);
            intent.putExtra(Constants.SONG_TITLE, song_title);
            intent.putExtra(Constants.NOTI, "yes");
        } else if (title.contains("bình luận file âm thanh văn học")) {
            intent = new Intent(this, CommentAudioVanHocActivity.class);
            intent.putExtra(Constants.SONG_ID, key);
            intent.putExtra(Constants.SONG_TITLE, song_title);
            intent.putExtra(Constants.NOTI, "yes");
        } else if (title.contains("bình luận bài thơ")) {
            intent = new Intent(this, CommentThoActivity.class);
            intent.putExtra(Constants.POST_ID, key);
        } else if (title.contains("bình luận truyện")) {
            intent = new Intent(this, CommentTruyenActivity.class);
            intent.putExtra(Constants.POST_ID, key);
        } else if (title.contains("bình luận công trình")) {
            intent = new Intent(this, CommentCongTrinhActivity.class);
            intent.putExtra(Constants.POST_ID, key);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_music)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

}
