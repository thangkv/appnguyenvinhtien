package com.nguyenvinhtien.music.activities.update_post;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateDataPostActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_POST_REQUEST = 6;
    private static final int IMAGE_GALLERY_REQUEST = 2;

    Button btnSelectPost, btnUpdatePost;
    ImageView imagePost;
    EditText edtPostTitle, edtPostContent;
    RelativeLayout rlPostDetail;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_post;
    String title_post;
    String imagePath_post;
    String content_post;

    Uri imageUri = null;
    Uri imageURL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_data_post);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_data_post);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        rlPostDetail = (RelativeLayout) findViewById(R.id.rl_detail_post);
        btnSelectPost = (Button) findViewById(R.id.btn_select_post);
        btnUpdatePost = (Button) findViewById(R.id.btn_update);
        edtPostTitle = (EditText) findViewById(R.id.edt_title_post);
        edtPostContent = (EditText) findViewById(R.id.edt_content_post);
        imagePost = (ImageView) findViewById(R.id.image_post);

    }

    public void initEvents() {
        btnSelectPost.setOnClickListener(this);
        btnUpdatePost.setOnClickListener(this);
        imagePost.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_post:
                Intent selectVideoIntent = new Intent(UpdateDataPostActivity.this, SelectPostToUpdateActivity.class);
                startActivityForResult(selectVideoIntent, SELECT_POST_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;

            case R.id.image_post:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_POST_REQUEST && resultCode == RESULT_OK) {
            key_post = data.getStringExtra("KEY_POST");
            title_post = data.getStringExtra("TITLE_POST");
            imagePath_post = data.getStringExtra("IMAGE_PATH_POST");
            content_post = data.getStringExtra("CONTENT_POST");
            updateUI();

        }

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            imagePost.setImageURI(imageUri);
        }

    }

    private void updateUI() {
        if (title_post != null
                && content_post != null
                && imagePath_post != null) {
            rlPostDetail.setVisibility(View.VISIBLE);
            Glide.with(this).load(imagePath_post).into(imagePost);
            edtPostTitle.setText(title_post);
            edtPostContent.setText(content_post);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật Bài viết...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String post_title_val = edtPostTitle.getText().toString().trim();
        final String post_content_val = edtPostContent.getText().toString().trim();

        if (!TextUtils.isEmpty(post_title_val)
                && !TextUtils.isEmpty(post_content_val)
                && !title_post.equals("")) {

            mProgress.show();

            final DatabaseReference newVideo = mDatabase.child("Posts").child(key_post);

            if (imageUri != null){
                StorageReference imagePath = mStorage.child("PostArts").child(key_post);
                imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageURL = taskSnapshot.getDownloadUrl();
                        newVideo.child("imagePath").setValue(imageURL.toString());
                        newVideo.child("title").setValue(post_title_val);
                        newVideo.child("content").setValue(post_content_val);
                        Toast.makeText(getApplicationContext(), "Cập nhật bài viết thành công!", Toast.LENGTH_SHORT).show();
                        mProgress.dismiss();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                newVideo.child("title").setValue(post_title_val);
                newVideo.child("content").setValue(post_content_val);
                Toast.makeText(getApplicationContext(), "Cập nhật bài viết thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();
            }


        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn bài viết và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
