package com.nguyenvinhtien.music.activities.update_album;

import android.app.ProgressDialog;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

public class AddAlbumActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_SONG_REQUEST = 3;

    Button btnSelectSong, btnUpdateAlbum;
    ImageButton ibSelectAlbumArt;
    EditText edtAlbumTitle, edtAlbumArtist, edtAlbumInfo;

    ProgressDialog mProgress;
    TextView tvSelectSong;
    TextView tvListSelectSong;
    TextView tvAlbumArt;

    ArrayList<String> key_list;
    ArrayList<String> title_list;
    ArrayList<String> artist_list;
    ArrayList<String> image_path_list;
    ArrayList<String> song_path_list;
    ArrayList<String> song_lyric_list;



    StorageReference mStorage;
    DatabaseReference mDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_album);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_album);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();


    }

    private void updateUI() {
        if (title_list != null) {
            for (String title : title_list) {
                tvSelectSong.setVisibility(View.VISIBLE);
                tvSelectSong.setText(title_list.size() + " bài hát");
                tvListSelectSong.append(title + '\n');
            }
        }


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initControls() {

        mProgress = new ProgressDialog(this);
        btnSelectSong = (Button) findViewById(R.id.btn_select_song);
        btnUpdateAlbum = (Button) findViewById(R.id.btn_update);
        ibSelectAlbumArt = (ImageButton) findViewById(R.id.image_select);
        edtAlbumTitle = (EditText) findViewById(R.id.edt_album_title);
        edtAlbumArtist = (EditText) findViewById(R.id.edt_album_artist);
        edtAlbumInfo = (EditText) findViewById(R.id.edt_album_info);
        tvListSelectSong = (TextView) findViewById(R.id.tv_list_select_song);
        tvSelectSong = (TextView) findViewById(R.id.tv_select_song);
        tvListSelectSong.setMovementMethod(new ScrollingMovementMethod());
        tvAlbumArt = (TextView) findViewById(R.id.tv_album_art);
    }

    private void initEvents() {
        btnSelectSong.setOnClickListener(this);
        btnUpdateAlbum.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_SONG_REQUEST && resultCode == RESULT_OK) {


            key_list = data.getStringArrayListExtra("LIST_KEY_SONG");
            title_list = data.getStringArrayListExtra("LIST_TITLE_SONG");
            artist_list = data.getStringArrayListExtra("LIST_ARTIST_SONG");
            image_path_list = data.getStringArrayListExtra("LIST_IMAGE_PATH_SONG");
            song_path_list = data.getStringArrayListExtra("LIST_PATH_SONG");
            song_lyric_list = data.getStringArrayListExtra("LIST_LYRIC_SONG");



            updateUI();

            if (image_path_list != null) {
                tvAlbumArt.setVisibility(View.VISIBLE);
                ibSelectAlbumArt.setVisibility(View.VISIBLE);
                StorageReference reference = FirebaseStorage.getInstance().getReference();
                if (image_path_list.get(0).equals(Constants.DEFAULT_SONG_ART_URL)){
                    reference = reference.child("SongArts").child(Constants.DEFAULT_NAME_SONG_ART);
                }else {
                    reference = reference.child("SongArts").child(key_list.get(0));
                }

                Glide.with(this)
                        .using(new FirebaseImageLoader())
                        .load(reference)
                        .into(ibSelectAlbumArt);

            }

        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_song:
                tvListSelectSong.setText("");
                tvSelectSong.setText("");
                Intent selectSongIntent = new Intent(AddAlbumActivity.this, SelectSongForAlbumActivity.class);
                selectSongIntent.putExtra(SelectSongForAlbumActivity.KIEM_TRA_ACTIVY_GOI, "AddAlbum");
                startActivityForResult(selectSongIntent, SELECT_SONG_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }

                break;
        }
    }

    private void startUpdate() {
        mProgress.setMessage("Đang thêm Album...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);


        final String album_title_val = edtAlbumTitle.getText().toString().trim();
        final String album_artist_val = edtAlbumArtist.getText().toString().trim();
        final String album_info_val = edtAlbumInfo.getText().toString().trim();

        if (!TextUtils.isEmpty(album_title_val) &&
                !TextUtils.isEmpty(album_artist_val) &&
                !TextUtils.isEmpty(album_info_val)
                && !tvSelectSong.getText().equals("")) {

            mProgress.show();

            final DatabaseReference newAlbum = mDatabase.child("Albums").push();

            final String key = newAlbum.getKey();


            newAlbum.child("info").setValue(album_info_val);
            newAlbum.child("title").setValue(album_title_val);
            newAlbum.child("artist").setValue(album_artist_val);

            newAlbum.child("albumArtPath").setValue(image_path_list.get(0));


            DatabaseReference newAlbumSong = mDatabase.child("Albums-Songs").child(key);
            DatabaseReference song = mDatabase.child("Songs");


            //thêm danh sách bài hát được chọn vào album trong Albums-Songs
            for (int i = 0; i < key_list.size(); i++) {
                //thêm bài hát vào Albums-Songs
                newAlbumSong.child(key_list.get(i)).child("title").setValue(title_list.get(i));
                newAlbumSong.child(key_list.get(i)).child("albumImagePath").setValue(image_path_list.get(i));
                newAlbumSong.child(key_list.get(i)).child("path").setValue(song_path_list.get(i));
                newAlbumSong.child(key_list.get(i)).child("lyric").setValue(song_lyric_list.get(i));
                newAlbumSong.child(key_list.get(i)).child("artist").setValue(artist_list.get(i));
                newAlbumSong.child(key_list.get(i)).child("type").setValue("song");


                //thêm giá trị album_key vào bài hát trong Songs
                song.child(key_list.get(i)).child("albums").child(key).setValue(key);


            }

            //AppController.getInstance().sendNotificationAddAlbum(album_title_val, album_artist_val);
            Toast.makeText(getApplicationContext(), "Thêm Album thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();

        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn danh sách bài hát và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }

    }
}
