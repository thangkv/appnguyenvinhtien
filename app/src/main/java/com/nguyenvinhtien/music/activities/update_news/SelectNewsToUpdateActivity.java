package com.nguyenvinhtien.music.activities.update_news;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.models.Newspaper;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class SelectNewsToUpdateActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    RecyclerView listNews;
    FirebaseRecyclerAdapter<Newspaper, SelectNewsToUpdateActivity.SelectNewsRecyclerViewHolder> adapter;
    String newsKey = null;
    String newsTitle = null;
    String newsSource = null;
    String newsLink = null;
    String newsImagePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_news_to_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_news_to_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("News");
        adapter = new FirebaseRecyclerAdapter<Newspaper, SelectNewsToUpdateActivity.SelectNewsRecyclerViewHolder>(Newspaper.class, R.layout.item_select_news, SelectNewsToUpdateActivity.SelectNewsRecyclerViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(final SelectNewsToUpdateActivity.SelectNewsRecyclerViewHolder viewHolder, final Newspaper model, final int position) {
                viewHolder.tvTitle.setText(model.getTitle());
                viewHolder.tvSource.setText(model.getSource());

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        newsKey = getRef(position).getKey();
                        newsTitle = model.getTitle();
                        newsSource = model.getSource();
                        newsLink = model.getLink();
                        newsImagePath = model.getImagePath();
                        viewHolder.itemView.setBackgroundColor(Color.rgb(66,110,180));
                        Intent i = new Intent(SelectNewsToUpdateActivity.this, UpdateDataNewsActivity.class);
                        i.putExtra("KEY_NEWS", newsKey);
                        i.putExtra("TITLE_NEWS", newsTitle);
                        i.putExtra("SOURCE_NEWS", newsSource);
                        i.putExtra("LINK_NEWS", newsLink);
                        i.putExtra("IMAGE_PATH_NEWS", newsImagePath);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });


            }


        };

        initControls();
        initEvents();
        adapter.notifyDataSetChanged();
        listNews.setAdapter(adapter);

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

    public static class SelectNewsRecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvSource;


        public SelectNewsRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_news);
            tvSource = (TextView) itemView.findViewById(R.id.tv_source_news);

        }

    }

    private void initControls() {
        listNews = (RecyclerView) findViewById(R.id.rv_news_list_select);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        listNews.setLayoutManager(layoutManager);

    }

    private void initEvents() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
