package com.nguyenvinhtien.music.adapter.cong_trinh;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.PostDetailActivity;
import com.nguyenvinhtien.music.activities.kien_truc.cong_trinh.CongTrinhDetailActivity;
import com.nguyenvinhtien.music.adapter.post.PostListSuggestAdapter;
import com.nguyenvinhtien.music.models.CongTrinh;
import com.nguyenvinhtien.music.models.Post;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

/**
 * Created by Kim Van Thang on 5/29/2017.
 */

public class CongTrinhListSuggestAdapter extends RecyclerView.Adapter<CongTrinhListSuggestAdapter.ViewHolderNews> {


    Activity mContext;
    ArrayList<CongTrinh> mData;

    LayoutInflater mLayoutInflater;

    public CongTrinhListSuggestAdapter(Activity mContext, ArrayList<CongTrinh> mData) {
        this.mContext = mContext;
        this.mData = mData;

        mLayoutInflater = LayoutInflater.from(mContext);


    }

    @Override
    public CongTrinhListSuggestAdapter.ViewHolderNews onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_post, null);
        CongTrinhListSuggestAdapter.ViewHolderNews holder = new CongTrinhListSuggestAdapter.ViewHolderNews(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final CongTrinhListSuggestAdapter.ViewHolderNews holder, final int position) {

        CongTrinh item = mData.get(position);
        holder.setId(position);
        String imagePath = item.getImagePath();
        if (imagePath != null && !imagePath.isEmpty()) {
            Glide.with(mContext).load(imagePath).placeholder(R.drawable.picture).into(holder.imgImage);
        } else {
            holder.imgImage.setImageResource(R.drawable.default_cover_big);
        }
        holder.tvTitle.setText(item.getTitle());
        holder.tvContent.setText(item.getContent());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class ViewHolderNews extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        TextView tvContent;
        ImageView imgImage;

        int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public ViewHolderNews(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_post);
            tvContent = (TextView) itemView.findViewById(R.id.tv_content_post);
            imgImage = (ImageView) itemView.findViewById(R.id.img_post);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, CongTrinhDetailActivity.class);
            intent.putExtra(Constants.POST_ID, mData.get(id).getId());
            intent.putExtra(Constants.POST_TITLE, mData.get(id).getTitle());
            mContext.startActivity(intent);
            mContext.overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
        }


    }
}
