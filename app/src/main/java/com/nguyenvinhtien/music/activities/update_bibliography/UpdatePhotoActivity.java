package com.nguyenvinhtien.music.activities.update_bibliography;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.Common;

public class UpdatePhotoActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddPhoto;
    Button btnDeletePhoto;
    Button btnUpdatePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_photo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_photo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnAddPhoto = (Button) findViewById(R.id.btn_AddPhoto);
        btnUpdatePhoto = (Button) findViewById(R.id.btn_UpdatePhoto);
        btnDeletePhoto = (Button)findViewById(R.id.btn_DeletePhoto);
    }

    public void initEvents(){
        btnAddPhoto.setOnClickListener(this);
        btnUpdatePhoto.setOnClickListener(this);
        btnDeletePhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddPhoto:
                Intent intent_add_photo = new Intent(getApplicationContext(), AddPhotoActivity.class);
                startActivity(intent_add_photo);
                break;
            case R.id.btn_UpdatePhoto:
                Intent intent_update_photo = new Intent(getApplicationContext(), UpdateDataPhotoActivity.class);
                startActivity(intent_update_photo);
                break;
            case R.id.btn_DeletePhoto:
                Intent intent_delete_photo = new Intent(getApplicationContext(), DeletePhotoActivity.class);
                startActivity(intent_delete_photo);
                break;

        }
    }
}
