package com.nguyenvinhtien.music.activities.update_grant_permission;

import android.app.ProgressDialog;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

import java.util.ArrayList;

public class AddUserPermissionActivity extends AppCompatActivity implements View.OnClickListener {


    EditText edtUserEmail;
    RadioGroup rgAddUser;
    RadioButton rbAdmin, rbDatabase;
    Button btnUpdate;
    TextView tvPermissionSelected;
    DatabaseReference mDatabase;

    public ArrayList<String> listAdminUser = new ArrayList<>();
    public ArrayList<String> listSpecialUser = new ArrayList<>();

    ProgressDialog mProgress;

    String permission_type;

    String user_email_val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_user_permission);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_user);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        listAdminUser = getIntent().getStringArrayListExtra("LIST_ADMIN_USER");
        listSpecialUser = getIntent().getStringArrayListExtra("LIST_SPECIAL_USER");

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        rgAddUser = (RadioGroup) findViewById(R.id.rg_AddUser);
        edtUserEmail = (EditText) findViewById(R.id.edt_user_email);
        rbAdmin = (RadioButton) findViewById(R.id.rb_admin);
        rbDatabase = (RadioButton) findViewById(R.id.rb_database);
        btnUpdate = (Button) findViewById(R.id.btn_update);
        tvPermissionSelected = (TextView) findViewById(R.id.tv_permission_selected);

        rgAddUser.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (rbAdmin.isChecked()) {
                    permission_type = "Admins";
                    tvPermissionSelected.setText("Đã chọn Quyền quản trị");
                }

                if (rbDatabase.isChecked()) {
                    permission_type = "Special-Users";
                    tvPermissionSelected.setText("Đã chọn Quyền cập nhật dữ liệu");
                }
            }
        });



    }

    public void initEvents() {
        btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    user_email_val = edtUserEmail.getText().toString().trim();
                    if (!listAdminUser.contains(user_email_val) && !listSpecialUser.contains(user_email_val)){
                        startUpdate();
                    } else if (listAdminUser.contains(user_email_val)){
                        Toast.makeText(this, "Email đã được cấp quyền quản trị!", Toast.LENGTH_SHORT).show();
                    } else if (listSpecialUser.contains(user_email_val)){
                        Toast.makeText(this, "Email đã được cấp quyền cập nhật dữ liệu!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }


        }
    }


    private void startUpdate() {
        mProgress.setMessage("Đang cấp quyền cho người dùng...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        if (!TextUtils.isEmpty(user_email_val)
                && !tvPermissionSelected.getText().equals("")) {

            mProgress.show();

            final DatabaseReference newUser = mDatabase.child("Permission-Users").child(permission_type).push();
            newUser.child("email").setValue(user_email_val).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getApplicationContext(), "Cấp quyền thành công!", Toast.LENGTH_SHORT).show();
                    mProgress.dismiss();
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Cấp quyền thất bại! Hãy thử lại!", Toast.LENGTH_SHORT).show();
                }
            });



        } else {
            Toast.makeText(getApplicationContext(), "Cần điền đủ các trường và chọn quyền", Toast.LENGTH_SHORT).show();
        }
    }
}
