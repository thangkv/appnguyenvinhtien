package com.nguyenvinhtien.music.adapter.newspaper;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.newspaper.NewspapeDetailActivity;
import com.nguyenvinhtien.music.models.Newspaper;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.NewspaperRecyclerViewHolder;

import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/22/2017.
 */

public class NewspaperListFirebaseAdapter extends FirebaseRecyclerAdapter<Newspaper, NewspaperRecyclerViewHolder> {



    private Activity mContext;


    ArrayList<Newspaper> mLstNews = new ArrayList<>();


    private DatabaseReference mDatabase;


    public NewspaperListFirebaseAdapter(Activity mContext, Class<Newspaper> modelClass, int modelLayout, Class<NewspaperRecyclerViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;

        getListNewspaper();

    }


    @Override
    protected void populateViewHolder(final NewspaperRecyclerViewHolder viewHolder, final Newspaper model, final int position) {
        viewHolder.tvTitle.setText(model.getTitle());
        viewHolder.tvSource.setText(model.getSource());
        viewHolder.tvOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolder.tvOption);
                popupMenu.inflate(R.menu.menu_news);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_share:
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, model.getLink());
                                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
        try {
            Glide.with(mContext).load(model.getImagePath()).placeholder(R.drawable.picture).into(viewHolder.imgImage);
        } catch (Exception e) {
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NewspapeDetailActivity.class);
                intent.putExtra(Constants.NEWS_LINK, model.getLink());
                intent.putExtra(Constants.NEWS_ID, getRef(position).getKey());
                intent.putExtra(Constants.NEWS_TITLE, model.getTitle());

                mContext.startActivity(intent);
                mContext.overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
            }
        });


    }

    private ArrayList<Newspaper> getListNewspaper() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("News").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                String title = (String) dataSnapshot.child("title").getValue();
                String source = (String) dataSnapshot.child("source").getValue();
                String link = (String) dataSnapshot.child("link").getValue();
                String imagePath = (String) dataSnapshot.child("imagePath").getValue();

                Newspaper newspaper = new Newspaper(id, title, source, link, imagePath);
                mLstNews.add(newspaper);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return mLstNews;
    }
}
