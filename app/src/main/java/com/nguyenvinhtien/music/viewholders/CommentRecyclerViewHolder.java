package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;

/**
 * Created by KimVanThang on 4/7/2017.
 */

public class CommentRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView tvAuthorComment;
    public TextView tvContentComment;
    public ImageView imageViewAva;

    public CommentRecyclerViewHolder(View itemView) {
        super(itemView);
        tvAuthorComment = (TextView) itemView.findViewById(R.id.tv_author);
        tvContentComment = (TextView) itemView.findViewById(R.id.tv_content);
        imageViewAva = (ImageView) itemView.findViewById(R.id.img_ava);
        tvContentComment.setMovementMethod(new ScrollingMovementMethod());
    }
}
