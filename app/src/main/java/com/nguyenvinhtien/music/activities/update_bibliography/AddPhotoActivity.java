package com.nguyenvinhtien.music.activities.update_bibliography;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class AddPhotoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int IMAGE_GALLERY_REQUEST = 2;

    Button btnUpdatePhoto;
    ImageButton ibSelectPhoto;
    EditText edtPhotoTitle;
    Uri imageUri = null;
    ProgressDialog mProgress;

    StorageReference mStorage;
    DatabaseReference mDatabase;

    Uri imageURL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_photo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_photo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        mProgress = new ProgressDialog(this);

        btnUpdatePhoto = (Button)findViewById(R.id.btn_update);
        ibSelectPhoto = (ImageButton) findViewById(R.id.image_select);
        edtPhotoTitle = (EditText)findViewById(R.id.edt_photo_title);

    }

    public void initEvents(){

        btnUpdatePhoto.setOnClickListener(this);
        ibSelectPhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_select:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            ibSelectPhoto.setImageURI(imageUri);
        }
    }

    private void startUpdate() {
        mProgress.setMessage("Đang thêm Hình ảnh...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);


        final String photo_title_val = edtPhotoTitle.getText().toString().trim();


        if (!TextUtils.isEmpty(photo_title_val)
                && imageUri != null){

            mProgress.show();

            final DatabaseReference newPhoto = mDatabase.child("Photos").push();
            String photo_key = newPhoto.getKey();

            StorageReference imagePath = mStorage.child("Photos").child(photo_key);

            imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imageURL = taskSnapshot.getDownloadUrl();

                    newPhoto.child("title").setValue(photo_title_val);
                    newPhoto.child("path").setValue(imageURL.toString());

                    //AppController.getInstance().sendNotificationAddPhoto(photo_title_val);
                    Toast.makeText(getApplicationContext(), "Thêm hình ảnh thành công!", Toast.LENGTH_SHORT).show();
                    mProgress.dismiss();
                    finish();


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Cần điền đủ các trường và chọn ảnh", Toast.LENGTH_SHORT).show();
        }
    }
}
