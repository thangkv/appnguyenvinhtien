package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;


import com.nguyenvinhtien.music.R;

/**
 * Created by KimVanThang on 4/27/2017.
 */

public class PhotoRecyclerViewHolder extends RecyclerView.ViewHolder {
    public ImageView imgImage;


    public PhotoRecyclerViewHolder(View itemView) {
        super(itemView);
        imgImage = (ImageView) itemView.findViewById(R.id.photo_img_item);
    }
}
