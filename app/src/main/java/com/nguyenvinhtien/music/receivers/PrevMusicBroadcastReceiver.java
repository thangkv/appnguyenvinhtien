package com.nguyenvinhtien.music.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;


public class PrevMusicBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PlayMusicActivity musicActivity = (PlayMusicActivity) AppController.getInstance().getPlayMusicActivity();
        PlayMusicService musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        String path = musicService.getCurrentSong().getPath();

        if (path.contains("firebasestorage")){
            if (AppController.getInstance().checkInternetState() == false){
                Toast.makeText(context, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            } else {
                if (musicActivity != null) {
                    musicActivity.backMusic();

                } else {
                    musicService.backMusic();
                }
                musicService.showNotification(true);

                Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                context.sendBroadcast(intent1);
            }
        } else {
            if (musicActivity != null) {
                musicActivity.backMusic();

            } else {
                musicService.backMusic();
            }
            musicService.showNotification(true);

            Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
            context.sendBroadcast(intent1);
        }

    }
}
