package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;

/**
 * Created by KimVanThang on 4/19/2017.
 */

public class VideoRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView tvTitle;
    public TextView tvArtist;
    public TextView tvOption;
    public ImageView imgImage;


    public VideoRecyclerViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_video_title_item);
        tvArtist = (TextView) itemView.findViewById(R.id.tv_video_artist_item);
        tvOption = (TextView) itemView.findViewById(R.id.tv_option);
        imgImage = (ImageView) itemView.findViewById(R.id.iv_video_img_item);


    }
}
