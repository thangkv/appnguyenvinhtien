package com.nguyenvinhtien.music.activities.update_bibliography;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.ImageView;

import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.models.Photo;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.GridSpacingItemDecoration;

public class SelectPhotoToUpdateActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    RecyclerView listPhoto;
    FirebaseRecyclerAdapter<Photo, SelectPhotoToUpdateActivity.SelectPhotoRecyclerViewHolder> adapter;
    String photoKey = null;
    String photoTitle = null;
    String photoPath = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_photo_to_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_photo_to_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Photos");
        adapter = new FirebaseRecyclerAdapter<Photo, SelectPhotoToUpdateActivity.SelectPhotoRecyclerViewHolder>(Photo.class, R.layout.item_select_photo, SelectPhotoToUpdateActivity.SelectPhotoRecyclerViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(final SelectPhotoToUpdateActivity.SelectPhotoRecyclerViewHolder viewHolder, final Photo model, final int position) {
                Glide.with(getApplicationContext()).load(model.getPath()).centerCrop().placeholder(R.drawable.picture).into(viewHolder.imgPhoto);

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        photoKey = getRef(position).getKey();
                        photoTitle = model.getTitle();
                        photoPath = model.getPath();

                        viewHolder.itemView.setBackgroundColor(Color.rgb(66,110,180));

                        Intent i = new Intent(SelectPhotoToUpdateActivity.this, UpdateDataPhotoActivity.class);
                        i.putExtra("KEY_PHOTO", photoKey);
                        i.putExtra("TITLE_PHOTO", photoTitle);
                        i.putExtra("PATH_PHOTO", photoPath);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });
            }
        };

        initControls();
        initEvents();
        adapter.notifyDataSetChanged();
        listPhoto.setAdapter(adapter);

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

    public static class SelectPhotoRecyclerViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgPhoto;

        public SelectPhotoRecyclerViewHolder(View itemView) {
            super(itemView);
            imgPhoto = (ImageView) itemView.findViewById(R.id.img_photo);
        }

    }

    private void initControls() {
        listPhoto = (RecyclerView) findViewById(R.id.rv_photo_list_select);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        listPhoto.setLayoutManager(layoutManager);

        int spanCount = 3; // 3 columns
        int spacing = 20; // 20px
        boolean includeEdge = true;
        listPhoto.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
    }

    private void initEvents() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
