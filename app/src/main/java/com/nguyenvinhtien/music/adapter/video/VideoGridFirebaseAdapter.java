package com.nguyenvinhtien.music.adapter.video;

import android.app.Activity;

import android.content.Intent;

import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;


import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.other.PlayVideoActivity;
import com.nguyenvinhtien.music.models.Video;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.VideoRecyclerViewHolder;

import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/19/2017.
 */

public class VideoGridFirebaseAdapter  extends FirebaseRecyclerAdapter<Video, VideoRecyclerViewHolder> {


    private Activity mContext;
    private DatabaseReference mDatabase;
    ArrayList<Video> mLstVideo = new ArrayList<>();

    public VideoGridFirebaseAdapter(Activity mContext, Class<Video> modelClass, int modelLayout, Class<VideoRecyclerViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;

        getListVideo();
    }

    @Override
    protected void populateViewHolder(final VideoRecyclerViewHolder viewHolder, final Video model, final int position) {
        viewHolder.tvTitle.setText(model.getTitle());
        viewHolder.tvArtist.setText(model.getArtist());
        try {
            Glide.with(mContext).load(model.getImagePath()).placeholder(R.drawable.video_youtube).centerCrop().into(viewHolder.imgImage);
        } catch (Exception e) {
        }

        viewHolder.tvOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolder.tvOption);
                popupMenu.inflate(R.menu.menu_video);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_share:
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, model.getPath());
                                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PlayVideoActivity.class);
                intent.putExtra(PlayVideoActivity.VIDEO_LINK, model.getPath());
                intent.putExtra(Constants.VIDEO_ID, getRef(position).getKey());
                intent.putExtra(Constants.VIDEO_TITLE, model.getTitle());
                intent.putExtra(Constants.PRIVACY_DESC, model.getDescription());
                mContext.startActivity(intent);
            }
        });

    }

    private ArrayList<Video> getListVideo() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Videos").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                String title = (String) dataSnapshot.child("title").getValue();
                String artist = (String) dataSnapshot.child("artist").getValue();
                String imagePath = (String) dataSnapshot.child("imagePath").getValue();
                String path = (String) dataSnapshot.child("path").getValue();

                Video video = new Video(id, title, artist, imagePath, path);
                mLstVideo.add(video);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return mLstVideo;
    }

}
