package com.nguyenvinhtien.music.activities.other;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.adapter.song.SongListPlayingAdapter;
import com.nguyenvinhtien.music.adapter.other.ViewPagerPlayAdapter;
import com.nguyenvinhtien.music.fragments.FragmentLyric;
import com.nguyenvinhtien.music.fragments.FragmentPlay;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

public class PlayMusicActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String IS_PLAYING = "is_playing";

    private SeekBar seekBar;
    PlayMusicService mPlayMusicService;
    String path;
    ImageView btnPlayPause;
    ImageView btnNext;
    ImageView btnPrev;
    ImageView btnShuffle;
    ImageView btnRepeat;
    TextView tvTimePlayed;
    TextView tvTotalTime;
    RelativeLayout layout;
    int totalTime;
    ArrayList<Song> mData;
    int currentPos;
    boolean isShuffle = false;
    boolean isPlaying = true;
    TextView mTvSongName;
    TextView mTvArtist;
    ViewPager mViewPager;
    ViewPagerPlayAdapter mVpPlayAdapter;
    boolean isSeeking;

    LinearLayout llSeekBar;
    LinearLayout llControl;

    String albumPath;

    String songID;

    ImageView imgBackgroundPlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_play_music);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_playing);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setStatusBarTranslucent(true);

        AppController.getInstance().setPlayMusicActivity(this);

        mPlayMusicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        songID = getIntent().getStringExtra(Constants.SONG_POS);

        getDataFromIntent();

        initControls();
        initEvents();


        if (path.contains("firebasestorage")) {
            if (AppController.getInstance().checkInternetState() == false) {
                Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                updateSeekBar();
                totalTime = mPlayMusicService.getTotalTime();
                mPlayMusicService.showNotification(!mPlayMusicService.isShowNotification());
                setName();
                updateRepeatButton();
                updateShuffleButton();
                updatePlayPauseButton();

            } else {
                if (mPlayMusicService == null) {
                    initPlayService();
                } else {
                    updateSeekBar();
                    totalTime = mPlayMusicService.getTotalTime();
                    mPlayMusicService.showNotification(!mPlayMusicService.isShowNotification());
                    setName();
                    if (!isPlaying) {
                        playMusic();
                    }
                    updateRepeatButton();
                    updateShuffleButton();
                    updatePlayPauseButton();

                }
                setAlbumArt();
                setSongLyric();
                updateHomeActivity();
            }
        } else {
            if (mPlayMusicService == null) {
                initPlayService();
            } else {
                updateSeekBar();
                totalTime = mPlayMusicService.getTotalTime();
                mPlayMusicService.showNotification(!mPlayMusicService.isShowNotification());
                setName();
                if (!isPlaying) {
                    playMusic();
                }
                updateRepeatButton();
                updateShuffleButton();
                updatePlayPauseButton();

            }

            setAlbumArt();
            setSongLyric();
            updateHomeActivity();

        }
        registerBroadcastSongComplete();
        registerBroadcastSwitchSong();
        registerBroadcastReceiverUpdatePlayPauseButton();


    }

    //update giao dien thanh dieu khien nhac moi activity
    private void updateHomeActivity() {
        Intent intent = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
        sendBroadcast(intent);
    }


    private void setAlbumArt() {
        path = mData.get(currentPos).getPath();
        albumPath = mData.get(currentPos).getAlbumImagePath();
        if (albumPath != null) {
            if (albumPath.contains("firebasestorage")) {
                if (albumPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                    imgBackgroundPlay.setImageResource(R.drawable.default_cover_big);
                } else {
                    Glide.with(this).load(albumPath).centerCrop().into(imgBackgroundPlay);
                }
            } else {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(path);

                byte[] image = mmr.getEmbeddedPicture();
                if (image != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                    imgBackgroundPlay.setImageBitmap(bitmap);
                } else {
                    imgBackgroundPlay.setImageResource(R.drawable.default_cover_big);
                }
            }
        } else {
            imgBackgroundPlay.setImageResource(R.drawable.default_cover_big);
        }

        Intent intent1 = new Intent(Constants.ACTION_CHANGE_ALBUM_ART);
        intent1.putExtra(FragmentPlay.KEY_ALBUM_PLAY, mData.get(currentPos).getAlbumImagePath());
        intent1.putExtra(FragmentPlay.KEY_SONG_PLAY, mData.get(currentPos).getPath());

        intent1.putExtra(FragmentPlay.TYPE, mData.get(currentPos).getType());//KVT

        sendBroadcast(intent1);


    }

    private void setSongLyric() {
        Intent intent1 = new Intent(Constants.ACTION_CHANGE_SONG_LYRIC);
        intent1.putExtra(FragmentLyric.KEY_SONG_LYRIC, mData.get(currentPos).getLyric());
        sendBroadcast(intent1);
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    BroadcastReceiver broadcastReceiverSongCompleted = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (path.contains("firebasestorage")) {
                if (AppController.getInstance().checkInternetState() == false) {
                    Toast.makeText(PlayMusicActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                    if (mPlayMusicService != null) {
                        changePlayButtonState();
                        pauseMusic();
                        mPlayMusicService.changePlayPauseState();
                        Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                        context.sendBroadcast(intent1);
                    }
                } else {
                    nextMusic();
                    //totalTime = mPlayMusicService.getTotalTime();
                    //updateSeekBar();
                    mPlayMusicService.showNotification(true);
                    Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                    context.sendBroadcast(intent1);

                }
            } else {
                nextMusic();
                //totalTime = mPlayMusicService.getTotalTime();
                //updateSeekBar();
                mPlayMusicService.showNotification(true);
                Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                context.sendBroadcast(intent1);
            }

        }
    };

    private void registerBroadcastSongComplete() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_COMPLETE_SONG);
        registerReceiver(broadcastReceiverSongCompleted, intentFilter);
    }

    private void unRegisterBroadcastSongComplete() {
        unregisterReceiver(broadcastReceiverSongCompleted);
    }


    //Broadcast lắng nghe chọn bài
    BroadcastReceiver broadcastReceiverSwitchSong = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            currentPos = intent.getExtras().getInt(SongListPlayingAdapter.KEY_ID_SWITCH);
            path = mData.get(currentPos).getPath();
            mPlayMusicService.setDataForNotification(mData,
                    currentPos, mData.get(currentPos), mData.get(currentPos).getAlbumImagePath());
            playMusic();
            //setAlbumArt();
            mPlayMusicService.showNotification(true);
        }
    };

    private void registerBroadcastSwitchSong() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_SWITCH_SONG);
        registerReceiver(broadcastReceiverSwitchSong, intentFilter);
    }

    private void unRegisterBroadcastSwitchSong() {
        unregisterReceiver(broadcastReceiverSwitchSong);
    }


    BroadcastReceiver broadcastReceiverUpdatePlayPauseButton = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mPlayMusicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
            updatePlayPauseButton();



        }
    };

    private void registerBroadcastReceiverUpdatePlayPauseButton() {
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_UPDATE_PLAY_PAUSE_BUTTON);
        registerReceiver(broadcastReceiverUpdatePlayPauseButton, intentFilter);
    }

    private void unRegisterBroadcastReceiverUpdatePlayPauseButton() {
        unregisterReceiver(broadcastReceiverUpdatePlayPauseButton);
    }



    private void initEvents() {
        btnPlayPause.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnShuffle.setOnClickListener(this);
        btnRepeat.setOnClickListener(this);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvTimePlayed.setText(Common.miliSecondToString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isSeeking = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                mPlayMusicService.seekTo(seekBar.getProgress());
                if (!mPlayMusicService.isPlaying()) {
                    mPlayMusicService.resumeMusic();
                    btnPlayPause.setImageResource(R.drawable.pb_pause);
                }
                isSeeking = false;
                updateSeekBar();
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initControls() {
        llControl = (LinearLayout) findViewById(R.id.ll_control_music);
        llSeekBar = (LinearLayout) findViewById(R.id.ll_seek_bar);

        mTvSongName = (TextView) findViewById(R.id.tv_song_name_play);
        mTvArtist = (TextView) findViewById(R.id.tv_artist_play);

        layout = (RelativeLayout) findViewById(R.id.activity_play_music);
        seekBar = (SeekBar) findViewById(R.id.seek_bar_play);
        btnPlayPause = (ImageView) findViewById(R.id.btn_play_pause);
        btnPrev = (ImageView) findViewById(R.id.btn_prev);
        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnShuffle = (ImageView) findViewById(R.id.btn_shuffle);
        btnRepeat = (ImageView) findViewById(R.id.btn_repeat);
        tvTotalTime = (TextView) findViewById(R.id.tv_time_left);
        tvTimePlayed = (TextView) findViewById(R.id.tv_time_played);

        mViewPager = (ViewPager) findViewById(R.id.view_pager_play);
        mVpPlayAdapter = new ViewPagerPlayAdapter(getSupportFragmentManager(), mData,
                mData.get(currentPos).getAlbumImagePath(), mData.get(currentPos).getPath()
                , mData.get(currentPos).getLyric(), mData.get(currentPos).getType())
                ;
        mViewPager.setAdapter(mVpPlayAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setCurrentItem(1);
        isSeeking = false;

        imgBackgroundPlay = (ImageView) findViewById(R.id.img_bg_play);
        imgBackgroundPlay.setAlpha(100);
    }

    private void playMusic() {
        mPlayMusicService.playMusic(path);
        totalTime = mPlayMusicService.getTotalTime();
        updateSeekBar(); // KVTT
        Song item = mData.get(currentPos);
        mPlayMusicService.setDataForNotification(mData, currentPos, item, item.getAlbumImagePath());
        Intent intent1 = new Intent(this, PlayMusicService.class);
        startService(intent1);

        setName();
        setAlbumArt();
        setSongLyric();
        mPlayMusicService.showNotification(true);

        updateHomeActivity();
        updatePlayPauseButton();
    }


    private void getDataFromIntent() {
        Intent intent = getIntent();
        isPlaying = intent.getExtras().getBoolean(IS_PLAYING);

        //IS_PLAYING co gia tri true khi an vao thanh dieu khien nhac, false khi an vao bai hat trong list
        if (isPlaying) {
            path = mPlayMusicService.getCurrentSong().getPath();
            currentPos = mPlayMusicService.getCurrentSongPos();
            mData = mPlayMusicService.getLstSongPlaying();
            isShuffle = mPlayMusicService.isShuffle();


        } else {
            path = intent.getExtras().getString(Constants.SONG_PATH);
            currentPos = intent.getExtras().getInt(Constants.SONG_POS);
            mData = (ArrayList<Song>) intent.getExtras().getSerializable(Constants.LIST_SONG);
        }
    }

    private void initPlayService() {
        Intent intent = new Intent(this, PlayMusicService.class);
        bindService(intent, serviceConnection, BIND_AUTO_CREATE); //lỗi leaked ServiceConnection
    }

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlayMusicService.LocalBinder binder = (PlayMusicService.LocalBinder) service;
            mPlayMusicService = binder.getInstantBoundService();
            AppController.getInstance().setPlayMusicService(mPlayMusicService);
            mPlayMusicService.setRepeat(false);
            playMusic();
            updateSeekBar();
            totalTime = mPlayMusicService.getTotalTime();
    }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("CHECK", "DISCONECTED");
        }
    };

    //Cap nhat seekBar
    private void updateSeekBar() {

        seekBar.setMax(totalTime);
        int currentLength = mPlayMusicService.getCurrentLength();

        if (!isSeeking) {
            seekBar.setProgress(currentLength);
            tvTimePlayed.setText(Common.miliSecondToString(currentLength));
        }
        tvTotalTime.setText(Common.miliSecondToString(totalTime));

        Handler musicHandler = new Handler();
        musicHandler.post(new Runnable() {
            @Override
            public void run() {
                updateSeekBar();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (path.contains("firebasestorage")) {
                    if (AppController.getInstance().checkInternetState() == false) {
                        Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                    }  else {
                        nextMusic();
                        mPlayMusicService.showNotification(true);
                    }
                } else {
                    nextMusic();
                    mPlayMusicService.showNotification(true);
                }
                break;
            case R.id.btn_play_pause:
                playPauseMusic();
                mPlayMusicService.showNotification(true);

                break;
            case R.id.btn_prev:
                if (path.contains("firebasestorage")) {
                    if (AppController.getInstance().checkInternetState() == false) {
                        Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                    }  else {
                        backMusic();
                        mPlayMusicService.showNotification(true);
                    }
                } else {
                    backMusic();
                    mPlayMusicService.showNotification(true);
                }
                break;
            case R.id.btn_shuffle:
                if (mPlayMusicService == null) return;
                if (mPlayMusicService.isShuffle()) {
                    btnShuffle.setImageResource(R.drawable.ic_widget_shuffle_off);
                    mPlayMusicService.setShuffle(false);
                    Log.d("shuffle", "true");
                } else {
                    btnShuffle.setImageResource(R.drawable.ic_widget_shuffle_on);
                    mPlayMusicService.setShuffle(true);
                    Log.d("shuffle", "false");
                }
                break;
            case R.id.btn_repeat:
                if (mPlayMusicService.isRepeat()) {
                    btnRepeat.setImageResource(R.drawable.ic_widget_repeat_off);
                    mPlayMusicService.setRepeat(false);
                } else {
                    btnRepeat.setImageResource(R.drawable.ic_widget_repeat_one);
                    mPlayMusicService.setRepeat(true);
                }
                break;
        }
    }

    //thay đổi trạng thái pause và play
    public void playPauseMusic() {
        if (mPlayMusicService.isPlaying()) {
            btnPlayPause.setImageResource(R.drawable.pb_play);
            mPlayMusicService.pauseMusic();
        } else {
            btnPlayPause.setImageResource(R.drawable.pb_pause);
            mPlayMusicService.resumeMusic();
        }

        updateHomeActivity();

        updatePlayPauseButton();
    }

//    public void resumeMusic() {
//        if (!mPlayMusicService.isPlaying()) {
//            btnPlayPause.setImageResource(R.drawable.pb_pause);
//            mPlayMusicService.resumeMusic();
//        }
//    }

    public void pauseMusic() {
        if (mPlayMusicService.isPlaying()) {
            btnPlayPause.setImageResource(R.drawable.pb_play);
            mPlayMusicService.pauseMusic();

        }
    }

    public void nextMusic() {
        if (!mPlayMusicService.isRepeat()) {
            currentPos = mPlayMusicService.getNextPosition();
            path = mData.get(currentPos).getPath();
        }

        setSongLyric();
        playMusic();
    }

    public void backMusic() {
        if (!mPlayMusicService.isRepeat()) {
            currentPos = mPlayMusicService.getPrePosition();
            path = mData.get(currentPos).getPath();
        }
        setAlbumArt();
        setSongLyric();
        playMusic();
    }

    public void updatePlayPauseButton() {
        if (mPlayMusicService != null) {
            if (mPlayMusicService.isPlaying()) {
                btnPlayPause.setImageResource(R.drawable.pb_pause);
            } else {
                btnPlayPause.setImageResource(R.drawable.pb_play);
            }
        }
    }

    public void updateShuffleButton() {
        if (mPlayMusicService.isShuffle()) {
            btnShuffle.setImageResource(R.drawable.ic_widget_shuffle_on);
        } else {
            btnShuffle.setImageResource(R.drawable.ic_widget_shuffle_off);
        }
    }

    public void updateRepeatButton() {
        if (mPlayMusicService.isRepeat()) {
            btnRepeat.setImageResource(R.drawable.ic_widget_repeat_one);
        } else {
            btnRepeat.setImageResource(R.drawable.ic_widget_repeat_off);
        }
    }


    private void setName() {
        mTvSongName.setText(mData.get(currentPos).getTitle());
        mTvArtist.setText(mData.get(currentPos).getArtist());

    }


    public void changePlayButtonState() {
        btnPlayPause.setImageResource(R.drawable.pb_play);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_change, R.anim.slide_out_down);
        AppController.getInstance().setPlayMusicActivity(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterBroadcastSongComplete();
        unRegisterBroadcastSwitchSong();
        unRegisterBroadcastReceiverUpdatePlayPauseButton();

    }


}
