package com.nguyenvinhtien.music.activities.update_cong_trinh;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.adapter.cong_trinh.CongTrinhPhotoUpdateAdapter;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

import java.net.URL;
import java.util.ArrayList;

public class AddCongTrinhActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int IMAGE_GALLERY_REQUEST = 2;

    Button btnUpdatePost, btnSelectPhoto;
    RecyclerView mRvCongTrinhPhotoList;

    CongTrinhPhotoUpdateAdapter adapter;

    EditText edtPostTitle, edtPostContent;
    ArrayList<Uri> imageUri;
    ProgressDialog mProgress;

    StorageReference mStorage;
    DatabaseReference mDatabase;

    Uri imageURL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_cong_trinh);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_post);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();

        imageUri = new ArrayList<>();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);

        btnUpdatePost = (Button) findViewById(R.id.btn_update);
        mRvCongTrinhPhotoList = (RecyclerView) findViewById(R.id.rv_congtrinh_photo);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvCongTrinhPhotoList.setLayoutManager(layoutManager);

        edtPostTitle = (EditText) findViewById(R.id.edt_title_post);
        edtPostContent = (EditText) findViewById(R.id.edt_content_post);

        btnSelectPhoto = (Button) findViewById(R.id.btn_select_photo);
    }

    public void initEvents() {
        btnUpdatePost.setOnClickListener(this);
        btnSelectPhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_photo:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false) {
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK) {

            if (imageUri != null) {
                imageUri.clear();
            }

            if (data.getData() != null) {
                Uri uri = data.getData();
                imageUri.add(uri);
            } else {
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        imageUri.add(uri);
                    }
                }
            }

        }

        adapter = new CongTrinhPhotoUpdateAdapter(this, imageUri);
        adapter.notifyDataSetChanged();
        mRvCongTrinhPhotoList.setAdapter(adapter);

    }


    private void startUpdate() {
        mProgress.setMessage("Đang thêm Công trình...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);


        final String post_title_val = edtPostTitle.getText().toString().trim();
        final String post_content_val = edtPostContent.getText().toString().trim();

        if (!TextUtils.isEmpty(post_title_val)
                && !TextUtils.isEmpty(post_content_val)
                && imageUri.size() != 0) {

            mProgress.show();

            final DatabaseReference newPost = mDatabase.child("Congtrinhs").push();
            final String key_post = newPost.getKey();

            for (int i = 0; i < imageUri.size(); i++) {
                final DatabaseReference newPhoto = mDatabase.child("Congtrinhs-Anhs").child(key_post).push();
                final String key_photo = newPhoto.getKey();
                StorageReference imagePath = mStorage.child("CongTrinhAnhs").child(key_post)
                        .child(key_photo);
                final int finalI = i;
                imagePath.putFile(imageUri.get(i)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageURL = taskSnapshot.getDownloadUrl();

                        newPhoto.child("path").setValue(imageURL.toString());
                        newPost.child("imagePath").setValue(imageURL.toString());// can luu y


                        if (finalI == imageUri.size() - 1) {
                            newPost.child("title").setValue(post_title_val);
                            newPost.child("content").setValue(post_content_val);
                            mProgress.dismiss();
                            finish();
                            Toast.makeText(getApplicationContext(), "Thêm công trình thành công!", Toast.LENGTH_SHORT).show();
                            //AppController.getInstance().sendNotificationAddCongTrinh(post_title_val);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                    }
                });

            }

        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn ảnh và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
