package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;

/**
 * Created by KimVanThang on 4/25/2017.
 */

public class PostRecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView tvTitle;
    public TextView tvContent;
    public ImageView imgImage;



    public PostRecyclerViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_title_post);
        tvContent = (TextView) itemView.findViewById(R.id.tv_content_post);
        imgImage = (ImageView) itemView.findViewById(R.id.img_post);


    }

}
