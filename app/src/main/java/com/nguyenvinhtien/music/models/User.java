package com.nguyenvinhtien.music.models;

/**
 * Created by KimVanThang on 4/7/2017.
 */

public class User {

    public String email;

    public User(){

    }

    public User(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
