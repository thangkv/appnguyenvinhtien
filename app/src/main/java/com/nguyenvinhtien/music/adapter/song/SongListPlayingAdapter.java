package com.nguyenvinhtien.music.adapter.song;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.fragments.FragmentPlay;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;


import java.util.ArrayList;


public class SongListPlayingAdapter extends RecyclerView.Adapter<SongListPlayingAdapter.ViewHolderSongPlaying> {

    public static final String KEY_ID_SWITCH = "key_id_switch";

    Context mContext;
    ArrayList<Song> mData;
    LayoutInflater mLayoutInflater;

    public SongListPlayingAdapter(Context mContext, ArrayList<Song> mData) {
        this.mContext = mContext;
        this.mData = mData;
        mLayoutInflater = LayoutInflater.from(mContext);

    }

    @Override
    public ViewHolderSongPlaying onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mLayoutInflater.inflate(R.layout.item_song_playing, null);
        ViewHolderSongPlaying holder = new ViewHolderSongPlaying(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolderSongPlaying holder, int position) {
        Song item = mData.get(position);
        holder.setId(position);
        String path = item.getAlbumImagePath();
        String songPath = item.getPath();

        if (path != null) {
            if (path.contains("firebasestorage")) {
                if (path.equals(Constants.DEFAULT_SONG_ART_URL)) {
                    holder.imgAlbum.setImageResource(R.drawable.default_cover_big);
                } else {
                    Glide.with(mContext).load(path).placeholder(R.drawable.song).into(holder.imgAlbum);
                }
            } else {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(songPath);

                byte[] image = mmr.getEmbeddedPicture();
                if (image != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                    holder.imgAlbum.setImageBitmap(bitmap);
                } else {
                    holder.imgAlbum.setImageResource(R.drawable.default_cover_big);
                }
            }
        } else {
            holder.imgAlbum.setImageResource(R.drawable.default_cover_big);
        }

        holder.tvTitle.setText(item.getTitle());
        holder.tvArtist.setText(item.getArtist());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolderSongPlaying extends RecyclerView.ViewHolder implements View.OnClickListener {
        int id;
        private ImageView imgAlbum;
        private TextView tvTitle;
        private TextView tvArtist;


        public ViewHolderSongPlaying(View itemView) {
            super(itemView);
            imgAlbum = (ImageView) itemView.findViewById(R.id.img_album_song_play);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_song_name_play);
            tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_song_play);

            itemView.setOnClickListener(this);
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public void onClick(View v) {
            String currentPath = mData.get(id).getPath();
                if (currentPath.contains("firebasestorage")){
                    if (AppController.getInstance().checkInternetState() == false){
                        Toast.makeText(mContext, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(Constants.ACTION_SWITCH_SONG);
                        intent.putExtra(KEY_ID_SWITCH, id);
                        mContext.sendBroadcast(intent);
                        Intent intent1 = new Intent(Constants.ACTION_CHANGE_ALBUM_ART);
                        intent1.putExtra(FragmentPlay.KEY_ALBUM_PLAY, mData.get(id).getAlbumImagePath());

                        //KVT
                        intent1.putExtra(FragmentPlay.KEY_SONG_PLAY, mData.get(id).getPath());

                        mContext.sendBroadcast(intent1);
                    }
                } else {
                    Intent intent = new Intent(Constants.ACTION_SWITCH_SONG);
                    intent.putExtra(KEY_ID_SWITCH, id);
                    mContext.sendBroadcast(intent);
                    Intent intent1 = new Intent(Constants.ACTION_CHANGE_ALBUM_ART);
                    intent1.putExtra(FragmentPlay.KEY_ALBUM_PLAY, mData.get(id).getAlbumImagePath());
                    intent1.putExtra(FragmentPlay.KEY_SONG_PLAY, mData.get(id).getPath());
                    mContext.sendBroadcast(intent1);

                }

        }
    }
}
