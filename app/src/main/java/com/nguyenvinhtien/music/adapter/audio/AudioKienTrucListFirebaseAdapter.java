package com.nguyenvinhtien.music.adapter.audio;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.activities.other.PlayVideoActivity;
import com.nguyenvinhtien.music.fragments.FragmentPlay;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.SongRecyclerViewHolder;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Kim Van Thang on 5/28/2017.
 */

public class AudioKienTrucListFirebaseAdapter extends FirebaseRecyclerAdapter<Song, SongRecyclerViewHolder> {

    public static final String ALBUM_KEY_ONLINE = "album_key";

    private Activity mContext;


    ArrayList<Song> mLstSong = new ArrayList<>();

    DownloadManager downloadManager;
    long enqueue;

    private DatabaseReference mDatabase;

    public AudioKienTrucListFirebaseAdapter(Activity mContext, Class<Song> modelClass, int modelLayout, Class<SongRecyclerViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;

        getListAudio();

    }


    @Override
    protected void populateViewHolder(final SongRecyclerViewHolder viewHolder, final Song model, final int position) {
        viewHolder.tvTitle.setText(model.getTitle());
        viewHolder.tvArtist.setText(model.getArtist());
        viewHolder.tvOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, viewHolder.tvOption);
                popupMenu.inflate(R.menu.menu_song);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_download:
                                Toast.makeText(mContext, "Đang tải bài hát " + model.getTitle(), Toast.LENGTH_SHORT).show();

                                File direct = new File(Environment.getExternalStorageDirectory()
                                        + "/Nguyễn Vĩnh Tiến - Nhạc");

                                if (!direct.exists()) {
                                    direct.mkdirs();
                                }

                                downloadManager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
                                //Yeu cau download
                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(model.getPath()));

                                //cho phep down bang wifi hoac 3G
                                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                                        //dat ten cho file down ve thanh "ten_bai_hat.mp3"
                                        .setTitle(model.getTitle() + ".mp3")
                                        //dat ten cho phan mieu ta thanh "ten_bai_hat-ten_ca_si"
                                        .setDescription(model.getTitle() + " - Nguyễn Vĩnh Tiến")
                                        //thu muc luu bai hat Public de cho cac app khac co the truy cap
                                        .setDestinationInExternalPublicDir("/Nguyễn Vĩnh Tiến - Nhạc", model.getTitle() + ".mp3");

                                //Dua request vao hang doi
                                enqueue = downloadManager.enqueue(request);
                                break;

                            case R.id.item_share:
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, model.getPath());
                                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
        try {
            if (!model.getAlbumImagePath().equals(Constants.DEFAULT_SONG_ART_URL)) {
                Glide.with(mContext).load(model.getAlbumImagePath()).placeholder(R.drawable.song).into(viewHolder.imgImage);
            } else {
                viewHolder.imgImage.setImageResource(R.drawable.default_cover_big);
            }
        } catch (Exception e) {
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.getInstance().checkInternetState() == false) {
                    Toast.makeText(mContext, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(mContext, PlayMusicActivity.class);
                    intent.putExtra(Constants.SONG_PATH, model.getPath());
                    intent.putExtra(Constants.SONG_POS, position);
                    intent.putExtra(Constants.LIST_SONG, mLstSong);
                    intent.putExtra(PlayMusicActivity.IS_PLAYING, false);
                    intent.putExtra(FragmentPlay.TYPE, model.getType());
                    mContext.startActivity(intent);
                    mContext.overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
                }

            }
        });


    }


    private ArrayList<Song> getListAudio() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("AudioKienTrucs").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                String title = (String) dataSnapshot.child("title").getValue();
                String artist = (String) dataSnapshot.child("artist").getValue();
                String album = (String) dataSnapshot.child("album").getValue();
                String albumImagePath = (String) dataSnapshot.child("albumImagePath").getValue();
                String path = (String) dataSnapshot.child("path").getValue();
                String lyric = (String) dataSnapshot.child("lyric").getValue();
                String type = (String) dataSnapshot.child("type").getValue();

                Song audio = new Song(id, title, album, artist, albumImagePath, path, lyric, type);
                mLstSong.add(audio);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return mLstSong;
    }
}
