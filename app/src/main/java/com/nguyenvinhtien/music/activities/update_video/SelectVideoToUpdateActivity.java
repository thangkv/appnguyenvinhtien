package com.nguyenvinhtien.music.activities.update_video;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.models.Video;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class SelectVideoToUpdateActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    RecyclerView listVideo;
    FirebaseRecyclerAdapter<Video, SelectVideoToUpdateActivity.SelectVideoRecyclerViewHolder> adapter;
    String videoKey = null;
    String videoTitle = null;
    String videoArtist = null;
    String videoPath = null;
    String videoDesc = null;
    String videoImagePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_video_to_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_video_to_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Videos");
        adapter = new FirebaseRecyclerAdapter<Video, SelectVideoToUpdateActivity.SelectVideoRecyclerViewHolder>(Video.class, R.layout.item_select_video, SelectVideoToUpdateActivity.SelectVideoRecyclerViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(final SelectVideoToUpdateActivity.SelectVideoRecyclerViewHolder viewHolder, final Video model, final int position) {
                viewHolder.tvTitle.setText(model.getTitle());
                viewHolder.tvArtist.setText(model.getArtist());


                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        videoKey = getRef(position).getKey();
                        videoTitle = model.getTitle();
                        videoArtist = model.getArtist();
                        videoPath = model.getPath();
                        videoDesc = model.getDescription();
                        videoImagePath = model.getImagePath();
                        viewHolder.itemView.setBackgroundColor(Color.rgb(66,110,180));
                        Intent i = new Intent(SelectVideoToUpdateActivity.this, UpdateDataVideoActivity.class);
                        i.putExtra("KEY_VIDEO", videoKey);
                        i.putExtra("TITLE_VIDEO", videoTitle);
                        i.putExtra("ARTIST_VIDEO", videoArtist);
                        i.putExtra("PATH_VIDEO", videoPath);
                        i.putExtra("DESC_VIDEO", videoDesc);
                        i.putExtra("IMAGE_PATH_VIDEO", videoImagePath);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });


            }


        };

        initControls();
        initEvents();
        adapter.notifyDataSetChanged();
        listVideo.setAdapter(adapter);

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

    public static class SelectVideoRecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvArtist;


        public SelectVideoRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_video);
            tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_video);

        }

    }

    private void initControls() {
        listVideo = (RecyclerView) findViewById(R.id.rv_video_list_select);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listVideo.setLayoutManager(layoutManager);

    }

    private void initEvents() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
