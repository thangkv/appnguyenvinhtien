package com.nguyenvinhtien.music.models;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/27/2017.
 */

public class Photo implements Serializable {
    String id;
    String title;
    String path;

    public static DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    private static ArrayList<Photo> mLstPhoto;

    public Photo() {
    }

    public Photo(String id, String title, String path) {
        this.id = id;
        this.title = title;
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public static ArrayList<Photo> getListPhoto() {
        if (mLstPhoto == null) {
            mLstPhoto = new ArrayList<>();
            mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child("Photos").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String path = (String) dataSnapshot.child("path").getValue();

                    Photo photo = new Photo(id, title, path);
                    mLstPhoto.add(photo);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        return mLstPhoto;
    }
}
