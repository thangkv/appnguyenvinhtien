package com.nguyenvinhtien.music.activities.van_hoc.tho;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.PostSuggestListActivity;
import com.nguyenvinhtien.music.activities.other.MainActivity;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.adapter.post.PostListSuggestAdapter;
import com.nguyenvinhtien.music.adapter.tho.ThoListSuggestAdapter;
import com.nguyenvinhtien.music.models.Post;
import com.nguyenvinhtien.music.models.Tho;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

public class ThoSuggestListActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvNewsTitle;
    RecyclerView mRvListNews;
    ThoListSuggestAdapter mNewsAdapter;
    ArrayList<Tho> mLstNews = new ArrayList<>();


    String query;
    ThoSuggestListActivity.LoadListNews loadListNews;


    public LinearLayout currentPlayingBar;
    PlayMusicService musicService;
    ImageView btnPlayPauseCurrent;
    ImageView btnNextCurrent;
    ImageView btnPrevCurrent;
    ImageView imgAlbumArtCurrent;
    TextView tvTitle;
    TextView tvArtist;

    String currentPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tho_suggest_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_list_news_suggest);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Common.setStatusBarTranslucent(true, this);
        AppController.getInstance().setThoSuggestActivity(this);

        initControls();
        initEvents();

        showListNews();


        if (musicService != null) {
            updatePlayingState();
            showCurrentSong();
        }

        registerBroadcastUpdatePlayingSuggestNews();

        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

    BroadcastReceiver broadcastReceiverUpdatePlayingSuggestNews = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
            if (musicService != null) {
                currentPlayingBar.setVisibility(View.VISIBLE);

            } else {
                currentPlayingBar.setVisibility(View.GONE);
            }
            showCurrentSong();
            updatePlayingState();
        }
    };

    private void registerBroadcastUpdatePlayingSuggestNews() {
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_UPDATE_PLAY_STATUS);
        registerReceiver(broadcastReceiverUpdatePlayingSuggestNews, intentFilter);
    }

    private void unRegisterBroadcastUpdatePlayingSuggestNews() {
        unregisterReceiver(broadcastReceiverUpdatePlayingSuggestNews);
    }

    public void showCurrentSong() {
        if (musicService != null) {
            tvTitle.setText(musicService.getCurrentSong().getTitle());
            tvArtist.setText(musicService.getCurrentSong().getArtist());
            String albumPath = musicService.getCurrentSong().getAlbumImagePath();

            currentPath = musicService.getCurrentSong().getPath();

            if (albumPath != null) {
                if (albumPath.contains("firebasestorage")) {
                    if (albumPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    } else {
                        Glide.with(this).load(albumPath).into(imgAlbumArtCurrent);
                    }
                } else {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(currentPath);

                    byte[] image = mmr.getEmbeddedPicture();
                    if (image != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                        imgAlbumArtCurrent.setImageBitmap(bitmap);
                    } else {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    }
                }
            } else {
                imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
            }
        }
    }

    public void updatePlayingState() {
        if (musicService != null) {
            if (musicService.isPlaying()) {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
            } else {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
            }
        }
    }



    private void initEvents() {

        currentPlayingBar.setOnClickListener(this);
        btnPlayPauseCurrent.setOnClickListener(this);
        btnNextCurrent.setOnClickListener(this);
        btnPrevCurrent.setOnClickListener(this);
    }

    private void initControls() {
        tvNewsTitle = (TextView) findViewById(R.id.tv_news_suggest_title);
        mRvListNews = (RecyclerView) findViewById(R.id.rv_news_suggest_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRvListNews.setLayoutManager(layoutManager);


        currentPlayingBar = (LinearLayout) findViewById(R.id.current_playing_bar);
        btnPlayPauseCurrent = (ImageView) findViewById(R.id.btn_play_pause_current);
        btnNextCurrent = (ImageView) findViewById(R.id.btn_next_current);
        btnPrevCurrent = (ImageView) findViewById(R.id.btn_prev_current);
        imgAlbumArtCurrent = (ImageView) findViewById(R.id.img_album_current_bar);
        tvTitle = (TextView) findViewById(R.id.tv_song_title_current);
        tvTitle.setSelected(true);
        tvArtist = (TextView) findViewById(R.id.tv_artist_current);

        musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        if (musicService != null) {
            currentPlayingBar.setVisibility(View.VISIBLE);
        } else {
            currentPlayingBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.current_playing_bar:
                if (musicService != null) {
                    Intent intent = new Intent(ThoSuggestListActivity.this, PlayMusicActivity.class);
                    intent.putExtra(PlayMusicActivity.IS_PLAYING, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
                }
                break;
            case R.id.btn_play_pause_current:
                if (musicService != null) {
                    Intent intent = new Intent(Constants.ACTION_PLAY_PAUSE);
                    if (musicService.isPlaying()) {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
                    } else {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
                    }
                    sendBroadcast(intent);

                }
                break;
            case R.id.btn_next_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_NEXT);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_NEXT);
                        sendBroadcast(intent);
                    }

                }
                break;
            case R.id.btn_prev_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_PREV);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_PREV);
                        sendBroadcast(intent);
                    }


                }
                break;
        }
    }

    //thay đổi button Play,Pause khi stop service trên thanh thông báo
    public void changePlayButtonState() {
        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showListNews() {
        loadListNews = new ThoSuggestListActivity.LoadListNews();
        loadListNews.execute();
    }


    private class LoadListNews extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Intent intent = getIntent();
            query = intent.getStringExtra(MainActivity.TITLE_THO);

            for (int i = 0; i < Tho.getListPost().size(); i++) {
                if (Tho.getListPost().get(i).getTitle().toLowerCase().contains(query.toLowerCase())) {
                    mLstNews.add(Tho.getListPost().get(i));
                }
            }
            return mLstNews;
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            mNewsAdapter = new ThoListSuggestAdapter(ThoSuggestListActivity.this, mLstNews);
            mNewsAdapter.notifyDataSetChanged();
            mRvListNews.setAdapter(mNewsAdapter);
            tvNewsTitle.setText(query);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterBroadcastUpdatePlayingSuggestNews();
    }
}
