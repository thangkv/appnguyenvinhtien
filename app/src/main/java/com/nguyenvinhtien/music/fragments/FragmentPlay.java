package com.nguyenvinhtien.music.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.hit_song.CommentSongActivity;
import com.nguyenvinhtien.music.activities.kien_truc.audio_kien_truc.CommentAudioKienTrucActivity;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.activities.van_hoc.audio_van_hoc.CommentAudioVanHocActivity;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;

import de.hdodenhof.circleimageview.CircleImageView;


public class FragmentPlay extends Fragment implements View.OnClickListener {

    public static final String KEY_ALBUM_PLAY = "key_album_play";

    public static final String KEY_SONG_PLAY = "key_song_play";

    public static final String TYPE = "type";//KVT

    View mView;


    public static CircleImageView mIvAlbum;
    String path;
    String pathSong;
    String type;


    ImageButton btnComment;

    FirebaseUser user;

    PlayMusicService musicService;
    PlayMusicActivity musicActivity;


    public FragmentPlay() {
        // Required empty public constructor
    }


    //Hàm này để ViewPagerPlayAdapter gọi để gán imgPath rồi BroadcastReceiver lấy ra
    public static FragmentPlay newInstance(String imgPath, String songPath, String type) {
        FragmentPlay fragment = new FragmentPlay();
        Bundle args = new Bundle();
        args.putString(KEY_ALBUM_PLAY, imgPath);
        args.putString(KEY_SONG_PLAY, songPath);
        args.putString(TYPE, type);


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBroadcastAlbumArt();

        user = FirebaseAuth.getInstance().getCurrentUser();
        musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
        musicActivity = (PlayMusicActivity) AppController.getInstance().getPlayMusicActivity();

        //Nếu Fragment đã được tạo ra từ trc thì lấy path từ argument của Fragment đó
        if (getArguments() != null) {
            path = getArguments().getString(KEY_ALBUM_PLAY);
            pathSong = getArguments().getString(KEY_SONG_PLAY);
            type = getArguments().getString(TYPE);

        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_play, container, false);
        initControls();
        initEvents();

        showImage();

        return mView;
    }

    //BroadcastReceiver lắng nghe thay đổi AlbumArt
    BroadcastReceiver broadcastReceiverAlbumArt = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            path = intent.getExtras().getString(KEY_ALBUM_PLAY);
            pathSong = intent.getExtras().getString(KEY_SONG_PLAY);
            showImage();

        }
    };

    private void registerBroadcastAlbumArt() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_CHANGE_ALBUM_ART);
        getActivity().registerReceiver(broadcastReceiverAlbumArt, intentFilter);
    }

    private void unregisterBroadcastAlbumArt() {
        getActivity().unregisterReceiver(broadcastReceiverAlbumArt);
    }


    private void showImage() {
        if (path != null) {
            if (path.contains("firebasestorage")) {
                if (path.equals(Constants.DEFAULT_SONG_ART_URL)) {
                    mIvAlbum.setImageResource(R.drawable.default_cover_big);
                } else {
                    Glide.with(this).load(path).into(mIvAlbum);
                }
            } else {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(pathSong);

                byte[] image = mmr.getEmbeddedPicture();
                if (image != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                    mIvAlbum.setImageBitmap(bitmap);
                } else {
                    mIvAlbum.setImageResource(R.drawable.default_cover_big);
                }

            }
        } else {
            mIvAlbum.setImageResource(R.drawable.default_cover_big);
        }

    }

    //Hiệu ứng xoay albumArt
    public void albumArtAnimation() {
        Animation rotateAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_animation);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        mIvAlbum.startAnimation(rotateAnimation);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBroadcastAlbumArt();
    }

    private void initControls() {
        mIvAlbum = (CircleImageView) mView.findViewById(R.id.img_album_play);
        btnComment = (ImageButton) mView.findViewById(R.id.btn_comment);

            if (pathSong.contains("firebasestorage")) {
                btnComment.setVisibility(View.VISIBLE);
            } else {
                btnComment.setVisibility(View.INVISIBLE);
            }

    }

    private void initEvents() {
        btnComment.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_comment:
                if (AppController.getInstance().checkInternetState() == true) {
                    if (type.equals("kientruc")) {
                        Intent intent = new Intent(getContext(), CommentAudioKienTrucActivity.class);
                        intent.putExtra(Constants.NOTI, "no");//KVT
                        startActivity(intent);
                    } else if (type.equals("vanhoc")){
                        Intent intent = new Intent(getContext(), CommentAudioVanHocActivity.class);
                        intent.putExtra(Constants.NOTI, "no");//KVT
                        startActivity(intent);
                    } else{
                        Intent intent = new Intent(getContext(), CommentSongActivity.class);
                        intent.putExtra(Constants.NOTI, "no");//KVT
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(getContext(), "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }
}
