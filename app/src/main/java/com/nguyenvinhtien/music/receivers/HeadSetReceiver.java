package com.nguyenvinhtien.music.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;

import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Constants;

public class HeadSetReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PlayMusicActivity musicActivity = (PlayMusicActivity) AppController.getInstance().getPlayMusicActivity();
        PlayMusicService musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        if (musicService!= null){

        if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
            if (musicActivity != null) {
                musicActivity.pauseMusic();
                musicService.changePlayPauseState();
                Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                context.sendBroadcast(intent1);
                Log.d("playpause", "test");
            } else {
                    musicService.pauseMusic();
                    musicService.changePlayPauseState();
                    Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
                    context.sendBroadcast(intent1);
                }

            }
            Log.d("Headphone", "Headset unplugged");
            Intent intent1 = new Intent(Constants.ACTION_UPDATE_PLAY_STATUS);
            context.sendBroadcast(intent1);
            musicService.showNotification(true);

        }

    }
}
