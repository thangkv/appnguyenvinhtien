package com.nguyenvinhtien.music.activities.update_song;

import android.app.ProgressDialog;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;


import java.util.ArrayList;
import java.util.List;

public class UpdateDataSongActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_SONG_REQUEST = 4;


    TextView tvSongSelect, tvSongArt;
    Button btnSelectSong, btnUpdateSong;
    EditText edtSongTitle, edtSongArtist, edtSongLyric;
    TableLayout tbSongDetial;
    RelativeLayout rlSongDetail;
    ImageButton imageSelect;


    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_song;
    String title_song;
    String artist_song;
    String image_path_song;
    String lyric_song;

   List<String> key_album = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_data_song);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_data_song);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        rlSongDetail = (RelativeLayout) findViewById(R.id.rl_detail_song);
        tvSongSelect = (TextView) findViewById(R.id.tv_song_select);
        btnSelectSong = (Button) findViewById(R.id.btn_select_song);
        btnUpdateSong = (Button) findViewById(R.id.btn_update);
        edtSongTitle = (EditText) findViewById(R.id.edt_song_title);
        edtSongArtist = (EditText) findViewById(R.id.edt_song_artist);
        edtSongLyric = (EditText) findViewById(R.id.edt_song_lyric);
        tbSongDetial = (TableLayout) findViewById(R.id.tb_song_detail);
        imageSelect = (ImageButton) findViewById(R.id.image_select);
        tvSongArt = (TextView) findViewById(R.id.tv_song_art);
    }

    public void initEvents() {
        btnSelectSong.setOnClickListener(this);
        btnUpdateSong.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_song:
                Intent selectSongIntent = new Intent(UpdateDataSongActivity.this, SelectSongToUpdateActivity.class);
                startActivityForResult(selectSongIntent, SELECT_SONG_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_SONG_REQUEST && resultCode == RESULT_OK) {
            key_song = data.getStringExtra("KEY_SONG");
            title_song = data.getStringExtra("TITLE_SONG");
            artist_song = data.getStringExtra("ARTIST_SONG");
            image_path_song = data.getStringExtra("IMAGE_PATH_SONG");
            lyric_song = data.getStringExtra("LYRIC_SONG");

            //lấy key_album trong albums của bài hát để cập nhật dữ liệu về bài hát đó trong Albums-Songs
            mDatabase.child("Songs").child(key_song).child("albums").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String keyalbum = dataSnapshot.getValue().toString();
                    key_album.add(keyalbum);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            updateUI();

        }


    }

    private void updateUI() {
        if (title_song != null && artist_song != null
                && lyric_song != null) {
            tvSongSelect.setVisibility(View.VISIBLE);
            tvSongSelect.setText(title_song);
            rlSongDetail.setVisibility(View.VISIBLE);
            edtSongTitle.setText(title_song);
            edtSongArtist.setText(artist_song);
            Glide.with(this).load(image_path_song).into(imageSelect);
            edtSongLyric.setText(lyric_song);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật bài hát...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String song_title_val = edtSongTitle.getText().toString().trim();
        final String song_artist_val = edtSongArtist.getText().toString().trim();
        final String song_lyric_val = edtSongLyric.getText().toString().trim();

        if (!TextUtils.isEmpty(song_title_val)
                && !TextUtils.isEmpty(song_artist_val)
                && !TextUtils.isEmpty(song_lyric_val)
                && !tvSongSelect.getText().equals("")) {

            mProgress.show();

            //cập nhật dữ liệu bài hát trong Songs trên database server
            final DatabaseReference newSong = mDatabase.child("Songs").child(key_song);

                newSong.child("title").setValue(song_title_val);
                newSong.child("artist").setValue(song_artist_val);
                newSong.child("lyric").setValue(song_lyric_val);
                Toast.makeText(getApplicationContext(), "Cập nhật bài hát thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();

            //cập nhật dữ liệu bài hát trong Albums-Songs trên database server
            for (int i = 0; i < key_album.size(); i++){
                DatabaseReference album_song = mDatabase.child("Albums-Songs").child(key_album.get(i)).child(key_song);
                album_song.child("title").setValue(song_title_val);
                album_song.child("artist").setValue(song_artist_val);
                album_song.child("lyric").setValue(song_lyric_val);

            }

        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn bài hát và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
