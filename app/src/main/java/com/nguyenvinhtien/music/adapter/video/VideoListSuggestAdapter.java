package com.nguyenvinhtien.music.adapter.video;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.other.PlayVideoActivity;
import com.nguyenvinhtien.music.models.Video;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/19/2017.
 */

public class VideoListSuggestAdapter  extends RecyclerView.Adapter<VideoListSuggestAdapter.ViewHolderVideo> {

    Context mContext;
    ArrayList<Video> mData;
    LayoutInflater mLayoutInflater;

    public VideoListSuggestAdapter(Activity mContext, ArrayList<Video> mData) {
        this.mContext = mContext;
        this.mData = mData;
        mLayoutInflater = LayoutInflater.from(mContext);


    }

    @Override
    public VideoListSuggestAdapter.ViewHolderVideo onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_video, null);
        VideoListSuggestAdapter.ViewHolderVideo holder = new VideoListSuggestAdapter.ViewHolderVideo(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(VideoListSuggestAdapter.ViewHolderVideo holder, int position) {
        Video item = mData.get(position);
        String path = mData.get(position).getImagePath();

        if (path != null && !path.isEmpty()) {
            Glide.with(mContext).load(path).placeholder(R.drawable.video_youtube).into(holder.imgImage);
        } else {
            holder.imgImage.setImageResource(R.drawable.default_cover_big);
        }

        holder.tvTitle.setText(item.getTitle());
        holder.tvArtist.setText(item.getArtist());
        holder.setId(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolderVideo extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        public TextView tvArtist;
        public ImageView imgImage;
        int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public ViewHolderVideo(View itemView) {
            super(itemView);
            imgImage = (ImageView) itemView.findViewById(R.id.iv_video_img_item);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_video_title_item);
            tvArtist = (TextView) itemView.findViewById(R.id.tv_video_artist_item);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, PlayVideoActivity.class);
            intent.putExtra(PlayVideoActivity.VIDEO_LINK, mData.get(id).getPath());
            intent.putExtra(Constants.VIDEO_ID, mData.get(id).getId());
            intent.putExtra(Constants.VIDEO_TITLE, mData.get(id).getTitle());
            intent.putExtra(Constants.PRIVACY_DESC, mData.get(id).getDescription());
            mContext.startActivity(intent);
        }
    }
}



