package com.nguyenvinhtien.music.adapter.song;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.ViewHolderSong> {


    Activity mContext;
    ArrayList<Song> mData;

    LayoutInflater mLayoutInflater;

    public SongListAdapter(Activity mContext, ArrayList<Song> mData) {
        this.mContext = mContext;
        this.mData = mData;

        mLayoutInflater = LayoutInflater.from(mContext);

        notifyDataSetChanged();

    }

    @Override
    public ViewHolderSong onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_song, null);
        ViewHolderSong holder = new ViewHolderSong(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolderSong holder, int position) {
        Song item = mData.get(position);
        String path = item.getAlbumImagePath();

        String pathSong = item.getPath();

        holder.tvTitle.setText(item.getTitle());
        holder.tvArtist.setText(item.getArtist());
        holder.setId(position);


        if (path != null) {
            if (path.contains("firebasestorage")) {
                if (path.equals(Constants.DEFAULT_SONG_ART_URL)) {
                    holder.image.setImageResource(R.drawable.default_cover_big);
                } else {
                    Glide.with(mContext).load(path).into(holder.image);
                }
            } else {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(pathSong);

                byte[] image = mmr.getEmbeddedPicture();
                if (image != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                    holder.image.setImageBitmap(bitmap);
                } else {
                    holder.image.setImageResource(R.drawable.default_cover_big);
                }
            }
        } else {
            holder.image.setImageResource(R.drawable.default_cover_big);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setFilter(ArrayList<Song> lstSong) {
        mData = new ArrayList<>();
        mData.addAll(lstSong);
        notifyDataSetChanged();
    }

    public class ViewHolderSong extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        TextView tvArtist;
        ImageView image;
        int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public ViewHolderSong(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.img_album_song_play);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_song_title_item);
            tvArtist = (TextView) itemView.findViewById(R.id.artist_name_song_item);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, PlayMusicActivity.class);
            intent.putExtra(Constants.SONG_PATH, mData.get(id).getPath());
            intent.putExtra(Constants.SONG_POS, id);
            intent.putExtra(Constants.LIST_SONG, mData);
            intent.putExtra(PlayMusicActivity.IS_PLAYING, false);
            mContext.startActivity(intent);
            mContext.overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
        }


    }

}
