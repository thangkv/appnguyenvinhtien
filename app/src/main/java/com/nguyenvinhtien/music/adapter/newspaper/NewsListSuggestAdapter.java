package com.nguyenvinhtien.music.adapter.newspaper;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.newspaper.NewspapeDetailActivity;
import com.nguyenvinhtien.music.models.Newspaper;
import com.nguyenvinhtien.music.utils.Constants;

import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/22/2017.
 */

public class NewsListSuggestAdapter extends RecyclerView.Adapter<NewsListSuggestAdapter.ViewHolderNews> {


    Activity mContext;
    ArrayList<Newspaper> mData;

    LayoutInflater mLayoutInflater;

    public NewsListSuggestAdapter(Activity mContext, ArrayList<Newspaper> mData) {
        this.mContext = mContext;
        this.mData = mData;

        mLayoutInflater = LayoutInflater.from(mContext);


    }

    @Override
    public NewsListSuggestAdapter.ViewHolderNews onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_news, null);
        NewsListSuggestAdapter.ViewHolderNews holder = new NewsListSuggestAdapter.ViewHolderNews(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final NewsListSuggestAdapter.ViewHolderNews holder, final int position) {

        Newspaper item = mData.get(position);
        holder.setId(position);
        String imagePath = item.getImagePath();
        if (imagePath != null && !imagePath.isEmpty()) {
            Glide.with(mContext).load(imagePath).placeholder(R.drawable.picture).into(holder.imgNews);
        } else {
            holder.imgNews.setImageResource(R.drawable.default_cover_big);
        }
        holder.tvTitle.setText(item.getTitle());
        holder.tvSource.setText(item.getSource());
        holder.tvOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, holder.tvOption);
                popupMenu.inflate(R.menu.menu_news);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item_share:
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mData.get(position).getLink());
                                mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



    public class ViewHolderNews extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        TextView tvSource;
        ImageView imgNews;
        TextView tvOption;


        int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public ViewHolderNews(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_news);
            tvSource = (TextView) itemView.findViewById(R.id.tv_source_news);
            imgNews = (ImageView) itemView.findViewById(R.id.img_news);
            tvOption = (TextView) itemView.findViewById(R.id.tv_option);


            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, NewspapeDetailActivity.class);
            intent.putExtra(Constants.NEWS_LINK, mData.get(id).getLink());
            intent.putExtra(Constants.NEWS_ID, mData.get(id).getId());
            intent.putExtra(Constants.NEWS_TITLE, mData.get(id).getTitle());
            mContext.startActivity(intent);
            mContext.overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
        }



    }


}
