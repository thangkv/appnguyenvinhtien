package com.nguyenvinhtien.music.activities.bibliography;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.activities.update_bibliography.UpdateBiblioContentActivity;
import com.nguyenvinhtien.music.activities.update_bibliography.AddPhotoActivity;
import com.nguyenvinhtien.music.activities.update_bibliography.DeletePhotoActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateBibliographyActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddPhoto;
    Button btnDeletePhoto;
    Button btnUpdateContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_bibliography);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_biblio);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnAddPhoto = (Button) findViewById(R.id.btn_AddBiblioPhoto);
        btnDeletePhoto = (Button) findViewById(R.id.btn_DeletebiblioPhoto);
        btnUpdateContent = (Button)findViewById(R.id.btn_UpdateBiblio);

    }

    public void initEvents(){
        btnAddPhoto.setOnClickListener(this);
        btnDeletePhoto.setOnClickListener(this);
        btnUpdateContent.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddBiblioPhoto:
                Intent intent_update_song = new Intent(getApplicationContext(), AddPhotoActivity.class);
                startActivity(intent_update_song);
                break;
            case R.id.btn_DeletebiblioPhoto:
                Intent intent_update_album = new Intent(getApplicationContext(), DeletePhotoActivity.class);
                startActivity(intent_update_album);
                break;
            case R.id.btn_UpdateBiblio:
                Intent intent_update_video = new Intent(getApplicationContext(), UpdateBiblioContentActivity.class);
                startActivity(intent_update_video);
                break;

        }
    }
}
