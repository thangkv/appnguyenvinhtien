package com.nguyenvinhtien.music.activities.update_audio_vanhoc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;

import java.io.ByteArrayOutputStream;

public class AddAudioVanHocActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int MUSIC_GALLERY_REQUEST = 1;


    Button btnSelectSong, btnUpdateSong;
    ImageButton ibSelectSongArt;
    EditText edtSongTitle, edtSongArtist, edtSongLyric;
    TextView tvStateSongSelect, tvSongArt;
    Uri songUri = null;

    ProgressDialog mProgress;

    StorageReference mStorage;
    DatabaseReference mDatabase;

    Uri songURL = null, imageURL = null;


    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_audio_van_hoc);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_song);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        btnSelectSong = (Button) findViewById(R.id.btn_select_song);
        btnUpdateSong = (Button) findViewById(R.id.btn_update);
        ibSelectSongArt = (ImageButton) findViewById(R.id.image_select);
        edtSongTitle = (EditText) findViewById(R.id.edt_song_title);
        edtSongArtist = (EditText) findViewById(R.id.edt_song_artist);
        edtSongLyric = (EditText) findViewById(R.id.edt_song_lyric);
        tvStateSongSelect = (TextView) findViewById(R.id.tv_select_song_state);
        tvSongArt = (TextView) findViewById(R.id.tv_song_art);
    }

    public void initEvents() {
        btnSelectSong.setOnClickListener(this);
        btnUpdateSong.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_song:
                Intent intent_select_song = new Intent(Intent.ACTION_GET_CONTENT);
                intent_select_song.setType("audio/*");
                startActivityForResult(intent_select_song, MUSIC_GALLERY_REQUEST);
                break;


            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false) {
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MUSIC_GALLERY_REQUEST && resultCode == RESULT_OK) {
            songUri = data.getData();

            if (songUri != null) {

                tvSongArt.setVisibility(View.VISIBLE);
                ibSelectSongArt.setVisibility(View.VISIBLE);

                try {
                    String path = AppController.getInstance().getRealPathFromURI(songUri);

                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(path);

                    byte[] image = mmr.getEmbeddedPicture();
                    if (image != null) {
                        bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                        ibSelectSongArt.setImageBitmap(bitmap); //associated cover art in bitmap
                    } else {
                        ibSelectSongArt.setImageResource(R.drawable.default_cover); //any default cover resourse folder
                    }





                } catch (Exception e) {

                }

                tvStateSongSelect.setText("Đã chọn");

            }
        }


    }

    private void startUpdate() {
        mProgress.setMessage("Đang thêm Audio văn học...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);


        final String song_title_val = edtSongTitle.getText().toString().trim();
        final String song_lyric_val = edtSongLyric.getText().toString().trim();

        if (!TextUtils.isEmpty(song_title_val)
                && !TextUtils.isEmpty(song_lyric_val)
                && songUri != null) {

            mProgress.show();

            final DatabaseReference newSong = mDatabase.child("AudioVanHocs").push();
            String key_song = newSong.getKey();

            StorageReference imagePath = mStorage.child("AudioVanHocArts").child(key_song);


            //upload ảnh bìa bài hát lên storage
            if (bitmap != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();

                UploadTask uploadTask = imagePath.putBytes(data);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        imageURL = taskSnapshot.getDownloadUrl();
                        newSong.child("albumImagePath").setValue(imageURL.toString());
                    }
                });
            } else {
                newSong.child("albumImagePath").setValue(Constants.DEFAULT_SONG_ART_URL);
            }


            StorageReference songPath = mStorage.child("AudioVanHocs").child(key_song);

            //upload bài hát lên storage
            songPath.putFile(songUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    songURL = taskSnapshot.getDownloadUrl();
                    newSong.child("title").setValue(song_title_val);
                    newSong.child("artist").setValue("Nguyễn Vĩnh Tiến");
                    newSong.child("path").setValue(songURL.toString());
                    newSong.child("lyric").setValue(song_lyric_val);
                    newSong.child("type").setValue("vanhoc");

                    //AppController.getInstance().sendNotificationAddAudioVanHoc(song_title_val);
                    Toast.makeText(getApplicationContext(), "Thêm audio văn học thành công!", Toast.LENGTH_SHORT).show();
                    mProgress.dismiss();
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Lỗi! Audio chưa được upload!", Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn audio và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
