package com.nguyenvinhtien.music.activities.update_news;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class DeleteNewsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_NEWS_REQUEST = 77;
    TextView tvNewsSelect;
    Button btnSelectNews, btnUpdateNews;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_news;
    String title_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_delete_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_delete_news);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvNewsSelect = (TextView) findViewById(R.id.tv_news_select);
        btnSelectNews = (Button) findViewById(R.id.btn_select_news);
        btnUpdateNews = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnSelectNews.setOnClickListener(this);
        btnUpdateNews.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_news:
                Intent selectNewsIntent = new Intent(DeleteNewsActivity.this, SelectNewsToUpdateActivity.class);
                startActivityForResult(selectNewsIntent, SELECT_NEWS_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_NEWS_REQUEST && resultCode == RESULT_OK) {
            key_news = data.getStringExtra("KEY_NEWS");
            title_news = data.getStringExtra("TITLE_NEWS");
            updateUI();

        }

    }

    private void updateUI() {
        if (title_news != null) {
            tvNewsSelect.setVisibility(View.VISIBLE);
            tvNewsSelect.setText(title_news);

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Xóa bài báo...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
        if (!tvNewsSelect.getText().equals("")){


            mProgress.show();

            DatabaseReference news = mDatabase.child("News").child(key_news);
            news.removeValue();

            DatabaseReference news_comment = mDatabase.child("News-Comments").child(key_news);
            news_comment.removeValue();

            StorageReference imagePath = mStorage.child("NewsArts").child(key_news);
            imagePath.delete();

            Toast.makeText(getApplicationContext(), "Xóa bài báo thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();
        } else {
            Toast.makeText(this, "Cần chọn bài báo muốn xóa", Toast.LENGTH_SHORT).show();
        }
    }
}
