package com.nguyenvinhtien.music.adapter.bibliography;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.models.Photo;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by KimVanThang on 4/27/2017.
 */

public class FullScreenImageAdapter extends PagerAdapter {
    private Activity _activity;
    private ArrayList<Photo> mLstPhoto;
    private LayoutInflater inflater;


    public FullScreenImageAdapter(Activity activity,
                                  ArrayList<Photo> mLstPhoto) {
        this._activity = activity;
        this.mLstPhoto = mLstPhoto;
    }

    @Override
    public int getCount() {
        return this.mLstPhoto.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PhotoView imgDisplay;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);


        imgDisplay = (PhotoView) viewLayout.findViewById(R.id.imgDisplay);
        Glide.with(_activity).load(mLstPhoto.get(position).getPath()).into(imgDisplay);

        PhotoViewAttacher photoView = new PhotoViewAttacher(imgDisplay);
        photoView.update();


        ((ViewPager) container).addView(viewLayout);


        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
