package com.nguyenvinhtien.music.utils;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.database.Cursor;

import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.models.Song;

import org.json.JSONObject;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AppController extends Application {

    private static AppController mInstance;

    private Service playMusicService;
    private Activity playMusicActivity;
    private Activity mainActivity;
    private Activity hotVideoActivity;
    private Activity hotVideoSuggestActivity;
    private Activity hitSongActivity;
    private Activity hitSongSuggestActivity;
    private Activity hotAlbumActivity;
    private Activity hotAlbumSuggestActivity;
    private Activity albumDetailActivity;
    private Activity hotNewsActivity;
    private Activity hotNewsSuggestActivity;
    private Activity privacyActivity;
    private Activity photoPrivacyActivity;
    private Activity postPrivacyActivity;
    private Activity biblioActivity;
    private Activity offlineSongActivity;
    private Activity thoActivity;
    private Activity truyenActivity;
    private Activity vanhocActivity;
    private Activity audiovanhocActivity;
    private Activity audiokientrucActivity;
    private Activity kientrucActivity;
    private Activity bibliographyActivity;
    private Activity congtrinhActivity;
    private Activity postSuggestActivity;
    private Activity thoSuggestActivity;
    private Activity truyenSuggestActivity;
    private Activity congTrinhSuggestActivity;
    private Activity audioKienTrucSuggestActivity;
    private Activity audioVanHocSuggestActivity;

    private ArrayList<Song> lstSong;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public Activity getAudioKienTrucSuggestActivity() {
        return audioKienTrucSuggestActivity;
    }

    public void setAudioKienTrucSuggestActivity(Activity audioKienTrucSuggestActivity) {
        this.audioKienTrucSuggestActivity = audioKienTrucSuggestActivity;
    }

    public Activity getAudioVanHocSuggestActivity() {
        return audioVanHocSuggestActivity;
    }

    public void setAudioVanHocSuggestActivity(Activity audioVanHocSuggestActivity) {
        this.audioVanHocSuggestActivity = audioVanHocSuggestActivity;
    }

    public Activity getCongTrinhSuggestActivity() {
        return congTrinhSuggestActivity;
    }

    public void setCongTrinhSuggestActivity(Activity congTrinhSuggestActivity) {
        this.congTrinhSuggestActivity = congTrinhSuggestActivity;
    }

    public Activity getThoSuggestActivity() {
        return thoSuggestActivity;
    }

    public void setThoSuggestActivity(Activity thoSuggestActivity) {
        this.thoSuggestActivity = thoSuggestActivity;
    }

    public Activity getTruyenSuggestActivity() {
        return truyenSuggestActivity;
    }

    public void setTruyenSuggestActivity(Activity truyenSuggestActivity) {
        this.truyenSuggestActivity = truyenSuggestActivity;
    }

    public Activity getPostSuggestActivity() {
        return postSuggestActivity;
    }

    public void setPostSuggestActivity(Activity postSuggestActivity) {
        this.postSuggestActivity = postSuggestActivity;
    }

    public Activity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(Activity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public Activity getHotVideoActivity() {
        return hotVideoActivity;
    }

    public void setHotVideoActivity(Activity hotVideoActivity) {
        this.hotVideoActivity = hotVideoActivity;
    }

    public Activity getHotVideoSuggestActivity() {
        return hotVideoSuggestActivity;
    }

    public void setHotVideoSuggestActivity(Activity hotVideoSuggestActivity) {
        this.hotVideoSuggestActivity = hotVideoSuggestActivity;
    }

    public Activity getHitSongActivity() {
        return hitSongActivity;
    }

    public void setHitSongActivity(Activity hitSongActivity) {
        this.hitSongActivity = hitSongActivity;
    }

    public Activity getHitSongSuggestActivity() {
        return hitSongSuggestActivity;
    }

    public void setHitSongSuggestActivity(Activity hitSongSuggestActivity) {
        this.hitSongSuggestActivity = hitSongSuggestActivity;
    }

    public Activity getHotAlbumActivity() {
        return hotAlbumActivity;
    }

    public void setHotAlbumActivity(Activity hotAlbumActivity) {
        this.hotAlbumActivity = hotAlbumActivity;
    }

    public Activity getHotAlbumSuggestActivity() {
        return hotAlbumSuggestActivity;
    }

    public void setHotAlbumSuggestActivity(Activity hotAlbumSuggestActivity) {
        this.hotAlbumSuggestActivity = hotAlbumSuggestActivity;
    }

    public Activity getAlbumDetailActivity() {
        return albumDetailActivity;
    }

    public void setAlbumDetailActivity(Activity albumDetailActivity) {
        this.albumDetailActivity = albumDetailActivity;
    }

    public Activity getHotNewsActivity() {
        return hotNewsActivity;
    }

    public void setHotNewsActivity(Activity hotNewsActivity) {
        this.hotNewsActivity = hotNewsActivity;
    }

    public Activity getHotNewsSuggestActivity() {
        return hotNewsSuggestActivity;
    }

    public void setHotNewsSuggestActivity(Activity hotNewsSuggestActivity) {
        this.hotNewsSuggestActivity = hotNewsSuggestActivity;
    }

    public Activity getPrivacyActivity() {
        return privacyActivity;
    }

    public void setPrivacyActivity(Activity privacyActivity) {
        this.privacyActivity = privacyActivity;
    }

    public Activity getPhotoPrivacyActivity() {
        return photoPrivacyActivity;
    }

    public void setPhotoPrivacyActivity(Activity photoPrivacyActivity) {
        this.photoPrivacyActivity = photoPrivacyActivity;
    }

    public Activity getPostPrivacyActivity() {
        return postPrivacyActivity;
    }

    public void setPostPrivacyActivity(Activity postPrivacyActivity) {
        this.postPrivacyActivity = postPrivacyActivity;
    }

    public Activity getBiblioActivity() {
        return biblioActivity;
    }

    public void setBiblioActivity(Activity biblioActivity) {
        this.biblioActivity = biblioActivity;
    }

    public Activity getOfflineSongActivity() {
        return offlineSongActivity;
    }

    public void setOfflineSongActivity(Activity offlineSongActivity) {
        this.offlineSongActivity = offlineSongActivity;
    }

    public Activity getThoActivity() {
        return thoActivity;
    }

    public void setThoActivity(Activity thoActivity) {
        this.thoActivity = thoActivity;
    }

    public Activity getTruyenActivity() {
        return truyenActivity;
    }

    public void setTruyenActivity(Activity truyenActivity) {
        this.truyenActivity = truyenActivity;
    }

    public Activity getVanhocActivity() {
        return vanhocActivity;
    }

    public void setVanhocActivity(Activity vanhocActivity) {
        this.vanhocActivity = vanhocActivity;
    }

    public Activity getAudiovanhocActivity() {
        return audiovanhocActivity;
    }

    public void setAudiovanhocActivity(Activity audiovanhocActivity) {
        this.audiovanhocActivity = audiovanhocActivity;
    }

    public Activity getAudiokientrucActivity() {
        return audiokientrucActivity;
    }

    public void setAudiokientrucActivity(Activity audiokientrucActivity) {
        this.audiokientrucActivity = audiokientrucActivity;
    }

    public Activity getKientrucActivity() {
        return kientrucActivity;
    }

    public void setKientrucActivity(Activity kientrucActivity) {
        this.kientrucActivity = kientrucActivity;
    }

    public Activity getBibliographyActivity() {
        return bibliographyActivity;
    }

    public void setBibliographyActivity(Activity bibliographyActivity) {
        this.bibliographyActivity = bibliographyActivity;
    }

    public Activity getCongtrinhActivity() {
        return congtrinhActivity;
    }

    public void setCongtrinhActivity(Activity congtrinhActivity) {
        this.congtrinhActivity = congtrinhActivity;
    }

    public Service getPlayMusicService() {
        return playMusicService;
    }

    public void setPlayMusicService(Service playMusicService) {
        this.playMusicService = playMusicService;
    }

    public Activity getPlayMusicActivity() {
        return playMusicActivity;
    }

    public void setPlayMusicActivity(Activity playMusicActivity) {
        this.playMusicActivity = playMusicActivity;
    }



    public static AppController getInstance() {
        return mInstance;
    }



    //Lay danh sach bai hat trong thu muc Nguyen Vinh Tien
    public ArrayList<Song> getListSong() {
        ArrayList<Song> lstSong = new ArrayList<>();
        String selection = MediaStore.Audio.Media.DATA + " LIKE '%/Nguyễn Vĩnh Tiến - Nhạc/%'";

        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Media.ALBUM,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.DURATION,
                        MediaStore.Audio.Media.DATA,
                        MediaStore.Audio.Media.ALBUM_ID,
                        MediaStore.Audio.Media.ARTIST_ID
                }
                , selection, null, MediaStore.Audio.Media.TITLE + " ASC");


        if (cursor != null && cursor.moveToFirst()) {
            do {
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String songId = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                int duration = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String albumID = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                String albumPath = getCoverArtPath(albumID);
                Song item = new Song(songId, title, album, artist, albumPath, duration, path);

                File f = new File(path);
                if (f.exists()) {
                    lstSong.add(item);

                }

            } while (cursor.moveToNext());

        }

        return lstSong;
    }


    public String getCoverArtPath(String albumId) {
        Cursor albumCursor = getContentResolver().query(
                MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Albums.ALBUM_ART},
                MediaStore.Audio.Albums._ID + " = ?",
                new String[]{albumId},
                null
        );
        boolean queryResult = albumCursor.moveToFirst();
        String result = null;
        if (queryResult) {
            result = albumCursor.getString(0);
        }
        albumCursor.close();
        return result;
    }


    //kiểm tra trạng thái mạng
    public boolean checkInternetState() {
        Boolean check = false;
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                || conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            // notify user you are online
            check = true;

        } else if (conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED
                || conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {

            check = false;
            // notify user you are not online
        }
        return check;
    }

    //lấy đường dẫn thực từ Uri
    public String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }


    //các hàm request thông báo mỗi khi cập nhật dữ liệu
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    //gửi thông báo đến các máy khi thêm album
    public void sendNotificationAddAlbum(final String tenalbum, final String tencasi) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();
                    String key = tenalbum + " - " + tencasi;

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Album mới!");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();

                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm bài báo
    public void sendNotificationAddNews(final String key) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Bài báo mới!");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm bài hát
    public void sendNotificationAddSong(final String tenbaihat, final String tencasi) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();
                    String key = tenbaihat + " - " + tencasi;

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Bài hát mới!");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm audio văn học
    public void sendNotificationAddAudioVanHoc(final String tenbaihat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();
                    String key = tenbaihat;

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có File âm thanh văn học mới!");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm audio kiến trúc
    public void sendNotificationAddAudioKienTruc(final String tenbaihat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();
                    String key = tenbaihat;

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Audio kiến trúc mới!");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm video
    public void sendNotificationAddVideo(final String tenbaihat, final String tencasi) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();
                    String key = tenbaihat + " - " + tencasi;

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Video mới!");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }


    //gửi thông báo đến các máy khi thêm hình ảnh
    public void sendNotificationAddPhoto(final String key) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();


                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Hình ảnh mới!");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm bài viết
    public void sendNotificationAddPost(final String key) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Bài viết mới");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm công trình
    public void sendNotificationAddCongTrinh(final String key) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Công trình mới");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }



    //gửi thông báo đến các máy khi thêm bài thơ
    public void sendNotificationAddTho(final String key) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Bài thơ mới");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi thêm truyện
    public void sendNotificationAddTruyen(final String key) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("message", key);

                    json2.put("body", key);
                    json2.put("title", "Có Truyện đọc mới");

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận bài báo
    public void sendNotificationAddNewsComment(final String key, final String message, final String tenbaibao) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận bài báo " + tenbaibao);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận bài viết
    public void sendNotificationAddPostComment(final String key, final String message, final String tenbaiviet) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();
                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận bài viết " + tenbaiviet);


                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận bài viết
    public void sendNotificationAddCongTrinhComment(final String key, final String message, final String tenbaiviet) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận công trình " + tenbaiviet);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận bài thơ
    public void sendNotificationAddThoComment(final String key, final String message, final String tenbaiviet) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận bài thơ " + tenbaiviet);


                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận truyện
    public void sendNotificationAddTruyenComment(final String key, final String message, final String tenbaiviet) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận truyện " + tenbaiviet);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }


    //gửi thông báo đến các máy khi bình luận video
    public void sendNotificationAddVideoComment(final String key, final String message, final String tenvideo) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận video " + tenvideo);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }


    //gửi thông báo đến các máy khi bình luận hình ảnh
    public void sendNotificationAddPhotoComment(final String key, final String message, final String tenhinhanh) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận hình ảnh " + tenhinhanh);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận bài hát
    public void sendNotificationAddSongComment(final String key, final String message
            , final String tenbaihat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);
                    json2.put("song_title", tenbaihat);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận bài hát " + tenbaihat);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận audio kiến trúc
    public void sendNotificationAddAudioKienTrucComment(final String key, final String message, final String tenbaihat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);
                    json2.put("song_title", tenbaihat);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận audio kiến trúc " + tenbaihat);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    //gửi thông báo đến các máy khi bình luận audio văn học
    public void sendNotificationAddAudioVanHocComment(final String key, final String message, final String tenbaihat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json = new JSONObject();

                    JSONObject json2 = new JSONObject();

                    json2.put("key", key);
                    json2.put("message", message);
                    json2.put("song_title", tenbaihat);

                    json2.put("body", message);
                    json2.put("title", "Có bình luận file âm thanh văn học " + tenbaihat);

                    json.put("to", "/topics/foo-bar");
                    json.put("data", json2);

                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization", "key=" + Constants.LEGACY_SERVER_KEY)
                            .url(Constants.FCM_PUSH_URL)
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                } catch (Exception e) {
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }


}
