package com.nguyenvinhtien.music.activities.update_news;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class AddNewsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int IMAGE_GALLERY_REQUEST = 8;

    Button btnUpdateNews;
    ImageButton ibSelectNewsArt;
    EditText edtNewsTitle, edtNewsSource, edtNewsLink;
    Uri imageUri = null;
    ProgressDialog mProgress;

    StorageReference mStorage;
    DatabaseReference mDatabase;

    Uri imageURL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_news);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        mProgress = new ProgressDialog(this);

        btnUpdateNews = (Button)findViewById(R.id.btn_update);
        ibSelectNewsArt = (ImageButton) findViewById(R.id.image_select);
        edtNewsTitle = (EditText)findViewById(R.id.edt_news_title);
        edtNewsSource = (EditText)findViewById(R.id.edt_news_source);
        edtNewsLink = (EditText)findViewById(R.id.edt_news_link);
    }

    public void initEvents(){

        btnUpdateNews.setOnClickListener(this);
        ibSelectNewsArt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_select:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            Glide.with(this).load(imageUri).centerCrop().into(ibSelectNewsArt);
        }
    }

    private void startUpdate() {
        mProgress.setMessage("Đang thêm Bài báo...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);


        final String news_title_val = edtNewsTitle.getText().toString().trim();
        final String news_source_val = edtNewsSource.getText().toString().trim();
        final String news_link_val = edtNewsLink.getText().toString().trim();

        if (!TextUtils.isEmpty(news_title_val)
                && !TextUtils.isEmpty(news_source_val)
                && !TextUtils.isEmpty(news_link_val)
                && imageUri != null){

            mProgress.show();

            final DatabaseReference newNews = mDatabase.child("News").push();
            String key_news = newNews.getKey();

            StorageReference imagePath = mStorage.child("NewsArts").child(key_news);

            imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imageURL = taskSnapshot.getDownloadUrl();
                    newNews.child("title").setValue(news_title_val);
                    newNews.child("source").setValue(news_source_val);
                    newNews.child("imagePath").setValue(imageURL.toString());
                    newNews.child("link").setValue(news_link_val);

                    //AppController.getInstance().sendNotificationAddNews(news_title_val);
                    Toast.makeText(getApplicationContext(), "Thêm bài báo thành công!", Toast.LENGTH_SHORT).show();
                    mProgress.dismiss();
                    finish();


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Cần điền đủ các trường và chọn ảnh", Toast.LENGTH_SHORT).show();
        }
    }
}
