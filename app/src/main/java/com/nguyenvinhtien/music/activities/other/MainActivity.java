package com.nguyenvinhtien.music.activities.other;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.bibliography.BibliographyActivity;
import com.nguyenvinhtien.music.activities.hit_song.HitSongActivity;
import com.nguyenvinhtien.music.activities.hot_album.HotAlbumActivity;
import com.nguyenvinhtien.music.activities.hot_video.HotVideoActivity;
import com.nguyenvinhtien.music.activities.newspaper.NewspaperListActivity;
import com.nguyenvinhtien.music.activities.kien_truc.KienTrucActivity;
import com.nguyenvinhtien.music.activities.hit_song.SuggestSongListActivity;
import com.nguyenvinhtien.music.activities.van_hoc.VanHocAcitivity;
import com.nguyenvinhtien.music.adapter.other.ItemListOfflineMusicAdapter;
import com.nguyenvinhtien.music.models.Album;
import com.nguyenvinhtien.music.models.Biblio;
import com.nguyenvinhtien.music.models.CongTrinh;
import com.nguyenvinhtien.music.models.ItemListOfflineMusic;
import com.nguyenvinhtien.music.models.Newspaper;
import com.nguyenvinhtien.music.models.Photo;
import com.nguyenvinhtien.music.models.Post;
import com.nguyenvinhtien.music.models.Song;

import com.nguyenvinhtien.music.models.Tho;
import com.nguyenvinhtien.music.models.Truyen;
import com.nguyenvinhtien.music.models.Video;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;


import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static String SONG_NAME = "songName";
    public static String AUDIOKIENTRUC_NAME = "audioKienTrucName";
    public static String AUDIOVANHOC_NAME = "audioVanHocName";
    public static String TITLE_ALBUM = "titleAlbum";
    public static String TITLE_VIDEO = "titleVideo";
    public static String TITLE_NEWS = "titleNews";
    public static String TITLE_POST = "titlePost";
    public static String TITLE_THO = "titleTho";
    public static String TITLE_TRUYEN = "titleTruyen";
    public static String TITLE_CONGTRINH = "titleCongTrinh";

    RecyclerView mRvListMain;
    ArrayList<ItemListOfflineMusic> mListMain;
    ItemListOfflineMusicAdapter mMainAdapter;
    public LinearLayout currentPlayingBar;
    PlayMusicService musicService;
    ImageView btnPlayPauseCurrent;
    ImageView btnNextCurrent;
    ImageView btnPrevCurrent;
    ImageView imgAlbumArtCurrent;
    TextView tvTitle;
    TextView tvArtist;

    ImageButton ibuttonVideoMain;
    ImageButton ibuttonHitSong;
    ImageButton ibuttonHotAlbum;
    ImageButton ibuttonNewspapers;
    ImageButton ibuttonKienTruc;
    ImageButton ibuttonVanHoc;
    ImageButton ibuttonBibliography;

    android.support.v4.widget.SimpleCursorAdapter suggestionAdapter;

    RelativeLayout rlMainOnline;

    CircleImageView ivAccount;
    TextView tvAccount;

    TextView tvInternetState;

    TextView tvVideo;
    TextView tvSong;
    TextView tvAlbum;
    TextView tvNews;
    TextView tvKienTruc;
    TextView tvVanHoc;
    TextView tvBiblio;
    LinearLayout lnAccount;

    String currentPath;

    FirebaseAuth mAuth;

    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        final String[] from = new String[]{"songName"};
        final int[] to = new int[]{android.R.id.text1};
        suggestionAdapter = new android.support.v4.widget.SimpleCursorAdapter(this,
                android.R.layout.simple_expandable_list_item_1,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);
        AppController.getInstance().setMainActivity(this);

        Song.getListSong();
        Song.getListAudioVanHoc();
        Song.getListAudioKienTruc();
        Video.getListVideo();
        Album.getListAlbum();
        Newspaper.getListNewspaper();
        CongTrinh.getListCongTrinh();
        Post.getListPost();
        Tho.getListPost();
        Truyen.getListTruyen();
        Biblio.showSuNghiep();
        Biblio.showTieuSu();

        initControls();
        initEvents();

        FirebaseMessaging.getInstance().subscribeToTopic("foo-bar");

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        if (user != null) {
            tvAccount.setText(user.getDisplayName());
            Glide.with(this).load(user.getPhotoUrl()).into(ivAccount);
        }


        showListMain();

        if (musicService != null) {
            updatePlayingState();
            showCurrentSong();
        }
        registerBroadcastUpdatePlaying();

        if (AppController.getInstance().checkInternetState() == false) {
            rlMainOnline.setVisibility(View.GONE);
            tvInternetState.setVisibility(View.VISIBLE);
            tvInternetState.append("\nChạm để thử lại...");

        }
    }


    private void showListMain() {
        mListMain = new ArrayList<>();
        mListMain.add(new ItemListOfflineMusic(R.drawable.ic_song_main, getString(R.string.list_song), AppController.getInstance().getListSong().size()));

        mMainAdapter = new ItemListOfflineMusicAdapter(MainActivity.this, mListMain);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        mRvListMain.setLayoutManager(layoutManager);
        mRvListMain.setAdapter(mMainAdapter);

    }

    private void initControls() {
        rlMainOnline = (RelativeLayout) findViewById(R.id.rl_main_online);

        tvInternetState = (TextView) findViewById(R.id.tv_internet_state);

        mRvListMain = (RecyclerView) findViewById(R.id.rv_list_main);
        currentPlayingBar = (LinearLayout) findViewById(R.id.current_playing_bar);
        btnPlayPauseCurrent = (ImageView) findViewById(R.id.btn_play_pause_current);
        btnNextCurrent = (ImageView) findViewById(R.id.btn_next_current);
        btnPrevCurrent = (ImageView) findViewById(R.id.btn_prev_current);
        imgAlbumArtCurrent = (ImageView) findViewById(R.id.img_album_current_bar);
        tvTitle = (TextView) findViewById(R.id.tv_song_title_current);
        tvTitle.setSelected(true);
        tvArtist = (TextView) findViewById(R.id.tv_artist_current);

        lnAccount = (LinearLayout) findViewById(R.id.ln_account);

        musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        ibuttonVideoMain = (ImageButton) findViewById(R.id.ibutton_video_main);
        ibuttonHitSong = (ImageButton) findViewById(R.id.ibutton_song_main);
        ibuttonHotAlbum = (ImageButton) findViewById(R.id.ibutton_album_main);
        ibuttonNewspapers = (ImageButton) findViewById(R.id.ibutton_newspaper_main);
        ibuttonKienTruc = (ImageButton) findViewById(R.id.ibutton_kientruc_main);
        ibuttonVanHoc = (ImageButton) findViewById(R.id.ibutton_vanhoc_main);
        ibuttonBibliography = (ImageButton) findViewById(R.id.ibutton_bibliography_main);

        ivAccount = (CircleImageView) findViewById(R.id.iv_taikhoan);
        tvAccount = (TextView) findViewById(R.id.tv_taikhoan);

        tvVideo = (TextView) findViewById(R.id.tv_video_main);
        tvSong = (TextView) findViewById(R.id.tv_song_main);
        tvAlbum = (TextView) findViewById(R.id.tv_album_main);
        tvNews = (TextView) findViewById(R.id.tv_news_main);
        tvKienTruc = (TextView) findViewById(R.id.tv_kientruc_main);
        tvVanHoc = (TextView) findViewById(R.id.tv_vanhoc_main);
        tvBiblio = (TextView) findViewById(R.id.tv_biblio_main);

        if (musicService != null) {
            currentPlayingBar.setVisibility(View.VISIBLE);
        } else {
            currentPlayingBar.setVisibility(View.GONE);
        }
    }

    private void initEvents() {
        currentPlayingBar.setOnClickListener(this);
        btnPlayPauseCurrent.setOnClickListener(this);
        btnNextCurrent.setOnClickListener(this);
        btnPrevCurrent.setOnClickListener(this);

        ibuttonVideoMain.setOnClickListener(this);
        ibuttonHitSong.setOnClickListener(this);
        ibuttonHotAlbum.setOnClickListener(this);
        ibuttonNewspapers.setOnClickListener(this);
        ibuttonKienTruc.setOnClickListener(this);
        ibuttonVanHoc.setOnClickListener(this);
        ibuttonBibliography.setOnClickListener(this);

        tvVideo.setOnClickListener(this);
        tvSong.setOnClickListener(this);
        tvAlbum.setOnClickListener(this);
        tvNews.setOnClickListener(this);
        tvKienTruc.setOnClickListener(this);
        tvVanHoc.setOnClickListener(this);
        tvBiblio.setOnClickListener(this);

        lnAccount.setOnClickListener(this);

        tvInternetState.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_internet_state:
                finish();
                startActivity(getIntent());
                break;

            case R.id.ln_account:

                Intent intent_taikhoan = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(intent_taikhoan);
                break;

            case R.id.tv_video_main:
                Intent intent_tvVideo = new Intent(MainActivity.this, HotVideoActivity.class);
                startActivity(intent_tvVideo);
                break;


            case R.id.ibutton_video_main:
                Intent intentVideo = new Intent(MainActivity.this, HotVideoActivity.class);
                startActivity(intentVideo);
                break;


            case R.id.ibutton_song_main:
                Intent intentHitSong = new Intent(MainActivity.this, HitSongActivity.class);
                startActivity(intentHitSong);
                break;

            case R.id.tv_song_main:
                Intent intent_tvHitSong = new Intent(MainActivity.this, HitSongActivity.class);
                startActivity(intent_tvHitSong);
                break;

            case R.id.ibutton_album_main:
                Intent intentHotAlbum = new Intent(MainActivity.this, HotAlbumActivity.class);
                startActivity(intentHotAlbum);
                break;

            case R.id.tv_album_main:
                Intent intent_tvHotAlbum = new Intent(MainActivity.this, HotAlbumActivity.class);
                startActivity(intent_tvHotAlbum);
                break;

            case R.id.ibutton_newspaper_main:
                Intent intentNews = new Intent(MainActivity.this, NewspaperListActivity.class);
                startActivity(intentNews);
                break;

            case R.id.tv_news_main:
                Intent intent_tvNews = new Intent(MainActivity.this, NewspaperListActivity.class);
                startActivity(intent_tvNews);
                break;

            case R.id.ibutton_kientruc_main:
                Intent intentPrivacy = new Intent(MainActivity.this, KienTrucActivity.class);
                startActivity(intentPrivacy);
                break;

            case R.id.tv_kientruc_main:
                Intent intent_tvPrivacy = new Intent(MainActivity.this, KienTrucActivity.class);
                startActivity(intent_tvPrivacy);
                break;

            case R.id.ibutton_vanhoc_main:
                Intent intentVanHoc = new Intent(MainActivity.this, VanHocAcitivity.class);
                startActivity(intentVanHoc);
                break;

            case R.id.tv_vanhoc_main:
                Intent intent_tvVanHoc = new Intent(MainActivity.this, KienTrucActivity.class);
                startActivity(intent_tvVanHoc);
                break;

            case R.id.ibutton_bibliography_main:
                Intent intentBibliography = new Intent(MainActivity.this, BibliographyActivity.class);
                startActivity(intentBibliography);
                break;

            case R.id.tv_biblio_main:
                Intent intent_tvBibliography = new Intent(MainActivity.this, BibliographyActivity.class);
                startActivity(intent_tvBibliography);
                break;


            case R.id.current_playing_bar:
                if (musicService != null) {
                    Intent intent = new Intent(MainActivity.this, PlayMusicActivity.class);
                    intent.putExtra(PlayMusicActivity.IS_PLAYING, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
                }
                break;
            case R.id.btn_play_pause_current:
                if (musicService != null) {
                    Intent intent = new Intent(Constants.ACTION_PLAY_PAUSE);
                    if (musicService.isPlaying()) {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
                    } else {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
                    }
                    sendBroadcast(intent);
                }
                break;
            case R.id.btn_next_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")) {
                        if (AppController.getInstance().checkInternetState() == false) {
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_NEXT);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_NEXT);
                        sendBroadcast(intent);
                    }

                }
                break;

            case R.id.btn_prev_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")) {
                        if (AppController.getInstance().checkInternetState() == false) {
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_PREV);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_PREV);
                        sendBroadcast(intent);
                    }

                }
                break;

        }
    }

    BroadcastReceiver broadcastReceiverUpdatePlaying = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
            if (musicService != null) {
                currentPlayingBar.setVisibility(View.VISIBLE);

            } else {
                currentPlayingBar.setVisibility(View.GONE);
            }
            showCurrentSong();
            updatePlayingState();
        }
    };

    private void registerBroadcastUpdatePlaying() {
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_UPDATE_PLAY_STATUS);
        registerReceiver(broadcastReceiverUpdatePlaying, intentFilter);
    }

    private void unRegisterBroadcastUpdatePlaying() {
        unregisterReceiver(broadcastReceiverUpdatePlaying);
    }

    public void updatePlayingState() {
        if (musicService != null) {
            if (musicService.isPlaying()) {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
            } else {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
            }
        }
    }

    //show bài hát đang phát (khung dưới cùng của màn hình chính)
    public void showCurrentSong() {
        if (musicService != null) {
            tvTitle.setText(musicService.getCurrentSong().getTitle());
            tvArtist.setText(musicService.getCurrentSong().getArtist());
            String albumPath = musicService.getCurrentSong().getAlbumImagePath();

            currentPath = musicService.getCurrentSong().getPath();

            if (albumPath != null) {
                if (albumPath.contains("firebasestorage")) {
                    if (albumPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    } else {
                        Glide.with(this).load(albumPath).into(imgAlbumArtCurrent);
                    }
                } else {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(currentPath);

                    byte[] image = mmr.getEmbeddedPicture();
                    if (image != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                        imgAlbumArtCurrent.setImageBitmap(bitmap);
                    } else {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    }
                }
            } else {
                imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
            }
        }
    }



    //thay đổi button Play,Pause khi stop service trên thanh thông báo
    public void changePlayButtonState() {
        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_search_detail);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        searchAutoComplete.setTextColor(Color.WHITE);
        searchAutoComplete.setHint("Nhập tên bài hát");
        searchAutoComplete.setHintTextColor(Color.GRAY);
        searchAutoComplete.setHighlightColor(Color.WHITE);


        ImageView closeButtonImage = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButtonImage.setColorFilter(Color.WHITE);

        searchView.setSuggestionsAdapter(suggestionAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getSuggestion(newText);
                return true;
            }
        });

        //Khi click vao goi y se goi lop List Activity hien thi list cac bai hat goi y
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                suggestionAdapter.getCursor().moveToPosition(position); //move to row
                String query = suggestionAdapter.getCursor().getString(1); //move to column
                //create intent
                Intent intent = new Intent(MainActivity.this, SuggestSongListActivity.class);
                intent.putExtra(SONG_NAME, query);
                startActivity(intent);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);

    }

    //suggest search, Lay ra cac goi y
    private void getSuggestion(String text) {
        //Suggestion is a matrix which has 2 columns and n rows. Column 0 is ID, column 1 is songName.
        MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "songName"});
        for (int i = 0; i < Song.getListSong().size(); i++) {
            //if songName contains input text, add them on suggestion
            if (Song.getListSong().get(i).getTitle().toLowerCase().contains(text.toLowerCase())) {
                c.addRow(new Object[]{i, Song.getListSong().get(i).getTitle()});
            }
        }

        suggestionAdapter.changeCursor(c);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().setMainActivity(null);
        try {
            unRegisterBroadcastUpdatePlaying();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


}
