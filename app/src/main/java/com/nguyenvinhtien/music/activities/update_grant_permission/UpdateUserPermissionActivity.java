package com.nguyenvinhtien.music.activities.update_grant_permission;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.IdRes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateUserPermissionActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_USER_REQUEST = 88;

    TextView tvUserSelect, tvPermissionSelected;
    Button btnSelectUser, btnUpdateUser;
    RelativeLayout rlUserDetail;

    RadioGroup rgUpdateUser;
    RadioButton rbAdmin, rbDatabase, rbNotPermission;

    String permission_type;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;

    String key_user;
    String email_user;
    String permission_user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_user_permission);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_user_permission);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvUserSelect = (TextView) findViewById(R.id.tv_user_select);
        btnSelectUser = (Button) findViewById(R.id.btn_select_user);
        btnUpdateUser = (Button) findViewById(R.id.btn_update);
        rlUserDetail = (RelativeLayout) findViewById(R.id.rl_user_detail);
        rgUpdateUser = (RadioGroup) findViewById(R.id.rg_UpdateUser);
        rbAdmin = (RadioButton) findViewById(R.id.rb_admin);
        rbDatabase = (RadioButton) findViewById(R.id.rb_database);
        rbNotPermission = (RadioButton) findViewById(R.id.rb_not_permission);
        tvPermissionSelected = (TextView) findViewById(R.id.tv_permission_selected);

        rgUpdateUser.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (rbAdmin.isChecked()) {
                    permission_type = "Admins";
                    tvPermissionSelected.setText("Đã chọn Quyền quản trị");
                }

                if (rbDatabase.isChecked()) {
                    permission_type = "Special-Users";
                    tvPermissionSelected.setText("Đã chọn Quyền cập nhật dữ liệu");
                }

                if (rbNotPermission.isChecked()) {
                    permission_type = "Not permission";
                    tvPermissionSelected.setText("Không cấp quyền cho người dùng này");
                }
            }
        });


    }

    public void initEvents() {
        btnSelectUser.setOnClickListener(this);
        btnUpdateUser.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_user:
                Intent selectNewsIntent = new Intent(UpdateUserPermissionActivity.this, SelectUserToUpdateActivity.class);
                startActivityForResult(selectNewsIntent, SELECT_USER_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_USER_REQUEST && resultCode == RESULT_OK) {
            key_user = data.getStringExtra("KEY_USER");
            email_user = data.getStringExtra("EMAIL_USER");
            permission_user = data.getStringExtra("PERMISSION_USER");
            updateUI();

        }
    }

    private void updateUI() {
        if (email_user != null) {
            tvUserSelect.setVisibility(View.VISIBLE);
            tvUserSelect.setText(email_user);
            rlUserDetail.setVisibility(View.VISIBLE);
            if (permission_user.equals("Admins")) {
                rgUpdateUser.check(R.id.rb_admin);
            } else {
                rgUpdateUser.check(R.id.rb_database);
            }

        }
    }


    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật quyền người dùng...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final DatabaseReference perRef = mDatabase.child("Permission-Users");

        if (!tvUserSelect.getText().equals("")) {

            mProgress.show();

            if (permission_type.equals("Not permission")) {
                DatabaseReference ref = perRef.child(permission_user).child(key_user);
                ref.removeValue();
            } else {
                if (!permission_type.equals(permission_user)) {
                    DatabaseReference oldRef = perRef.child(permission_user).child(key_user);
                    oldRef.removeValue();

                    DatabaseReference newRef = perRef.child(permission_type).push();
                    newRef.child("email").setValue(email_user);
                }
            }

            Toast.makeText(getApplicationContext(), "Cập nhật quyền thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn người dùng và chọn quyền", Toast.LENGTH_SHORT).show();
        }
    }
}
