package com.nguyenvinhtien.music.activities.update_grant_permission;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.Common;

import java.util.ArrayList;

public class GrantPermissionActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddUser;
    Button btnDeleteUser;

    public ArrayList<String> listAdminUser = new ArrayList<>();
    public ArrayList<String> listSpecialUser = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_grant_permission);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_grant_permission);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        listAdminUser = getIntent().getStringArrayListExtra("LIST_ADMIN_USER");
        listSpecialUser = getIntent().getStringArrayListExtra("LIST_SPECIAL_USER");

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnAddUser = (Button)findViewById(R.id.btn_AddUser);
        btnDeleteUser = (Button)findViewById(R.id.btn_DeleteUser);
    }

    public void initEvents(){
        btnAddUser.setOnClickListener(this);
        btnDeleteUser.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddUser:
                Intent intent_add_user = new Intent(getApplicationContext(), AddUserPermissionActivity.class);
                intent_add_user.putExtra("LIST_ADMIN_USER", listAdminUser);
                intent_add_user.putExtra("LIST_SPECIAL_USER", listSpecialUser);
                startActivity(intent_add_user);
                break;
            case R.id.btn_DeleteUser:
                Intent intent_delete_user = new Intent(getApplicationContext(), UpdateUserPermissionActivity.class);
                startActivity(intent_delete_user);
                break;

        }
    }
}
