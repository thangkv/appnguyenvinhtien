package com.nguyenvinhtien.music.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;


/**
 * Created by KimVanThang on 4/17/2017.
 */

public class AlbumRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView tvTitle;
    public TextView tvArtist;
    public ImageView imgImage;

    public AlbumRecyclerViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_album_title_item);
        tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_album_item);
        imgImage = (ImageView) itemView.findViewById(R.id.iv_album_img_item);

    }
}
