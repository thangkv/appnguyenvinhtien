package com.nguyenvinhtien.music.activities.van_hoc.tho;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.HotPostPrivacyActivity;
import com.nguyenvinhtien.music.activities.kien_truc.bai_viet.PostSuggestListActivity;
import com.nguyenvinhtien.music.activities.other.PlayMusicActivity;
import com.nguyenvinhtien.music.adapter.tho.ThoListFirebaseAdapter;
import com.nguyenvinhtien.music.models.Post;
import com.nguyenvinhtien.music.models.Tho;
import com.nguyenvinhtien.music.services.PlayMusicService;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.PostRecyclerViewHolder;

import static com.nguyenvinhtien.music.activities.other.MainActivity.TITLE_POST;
import static com.nguyenvinhtien.music.activities.other.MainActivity.TITLE_THO;

public class ThoActivity extends AppCompatActivity implements View.OnClickListener {

    android.support.v4.widget.SimpleCursorAdapter suggestionAdapter;

    RecyclerView mRvThoList;

    private DatabaseReference mDatabase;
    ThoListFirebaseAdapter adapter;

    ThoActivity.LoadListThos loadListThos;

    public LinearLayout currentPlayingBar;
    PlayMusicService musicService;
    ImageView btnPlayPauseCurrent;
    ImageView btnNextCurrent;
    ImageView btnPrevCurrent;
    ImageView imgAlbumArtCurrent;
    TextView tvTitle;
    TextView tvArtist;

    String currentPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tho);

        final String[] from = new String[]{"titleTho"};
        final int[] to = new int[]{android.R.id.text1};
        suggestionAdapter = new android.support.v4.widget.SimpleCursorAdapter(this,
                android.R.layout.simple_expandable_list_item_1,
                null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_list_news);
        setSupportActionBar(toolbar);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initControls();

        initEvents();

        Common.setStatusBarTranslucent(true, this);

        AppController.getInstance().setThoActivity(this);



        mDatabase = FirebaseDatabase.getInstance().getReference();

        showListThos();


        if (musicService != null) {
            updatePlayingState();
            showCurrentSong();
        }

        registerBroadcastUpdatePlayingListNews();


        if (AppController.getInstance().checkInternetState() == false){
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showListThos() {
        loadListThos = new ThoActivity.LoadListThos();
        loadListThos.execute();
    }

    private class LoadListThos extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Query queryComment = mDatabase.child("Thos");

            adapter = new ThoListFirebaseAdapter(ThoActivity.this, Tho.class, R.layout.item_post, PostRecyclerViewHolder.class, queryComment);
            adapter.notifyDataSetChanged();

            return adapter;
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            mRvThoList.setAdapter(adapter);
        }
    }


    BroadcastReceiver broadcastReceiverUpdatePlayingListNews = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();
            if (musicService != null) {
                currentPlayingBar.setVisibility(View.VISIBLE);

            } else {
                currentPlayingBar.setVisibility(View.GONE);
            }
            showCurrentSong();
            updatePlayingState();
        }
    };

    private void registerBroadcastUpdatePlayingListNews() {
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_UPDATE_PLAY_STATUS);
        registerReceiver(broadcastReceiverUpdatePlayingListNews, intentFilter);
    }

    private void unRegisterBroadcastUpdatePlayingListNews() {
        unregisterReceiver(broadcastReceiverUpdatePlayingListNews);
    }

    public void showCurrentSong() {
        if (musicService != null) {
            tvTitle.setText(musicService.getCurrentSong().getTitle());
            tvArtist.setText(musicService.getCurrentSong().getArtist());
            String albumPath = musicService.getCurrentSong().getAlbumImagePath();

            currentPath = musicService.getCurrentSong().getPath();

            if (albumPath != null) {
                if (albumPath.contains("firebasestorage")) {
                    if (albumPath.equals(Constants.DEFAULT_SONG_ART_URL)) {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    } else {
                        Glide.with(this).load(albumPath).into(imgAlbumArtCurrent);
                    }
                } else {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(currentPath);

                    byte[] image = mmr.getEmbeddedPicture();
                    if (image != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
                        imgAlbumArtCurrent.setImageBitmap(bitmap);
                    } else {
                        imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
                    }
                }
            } else {
                imgAlbumArtCurrent.setImageResource(R.drawable.default_cover_big);
            }
        }
    }

    public void updatePlayingState() {
        if (musicService != null) {
            if (musicService.isPlaying()) {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
            } else {
                btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
            }
        }
    }


    private void initEvents() {

        currentPlayingBar.setOnClickListener(this);
        btnPlayPauseCurrent.setOnClickListener(this);
        btnNextCurrent.setOnClickListener(this);
        btnPrevCurrent.setOnClickListener(this);
    }


    private void initControls() {


        mRvThoList = (RecyclerView) findViewById(R.id.rv_list_tho);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        mRvThoList.setLayoutManager(layoutManager);

        currentPlayingBar = (LinearLayout) findViewById(R.id.current_playing_bar);
        btnPlayPauseCurrent = (ImageView) findViewById(R.id.btn_play_pause_current);
        btnNextCurrent = (ImageView) findViewById(R.id.btn_next_current);
        btnPrevCurrent = (ImageView) findViewById(R.id.btn_prev_current);
        imgAlbumArtCurrent = (ImageView) findViewById(R.id.img_album_current_bar);
        tvTitle = (TextView) findViewById(R.id.tv_song_title_current);
        tvTitle.setSelected(true);
        tvArtist = (TextView) findViewById(R.id.tv_artist_current);

        musicService = (PlayMusicService) AppController.getInstance().getPlayMusicService();

        if (musicService != null) {
            currentPlayingBar.setVisibility(View.VISIBLE);
        } else {
            currentPlayingBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.current_playing_bar:
                if (musicService != null) {
                    Intent intent = new Intent(ThoActivity.this, PlayMusicActivity.class);
                    intent.putExtra(PlayMusicActivity.IS_PLAYING, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.no_change);
                }
                break;
            case R.id.btn_play_pause_current:
                if (musicService != null) {
                    Intent intent = new Intent(Constants.ACTION_PLAY_PAUSE);
                    if (musicService.isPlaying()) {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
                    } else {
                        btnPlayPauseCurrent.setImageResource(R.drawable.pb_pause);
                    }
                    sendBroadcast(intent);

                }
                break;
            case R.id.btn_next_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_NEXT);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_NEXT);
                        sendBroadcast(intent);
                    }

                }
                break;
            case R.id.btn_prev_current:
                if (musicService != null) {
                    currentPath = musicService.getCurrentSong().getPath();
                    if (currentPath.contains("firebasestorage")){
                        if (AppController.getInstance().checkInternetState() == false){
                            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(Constants.ACTION_PREV);
                            sendBroadcast(intent);
                        }
                    } else {
                        Intent intent = new Intent(Constants.ACTION_PREV);
                        sendBroadcast(intent);
                    }
                }
                break;
        }
    }


    //thay đổi button Play,Pause khi stop service trên thanh thông báo
    public void changePlayButtonState() {
        btnPlayPauseCurrent.setImageResource(R.drawable.pb_play);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_song_list, menu);
        MenuItem item = menu.findItem(R.id.action_search_detail);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        searchAutoComplete.setTextColor(Color.WHITE);
        searchAutoComplete.setHint("Nhập tên bài thơ");
        searchAutoComplete.setHintTextColor(Color.GRAY);

        ImageView closeButtonImage = (ImageView) searchView.findViewById(R.id.search_close_btn);
        closeButtonImage.setColorFilter(Color.WHITE);

        searchView.setSuggestionsAdapter(suggestionAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getSuggestion(newText);
                return true;
            }
        });

        //Khi click vao goi y se goi lop List Activity hien thi list cac bai bao goi y
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                suggestionAdapter.getCursor().moveToPosition(position); //move to row
                String query = suggestionAdapter.getCursor().getString(1); //move to column
                //create intent
                Intent intent = new Intent(ThoActivity.this, ThoSuggestListActivity.class);
                intent.putExtra(TITLE_THO, query);
                startActivity(intent);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    //suggest search, Lay ra cac goi y
    private void getSuggestion(String text) {
        //Suggestion is a matrix which has 2 columns and n rows. Column 0 is ID, column 1 is news title.
        MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "titleTho"});
        for (int i = 0; i < Tho.getListPost().size(); i++) {
            //if new title contains input text, add them on suggestion
            if (Tho.getListPost().get(i).getTitle().toLowerCase().contains(text.toLowerCase())) {
                c.addRow(new Object[]{i, Tho.getListPost().get(i).getTitle()});
            }
        }

        suggestionAdapter.changeCursor(c);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
        unRegisterBroadcastUpdatePlayingListNews();
    }
}
