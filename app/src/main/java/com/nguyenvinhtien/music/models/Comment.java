package com.nguyenvinhtien.music.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by KimVanThang on 4/7/2017.
 */

public class Comment {
    public String uID;
    public String author;
    public String content;



    public Comment(){

    }

    public Comment(String uID, String commentAuthor, String commentContent) {
        this.uID = uID;
        this.author = commentAuthor;
        this.content = commentContent;


    }

    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("uID", uID);
        result.put("author", author);
        result.put("content", content);


        return result;
    }
}

