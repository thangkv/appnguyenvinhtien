package com.nguyenvinhtien.music.activities.update_audio_vanhoc;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.update_song.SelectSongToUpdateActivity;
import com.nguyenvinhtien.music.activities.update_song.UpdateDataSongActivity;
import com.nguyenvinhtien.music.models.Song;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class SelectAudioVanHocToUpdateActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    RecyclerView listSong;
    FirebaseRecyclerAdapter<Song, SelectAudioVanHocToUpdateActivity.SelectSongRecyclerViewHolder> adapter;
    String songKey = null;
    String songTitle = null;
    String songImagePath = null;
    String songLyric = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_select_audio_van_hoc_to_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_song_to_update);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("AudioVanHocs");
        adapter = new FirebaseRecyclerAdapter<Song, SelectAudioVanHocToUpdateActivity.SelectSongRecyclerViewHolder>(Song.class, R.layout.item_select_song, SelectAudioVanHocToUpdateActivity.SelectSongRecyclerViewHolder.class, mDatabase) {
            @Override
            protected void populateViewHolder(final SelectAudioVanHocToUpdateActivity.SelectSongRecyclerViewHolder viewHolder, final Song model, final int position) {
                viewHolder.tvTitle.setText(model.getTitle());
                viewHolder.tvArtist.setText(model.getArtist());

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        songKey = getRef(position).getKey();
                        songTitle = model.getTitle();
                        songImagePath = model.getAlbumImagePath();
                        songLyric = model.getLyric();
                        viewHolder.itemView.setBackgroundColor(Color.rgb(66, 110, 180));
                        Intent i = new Intent(SelectAudioVanHocToUpdateActivity.this, UpdateDataAudioVanHocActivity.class);
                        i.putExtra("KEY_SONG", songKey);
                        i.putExtra("TITLE_SONG", songTitle);
                        i.putExtra("IMAGE_PATH_SONG", songImagePath);
                        i.putExtra("LYRIC_SONG", songLyric);

                        setResult(RESULT_OK, i);
                        finish();
                    }
                });


            }


        };
        initControls();
        initEvents();
        adapter.notifyDataSetChanged();
        listSong.setAdapter(adapter);

        if (AppController.getInstance().checkInternetState() == false) {
            Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }

    }

    public static class SelectSongRecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvArtist;

        public SelectSongRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title_song);
            tvArtist = (TextView) itemView.findViewById(R.id.tv_artist_song);

        }

    }

    private void initControls() {
        listSong = (RecyclerView) findViewById(R.id.rv_song_list_select);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listSong.setLayoutManager(layoutManager);

    }

    private void initEvents() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.cleanup();
    }
}
