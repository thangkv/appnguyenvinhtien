package com.nguyenvinhtien.music.activities.update_video;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class AddVideoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int IMAGE_GALLERY_REQUEST = 2;

    Button btnUpdateVideo;
    ImageButton ibSelectVideoArt;
    EditText edtVideoTitle, edtVideoArtist, edtVideoPath, edtVideoDesc;
    Uri imageUri = null;
    ProgressDialog mProgress;

    StorageReference mStorage;
    DatabaseReference mDatabase;

    Uri imageURL = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_video);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_video);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        mProgress = new ProgressDialog(this);

        btnUpdateVideo = (Button)findViewById(R.id.btn_update);
        ibSelectVideoArt = (ImageButton) findViewById(R.id.image_select);
        edtVideoTitle = (EditText)findViewById(R.id.edt_video_title);
        edtVideoArtist = (EditText)findViewById(R.id.edt_video_artist);
        edtVideoPath = (EditText)findViewById(R.id.edt_video_path);
        edtVideoDesc = (EditText) findViewById(R.id.edt_video_desc);
    }

    public void initEvents(){

        btnUpdateVideo.setOnClickListener(this);
        ibSelectVideoArt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_select:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            Glide.with(this).load(imageUri).centerCrop().into(ibSelectVideoArt);
        }
    }

    private void startUpdate() {
        mProgress.setMessage("Đang thêm Video...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);


        final String video_title_val = edtVideoTitle.getText().toString().trim();
        final String video_artist_val = edtVideoArtist.getText().toString().trim();
        final String video_path_val = edtVideoPath.getText().toString().trim();
        final String video_desc_val = edtVideoDesc.getText().toString().trim();

        if (!TextUtils.isEmpty(video_title_val)
                && !TextUtils.isEmpty(video_artist_val)
                && !TextUtils.isEmpty(video_path_val)
                && !TextUtils.isEmpty(video_desc_val)
                && imageUri != null){

            mProgress.show();

            final DatabaseReference newVideo = mDatabase.child("Videos").push();
            String key_video = newVideo.getKey();

            StorageReference imagePath = mStorage.child("VideoArts").child(key_video);

            imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imageURL = taskSnapshot.getDownloadUrl();
                    newVideo.child("title").setValue(video_title_val);
                    newVideo.child("artist").setValue(video_artist_val);
                    newVideo.child("imagePath").setValue(imageURL.toString());
                    newVideo.child("path").setValue(video_path_val);
                    newVideo.child("description").setValue(video_desc_val);

                    //AppController.getInstance().sendNotificationAddVideo(video_title_val, video_artist_val);
                    Toast.makeText(getApplicationContext(), "Thêm video thành công!", Toast.LENGTH_SHORT).show();
                    mProgress.dismiss();
                    finish();


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Cần điền đủ các trường và chọn ảnh", Toast.LENGTH_SHORT).show();
        }
    }
}