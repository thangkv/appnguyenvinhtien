package com.nguyenvinhtien.music.models;


import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;



public class Album implements Serializable {

    private String id;
    private String title;
    private String artist;

    String albumArtPath;
    String info;

    public static DatabaseReference mData = FirebaseDatabase.getInstance().getReference();

    private static ArrayList<Album> listAlbum;


    public Album() {

    }

    public Album(String title, String artist, String albumArtPath) {
        this.title = title;
        this.artist = artist;
        this.albumArtPath = albumArtPath;
    }

    public Album(String id, String title, String artist, String albumArtPath) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.albumArtPath = albumArtPath;
    }


    public String getAlbumArtPath() {
        return albumArtPath;
    }

    public void setAlbumArtPath(String albumArtPath) {
        this.albumArtPath = albumArtPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static ArrayList<Album> getListAlbum() {
        if (listAlbum == null) {
            listAlbum = new ArrayList<>();
            mData.child("Albums").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String artist = (String) dataSnapshot.child("artist").getValue();
                    String albumArtPath = (String) dataSnapshot.child("albumArtPath").getValue();

                    Album album = new Album(id, title, artist, albumArtPath);
                    listAlbum.add(album);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        return listAlbum;
    }
}
