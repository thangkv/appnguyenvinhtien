package com.nguyenvinhtien.music.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.Constants;



public class FragmentLyric extends Fragment {

    public static final String KEY_SONG_LYRIC = "key_song_lyric";

    View mView;
    String lyric;
    TextView mLyric;

    public FragmentLyric(){
        // Required empty public constructor
    }

    public static FragmentLyric newInstance(String lyric) {
        FragmentLyric fragment = new FragmentLyric();
        Bundle args = new Bundle();
        args.putString(KEY_SONG_LYRIC, lyric);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBroadcastSongLyric();
        //Nếu Fragment đã được tạo ra từ trc thì lấy path từ argument của Fragment đó
        if (getArguments() != null) {
            lyric = getArguments().getString(KEY_SONG_LYRIC);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_lyric, container, false);
        initControls();
        showLyric();



        return mView;
    }

    //BroadcastReceiver lắng nghe thay đổi SongLyric
    BroadcastReceiver broadcastReceiverSongLyric = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            lyric = intent.getExtras().getString(KEY_SONG_LYRIC);
            showLyric();
        }
    };

    private void registerBroadcastSongLyric() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_CHANGE_SONG_LYRIC);
        getActivity().registerReceiver(broadcastReceiverSongLyric, intentFilter);
    }

    private void unregisterBroadcastSongLyric() {
        getActivity().unregisterReceiver(broadcastReceiverSongLyric);
    }


    private void showLyric() {
        if (lyric != null) {
            mLyric.setText(lyric);
        } else {
            mLyric.setText("Không có lời bài hát");
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBroadcastSongLyric();
    }

    private void initControls() {
        mLyric = (TextView) mView.findViewById(R.id.tv_song_lyric);
        mLyric.setMovementMethod(new ScrollingMovementMethod());

    }


}
