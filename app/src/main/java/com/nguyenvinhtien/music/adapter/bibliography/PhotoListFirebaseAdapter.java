package com.nguyenvinhtien.music.adapter.bibliography;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.bumptech.glide.Glide;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.bibliography.PhotoDetailActivity;
import com.nguyenvinhtien.music.models.Photo;

import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.PhotoRecyclerViewHolder;


import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/27/2017.
 */

public class PhotoListFirebaseAdapter extends FirebaseRecyclerAdapter<Photo, PhotoRecyclerViewHolder>{


    private Activity mContext;
    private DatabaseReference mDatabase;
    ArrayList<Photo> mLstPhoto = new ArrayList<>();

    public PhotoListFirebaseAdapter(Activity mContext, Class<Photo> modelClass, int modelLayout, Class<PhotoRecyclerViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;

        getListPhoto();
    }

    @Override
    protected void populateViewHolder(PhotoRecyclerViewHolder viewHolder, final Photo model, final int position) {

        try {
            Glide.with(mContext).load(model.getPath()).centerCrop().placeholder(R.drawable.picture).into(viewHolder.imgImage);
        } catch (Exception e) {
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PhotoDetailActivity.class);
                intent.putExtra(String.valueOf(Constants.POSITION_PHOTO), position);
                intent.putExtra(Constants.PHOTO_ID, getRef(position).getKey());
                intent.putExtra(Constants.LIST_PHOTO, mLstPhoto);
                mContext.startActivity(intent);
            }
        });

    }

    private ArrayList<Photo> getListPhoto() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Photos").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                String title = (String) dataSnapshot.child("title").getValue();
                String path = (String) dataSnapshot.child("path").getValue();

                Photo photo = new Photo(id, title, path);
                mLstPhoto.add(photo);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return mLstPhoto;
    }
}
