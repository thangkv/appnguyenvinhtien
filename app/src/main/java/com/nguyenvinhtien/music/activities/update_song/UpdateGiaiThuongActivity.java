package com.nguyenvinhtien.music.activities.update_song;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateGiaiThuongActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnUpdateContent;
    EditText edtGiaiThuong;

    DatabaseReference mDatabase;
    ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_giai_thuong);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_giaithuong);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("GiaiThuong").child("content").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String noidung = dataSnapshot.getValue().toString();
                edtGiaiThuong.setText(noidung);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        edtGiaiThuong = (EditText) findViewById(R.id.edt_giaithuong);
        btnUpdateContent = (Button) findViewById(R.id.btn_update);

    }

    public void initEvents() {
        btnUpdateContent.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật giải thưởng...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String giai_thuong_val = edtGiaiThuong.getText().toString().trim();

        if (!TextUtils.isEmpty(giai_thuong_val)) {
            mProgress.show();
            mDatabase.child("GiaiThuong").child("content").setValue(giai_thuong_val);

            Toast.makeText(getApplicationContext(), "Cập nhật giải thưởng thành công!", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            finish();

        } else {
            Toast.makeText(getApplicationContext(), "Cần điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
