package com.nguyenvinhtien.music.adapter.album;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.hot_album.AlbumOnlineListActivity;
import com.nguyenvinhtien.music.adapter.song.SongOnlineListFirebaseAdapter;
import com.nguyenvinhtien.music.models.Album;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.AlbumRecyclerViewHolder;


import java.util.ArrayList;

/**
 * Created by KimVanThang on 4/17/2017.
 */

public class AlbumOnlineGridFirebaseAdapter extends FirebaseRecyclerAdapter<Album, AlbumRecyclerViewHolder> {


    private Activity mContext;
    private DatabaseReference mDatabase;
    ArrayList<Album> mLstAlbum = new ArrayList<>();

    public AlbumOnlineGridFirebaseAdapter(Activity mContext, Class<Album> modelClass, int modelLayout, Class<AlbumRecyclerViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;

        getListAlbum();
    }

    @Override
    protected void populateViewHolder(AlbumRecyclerViewHolder viewHolder, final Album model, final int position) {
        viewHolder.tvTitle.setText(model.getTitle());
        viewHolder.tvArtist.setText(model.getArtist());
        try {
            if (!model.getAlbumArtPath().equals(Constants.DEFAULT_SONG_ART_URL)){
                Glide.with(mContext).load(model.getAlbumArtPath()).placeholder(R.drawable.picture).into(viewHolder.imgImage);
            } else {
                viewHolder.imgImage.setImageResource(R.drawable.default_cover_big);
            }
        } catch (Exception e) {
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AlbumOnlineListActivity.class);
                intent.putExtra(SongOnlineListFirebaseAdapter.ALBUM_KEY_ONLINE, getRef(position).getKey());
                mContext.startActivity(intent);
            }
        });

    }



    private ArrayList<Album> getListAlbum(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Albums").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                String title = (String) dataSnapshot.child("title").getValue();
                String artist = (String) dataSnapshot.child("artist").getValue();
                String albumArtPath = (String) dataSnapshot.child("albumArtPath").getValue();

                Album album = new Album(id, title, artist, albumArtPath);
                mLstAlbum.add(album);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return mLstAlbum;
    }
}
