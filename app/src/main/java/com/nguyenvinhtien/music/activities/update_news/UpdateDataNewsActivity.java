package com.nguyenvinhtien.music.activities.update_news;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nguyenvinhtien.music.R;

import com.nguyenvinhtien.music.utils.AppController;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateDataNewsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SELECT_NEWS_REQUEST = 71;
    private static final int IMAGE_GALLERY_REQUEST = 2;

    TextView tvNewsSelect;
    Button btnSelectNews, btnUpdateNews;
    EditText edtNewsTitle, edtNewsSource, edtNewsLink;
    RelativeLayout rlNewsDetail;

    ImageButton ibSelectAlbumArt;

    ProgressDialog mProgress;
    DatabaseReference mDatabase;
    StorageReference mStorage;

    String key_news;
    String title_news;
    String source_news;
    String link_news;
    String image_path_news;

    Uri imageUri = null;
    Uri imageURL = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_data_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_update_data_news);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance().getReference();

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls() {
        mProgress = new ProgressDialog(this);
        tvNewsSelect = (TextView) findViewById(R.id.tv_news_select);
        btnSelectNews = (Button) findViewById(R.id.btn_select_news);
        btnUpdateNews = (Button) findViewById(R.id.btn_update);
        rlNewsDetail = (RelativeLayout) findViewById(R.id.rl_news_detail);
        edtNewsTitle = (EditText) findViewById(R.id.edt_news_title);
        edtNewsSource = (EditText) findViewById(R.id.edt_news_source);
        edtNewsLink = (EditText) findViewById(R.id.edt_news_link);
        ibSelectAlbumArt = (ImageButton) findViewById(R.id.image_select);

    }

    public void initEvents() {
        btnSelectNews.setOnClickListener(this);
        btnUpdateNews.setOnClickListener(this);
        ibSelectAlbumArt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_news:
                Intent selectNewsIntent = new Intent(UpdateDataNewsActivity.this, SelectNewsToUpdateActivity.class);
                startActivityForResult(selectNewsIntent, SELECT_NEWS_REQUEST);
                break;

            case R.id.image_select:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                break;

            case R.id.btn_update:
                if (AppController.getInstance().checkInternetState() == false){
                    Toast.makeText(this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                } else {
                    startUpdate();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_NEWS_REQUEST && resultCode == RESULT_OK) {
            key_news = data.getStringExtra("KEY_NEWS");
            title_news = data.getStringExtra("TITLE_NEWS");
            source_news = data.getStringExtra("SOURCE_NEWS");
            link_news = data.getStringExtra("LINK_NEWS");
            image_path_news = data.getStringExtra("IMAGE_PATH_NEWS");
            updateUI();

        }

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK) {
            imageUri = data.getData();
            ibSelectAlbumArt.setImageURI(imageUri);
        }

    }

    private void updateUI() {
        if (title_news != null && source_news != null
                && link_news != null) {
            tvNewsSelect.setVisibility(View.VISIBLE);
            tvNewsSelect.setText(title_news);
            rlNewsDetail.setVisibility(View.VISIBLE);
            Glide.with(this).load(image_path_news).into(ibSelectAlbumArt);
            edtNewsTitle.setText(title_news);
            edtNewsSource.setText(source_news);
            edtNewsLink.setText(link_news);
        }
    }


    private void startUpdate() {
        mProgress.setMessage("Đang cập nhật bài báo...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        final String news_title_val = edtNewsTitle.getText().toString().trim();
        final String news_source_val = edtNewsSource.getText().toString().trim();
        final String news_link_val = edtNewsLink.getText().toString().trim();

        if (!TextUtils.isEmpty(news_title_val)
                && !TextUtils.isEmpty(news_source_val)
                && !TextUtils.isEmpty(news_link_val)
                && !tvNewsSelect.getText().equals("")) {

            mProgress.show();

            final DatabaseReference newNews = mDatabase.child("News").child(key_news);

            if (imageUri == null){
                newNews.child("title").setValue(news_title_val);
                newNews.child("source").setValue(news_source_val);
                newNews.child("link").setValue(news_link_val);

                Toast.makeText(getApplicationContext(), "Cập nhật bài báo thành công!", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                finish();
            } else {
                StorageReference imagePath = mStorage.child("NewsArts").child(key_news);

                imagePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imageURL = taskSnapshot.getDownloadUrl();
                        newNews.child("title").setValue(news_title_val);
                        newNews.child("source").setValue(news_source_val);
                        newNews.child("link").setValue(news_link_val);
                        Toast.makeText(getApplicationContext(), "Cập nhật bài báo thành công!", Toast.LENGTH_SHORT).show();
                        mProgress.dismiss();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Lỗi! Ảnh chưa được upload!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        } else {
            Toast.makeText(getApplicationContext(), "Cần chọn bài báo và điền đủ các trường", Toast.LENGTH_SHORT).show();
        }
    }
}
