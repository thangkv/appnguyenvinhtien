package com.nguyenvinhtien.music.activities.van_hoc.tho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.update_tho.AddThoActivity;
import com.nguyenvinhtien.music.activities.update_tho.DeleteThoActivity;
import com.nguyenvinhtien.music.activities.update_tho.UpdateDataThoActivity;
import com.nguyenvinhtien.music.utils.Common;

public class UpdateThoActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnAddPost;
    Button btnUpdatePost;
    Button btnDeletePost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_update_tho);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_post);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Common.setStatusBarTranslucent(true, this);

        initControls();
        initEvents();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initControls(){
        btnUpdatePost = (Button) findViewById(R.id.btn_UpdateVideo);
        btnAddPost = (Button) findViewById(R.id.btn_AddVideo);
        btnDeletePost = (Button)findViewById(R.id.btn_DeleteVideo);
    }

    public void initEvents(){
        btnUpdatePost.setOnClickListener(this);
        btnAddPost.setOnClickListener(this);
        btnDeletePost.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_AddVideo:
                Intent intent_add_pri_video = new Intent(getApplicationContext(), AddThoActivity.class);
                startActivity(intent_add_pri_video);
                break;
            case R.id.btn_UpdateVideo:
                Intent intent_update_video = new Intent(getApplicationContext(), UpdateDataThoActivity.class);
                startActivity(intent_update_video);
                break;
            case R.id.btn_DeleteVideo:
                Intent intent_delete_video = new Intent(getApplicationContext(), DeleteThoActivity.class);
                startActivity(intent_delete_video);
                break;

        }
    }
}
