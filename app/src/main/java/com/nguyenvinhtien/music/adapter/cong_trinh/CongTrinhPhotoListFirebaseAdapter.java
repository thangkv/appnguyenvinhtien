package com.nguyenvinhtien.music.adapter.cong_trinh;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nguyenvinhtien.music.R;
import com.nguyenvinhtien.music.activities.kien_truc.cong_trinh.CongTrinhPhotoDetailActivity;
import com.nguyenvinhtien.music.models.Photo;
import com.nguyenvinhtien.music.utils.Constants;
import com.nguyenvinhtien.music.viewholders.PhotoRecyclerViewHolder;

import java.util.ArrayList;

/**
 * Created by Kim Van Thang on 5/29/2017.
 */

public class CongTrinhPhotoListFirebaseAdapter extends FirebaseRecyclerAdapter<Photo, PhotoRecyclerViewHolder> {


    private Activity mContext;
    private DatabaseReference mDatabase;
    ArrayList<Photo> mLstPhoto = new ArrayList<>();

    String congtrinh_key;

    public CongTrinhPhotoListFirebaseAdapter(Activity mContext, Class<Photo> modelClass, int modelLayout, Class<PhotoRecyclerViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;

        getListPhoto();
    }

    public CongTrinhPhotoListFirebaseAdapter(Activity mContext, Class<Photo> modelClass, int modelLayout, Class<PhotoRecyclerViewHolder> viewHolderClass, Query ref, String key) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        this.mContext = mContext;
        congtrinh_key = key;
        getListPhoto();
    }

    @Override
    protected void populateViewHolder(PhotoRecyclerViewHolder viewHolder, final Photo model, final int position) {

        try {
            Glide.with(mContext).load(model.getPath()).placeholder(R.drawable.picture).centerCrop().into(viewHolder.imgImage);
        } catch (Exception e) {
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CongTrinhPhotoDetailActivity.class);
                intent.putExtra(String.valueOf(Constants.POSITION_PHOTO), position);
                intent.putExtra(Constants.PHOTO_ID, getRef(position).getKey());
                intent.putExtra(Constants.LIST_PHOTO, mLstPhoto);
                intent.putExtra(Constants.POST_ID, congtrinh_key);
                mContext.startActivity(intent);
            }
        });

    }

    private ArrayList<Photo> getListPhoto() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Congtrinhs-Anhs").child(congtrinh_key).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String id = dataSnapshot.getKey();
                String title = (String) dataSnapshot.child("title").getValue();
                String path = (String) dataSnapshot.child("path").getValue();

                Photo photo = new Photo(id, title, path);
                mLstPhoto.add(photo);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return mLstPhoto;
    }
}
