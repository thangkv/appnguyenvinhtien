package com.nguyenvinhtien.music.models;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;


public class Song implements Serializable {

    private String id;
    private String title;
    private String album;
    private String artist;
    private String albumImagePath;
    private int duration;
    private String path;
    private String lyric;

    private String type;

    public static DatabaseReference mData = FirebaseDatabase.getInstance().getReference();

    private static ArrayList<Song> listHitSong;
    private static ArrayList<Song> listAudioVanHoc;
    private static ArrayList<Song> listAudioKienTruc;

    public Song() {

    }



    public Song(String id, String title, String album, String artist, String albumImagePath , int duration, String path, String lyric) {
        this.id = id;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.albumImagePath = albumImagePath;
        this.duration = duration;
        this.path = path;
        this.lyric = lyric;
    }

    public Song(String id, String title, String album, String artist, String albumImagePath, int duration, String path) {
        this.id = id;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.albumImagePath = albumImagePath;
        this.duration = duration;
        this.path = path;

    }



    public Song(String id, String title, String album, String artist, int duration, String path) {
        this.id = id;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.duration = duration;
        this.path = path;
    }

    public Song(String title, String album, String artist, int duration, String path) {
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.duration = duration;
        this.path = path;
    }

    //KVT
    public Song(String id, String title, String album, String artist, String albumImagePath, String path, String lyric, String type) {
        this.id = id;
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.albumImagePath = albumImagePath;//KVT
        this.path = path;
        this.lyric = lyric;

        this.type = type;
    }

    public Song(String title, String album, String artist, String path, String lyric) {
        this.title = title;
        this.album = album;
        this.artist = artist;
        this.path = path;
        this.lyric = lyric;
    }



    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbumImagePath() {
        return albumImagePath;
    }

    public void setAlbumImagePath(String albumImagePath) {
        this.albumImagePath = albumImagePath;
    }

    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static ArrayList<Song> getListSong() {
        if (listHitSong == null) {
            listHitSong = new ArrayList<>();
            mData.child("Songs").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String artist = (String) dataSnapshot.child("artist").getValue();
                    String album = (String) dataSnapshot.child("album").getValue();
                    String albumImagePath = (String) dataSnapshot.child("albumImagePath").getValue();
                    String path = (String) dataSnapshot.child("path").getValue();
                    String lyric = (String) dataSnapshot.child("lyric").getValue();
                    String type = (String) dataSnapshot.child("type").getValue();

                    Song song = new Song(id, title, album, artist, albumImagePath, path, lyric, type);
                    listHitSong.add(song);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        return listHitSong;
    }

    public static ArrayList<Song> getListAudioVanHoc() {
        if (listAudioVanHoc == null) {
            listAudioVanHoc = new ArrayList<>();
            mData.child("AudioVanHocs").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String artist = (String) dataSnapshot.child("artist").getValue();
                    String album = (String) dataSnapshot.child("album").getValue();
                    String albumImagePath = (String) dataSnapshot.child("albumImagePath").getValue();
                    String path = (String) dataSnapshot.child("path").getValue();
                    String lyric = (String) dataSnapshot.child("lyric").getValue();
                    String type = (String) dataSnapshot.child("type").getValue();

                    Song audio = new Song(id, title, album, artist, albumImagePath, path, lyric, type);
                    listAudioVanHoc.add(audio);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        return listAudioVanHoc;
    }

    public static ArrayList<Song> getListAudioKienTruc() {
        if (listAudioKienTruc == null) {
            listAudioKienTruc = new ArrayList<>();
            mData.child("AudioKienTrucs").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    String id = dataSnapshot.getKey();
                    String title = (String) dataSnapshot.child("title").getValue();
                    String artist = (String) dataSnapshot.child("artist").getValue();
                    String album = (String) dataSnapshot.child("album").getValue();
                    String albumImagePath = (String) dataSnapshot.child("albumImagePath").getValue();
                    String path = (String) dataSnapshot.child("path").getValue();
                    String lyric = (String) dataSnapshot.child("lyric").getValue();

                    String type = (String) dataSnapshot.child("type").getValue();

                    Song audio = new Song(id, title, album, artist, albumImagePath, path, lyric, type);
                    listAudioKienTruc.add(audio);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }
        return listAudioKienTruc;
    }
}
